<?php

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

$result = 0;

$data['file'] = $_FILES;
$data['text'] = $_POST;

$destination_path = "Uploaded_files/";

$filename = explode(".",basename( $_FILES['image']['name']));

$newfilename      = "ECF_".$_SESSION['CURREF']."_FichePrd.".$filename[1]; 

$target_path = $destination_path . $newfilename;

if(@move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
    echo json_encode($result);
}else{
    $result = 1;
    echo json_encode($result);
}


?>
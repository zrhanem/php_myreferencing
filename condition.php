<?php
@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}

$smarty->assign('DESFR1',$_SESSION['DESFR1']);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('LstuntAchat');

/*try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->LstuntAchat();
} catch ( Exception $e ) {
    echo $e->getMessage ();
}*/

if (!empty ($TabData) ){
     
    $smarty->assign('ListeUnt',$TabData);    

}

unset($TabData);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Ref_CondECF',array(
    'pre_ref' => $_SESSION['CURREF']
));
/*try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Ref_CondECF($_SESSION['CURREF']);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}*/


if (!empty ($TabData) ){
     
        $smarty->assign('A00A26',trim($TabData['A00A26']));
        $smarty->assign('A00A30',trim($TabData['A00A30']));
        $smarty->assign('A00A36',trim($TabData['A00A36']));
        $smarty->assign('A00A40',trim($TabData['A00A40']));
        $smarty->assign('A00A45',round(trim($TabData['A00A45']), 2));
        $smarty->assign('FRANC1',round(trim($TabData['FRANC1']), 2));
        $smarty->assign('A00A65',round(trim($TabData['A00A65']), 4));
        $smarty->assign('A00A75',round(trim($TabData['A00A75']), 4));
        $smarty->assign('A00A85',round(trim($TabData['A00A85']), 4));
        $smarty->assign('A00A95',round(trim($TabData['A00A95']), 4));        
        $smarty->assign('A00B10',round(trim($TabData['A00B10']), 2));
        $smarty->assign('A00F80',trim($TabData['A00F80']));
        $smarty->assign('A00B15',round(trim($TabData['A00B15']), 2));        
        $smarty->assign('A00A25',trim($TabData['A00A25']));
        $smarty->assign('A00A35',trim($TabData['A00A35']));
        $smarty->assign('A00A60',trim($TabData['A00A60']));
        $smarty->assign('A00A70',trim($TabData['A00A70']));
        $smarty->assign('A00A80',trim($TabData['A00A80']));
        $smarty->assign('A00A90',trim($TabData['A00A90']));        
        $smarty->assign('TVAPRD',trim($TabData['TVAPRD']));
        $smarty->assign('A00A50',round(trim($TabData['A00A50']), 4));
        $smarty->assign('A00A55',round(trim($TabData['A00A55']), 4));
        $smarty->assign('COMENT',trim($TabData['COMENT']));
        $smarty->assign('A00B25',trim($TabData['A00B25']));
        $smarty->assign('LBL1',trim($TabData['LBL1']));
        
        
        if(trim($TabData['A00A26']) != ''){
            $smarty->assign('LBL2',trim($TabData['A00A26']).' (de'.trim($TabData['A00A30']).')');
        }else{
            $smarty->assign('LBL2',trim($TabData['LBL2']).' (de'.trim($TabData['A00A30']).')');
        }
}

unset($TabData);

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Ref_CondX',array(
    'pre_ref' => $_SESSION['CURREF'],
    'Soc' => 'ECOTEL'
));

/*try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Ref_CondX($_SESSION['CURREF'],'ECOTEL');
} catch ( Exception $e ) {
    echo $e->getMessage ();
}*/


if (!empty ($TabData) ){
    
    $smarty->assign('EJRAXN7',trim($TabData['JRAXN7']));
    $smarty->assign('EJRVON6',trim($TabData['JRVON6']));
    $smarty->assign('EJRVPN6',trim($TabData['JRVPN6']));
    $smarty->assign('EJRVQN6',trim($TabData['JRVQN6']));
    $smarty->assign('EJRVRN6',trim($TabData['JRVRN6']));
    $smarty->assign('EJRVNN6',trim($TabData['JRVNN6']));
    $smarty->assign('EJRA1P4',round(floatval(trim($TabData['JRA1P4'])),2));
    $smarty->assign('EJRAZP4',round(floatval(trim($TabData['JRAZP4'])),2));
    $smarty->assign('EJRRUPC',round(floatval(trim($TabData['JRRUPC'])),2));
    $smarty->assign('EJRRVPC',round(floatval(trim($TabData['JRRVPC'])),2));
    $smarty->assign('EJRRWPC',round(floatval(trim($TabData['JRRWPC'])),2));
    $smarty->assign('EJRRXPC',round(floatval(trim($TabData['JRRXPC'])),2));
    $smarty->assign('EJRQ7Q1',round(floatval(trim($TabData['JRQ7Q1'])),2));
    
}else{
    $smarty->assign('EJRAXN7','');
    $smarty->assign('EJRVON6','');
    $smarty->assign('EJRVPN6','');
    $smarty->assign('EJRVQN6','');
    $smarty->assign('EJRVRN6','');
    $smarty->assign('EJRVNN6','');
    $smarty->assign('EJRA1P4','');
    $smarty->assign('EJRAZP4','');
    $smarty->assign('EJRRUPC','');
    $smarty->assign('EJRRVPC','');
    $smarty->assign('EJRRWPC','');
    $smarty->assign('EJRRXPC','');
    $smarty->assign('EJRQ7Q1','');
}

unset($TabData);


$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Ref_CondX',array(
    'pre_ref' => $_SESSION['CURREF'],
    'Soc' => 'CORPO'
));
/*try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Ref_CondX($_SESSION['CURREF'],'CORPO');
} catch ( Exception $e ) {
    echo $e->getMessage ();
}*/


if (!empty ($TabData) ){

    $smarty->assign('CJRAXN7',trim($TabData['JRAXN7']));
    $smarty->assign('LJRVON6',trim($TabData['JRVON6']));
    $smarty->assign('LJRVPN6',trim($TabData['JRVPN6']));
    $smarty->assign('LJRVQN6',trim($TabData['JRVQN6']));
    $smarty->assign('LJRVRN6',trim($TabData['JRVRN6']));
    $smarty->assign('LJRVNN6',trim($TabData['JRVNN6']));
    $smarty->assign('LJRA1P4',round(floatval(trim($TabData['JRA1P4'])),2));
    $smarty->assign('LJRAZP4',round(floatval(trim($TabData['JRAZP4'])),2));
    $smarty->assign('LJRRUPC',round(floatval(trim($TabData['JRRUPC'])),2));
    $smarty->assign('LJRRVPC',round(floatval(trim($TabData['JRRVPC'])),2));
    $smarty->assign('LJRRWPC',round(floatval(trim($TabData['JRRWPC'])),2));
    $smarty->assign('LJRRXPC',round(floatval(trim($TabData['JRRXPC'])),2));
    $smarty->assign('LJRQ7Q1',round(floatval(trim($TabData['JRQ7Q1'])),2));
}else{
    $smarty->assign('CJRAXN7','');
    $smarty->assign('LJRVON6','');
    $smarty->assign('LJRVPN6','');
    $smarty->assign('LJRVQN6','');
    $smarty->assign('LJRVRN6','');
    $smarty->assign('LJRVNN6','');
    $smarty->assign('LJRA1P4','');
    $smarty->assign('LJRAZP4','');
    $smarty->assign('LJRRUPC','');
    $smarty->assign('LJRRVPC','');
    $smarty->assign('LJRRWPC','');
    $smarty->assign('LJRRXPC','');
    $smarty->assign('LJRQ7Q1','');
}

unset($TabData);

//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->refetatpages($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}


//////////////////////////////////////////////////////////////////

$smarty->display('condition.tpl');



?>
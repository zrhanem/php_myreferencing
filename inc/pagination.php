<?php

function getCurrentUrl() {
 return dirname($_SERVER['SERVER_PROTOCOL']) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
}

function addParameterFrom($Url,$Param,$Value) {
    $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param.'[^&]*)\b/i','',$Url);

    if (strpos($sNewUrl,'?') === false) {
        $sNewUrl .= "?".$Param.$Value;
    }
    else {
        $sNewUrl .= "&".$Param.$Value;
    }
    $sNewUrl = str_replace('?&','?',$sNewUrl);

    return $sNewUrl;
}

function delParameterFrom($Url,$Param1, $Param2) {
    $sNewUrl = $Url;
    if ($Param1 != '') {
        $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param1.'[^&]*)\b/i','',$Url);
    }
    if ($Param2 != '') {
        $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param2.'[^&]*)\b/i','',$sNewUrl);
    }
    return $sNewUrl;
}

function deleteParameter($Param1, $Param2) {
    $Url = trim(dirname($_SERVER['SERVER_PROTOCOL']) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $sNewUrl = $Url;
    if ($Param1 != '') {
        $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param1.'[^&]*)\b/i','',$Url);
    }
    if ($Param2 != '') {
        $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param2.'[^&]*)\b/i','',$sNewUrl);
    }    
    return $sNewUrl;
}
function setNewParameter($Param,$Value) {
    $Url = trim(dirname($_SERVER['SERVER_PROTOCOL']) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param.'[^&]*)\b/i','',$Url);
    
    if (strpos($sNewUrl,'?') === false) {   
        $sNewUrl .= "?".$Param.$Value;
    }
    else {    
        $sNewUrl .= "&".$Param.$Value;
    }
    $sNewUrl = str_replace('?&','?',$sNewUrl);
    
    return $sNewUrl;
}

function setPrepareParameter($Param) {
$Url = trim(dirname($_SERVER['SERVER_PROTOCOL']) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
$sNewUrl = preg_replace('/\b([&|&amp;]{0,1}'.$Param.'[^&]*)\b/i','',$Url);

if (strpos($sNewUrl,'?') === false) { 
    $sNewUrl .= "?".$Param;
}
else {
    $sNewUrl .= "&".$Param;
}
$sNewUrl = str_replace('?&','?',$sNewUrl);

return $sNewUrl;
}

function pagination($current_page, $nb_pages, $link='Page=%d', $around=2, $firstlast=1)
{
    $pagination = array();

    // replace the parameter Page by the new page number
    // http://www.mychomette.com/mercuriale.php?Page=5&nbvue=6 -> http://www.mychomette.com/mercuriale.php?nbvue=6
    // and adding new page number
    // http://www.mychomette.com/mercuriale.php?nbvue=6 -> http://www.mychomette.com/mercuriale.php?nbvue=6&Page=6
    $url = preg_replace('/\b([&|&amp;]{0,1}page=[^&]*)\b/i','',getCurrentUrl());
    if (strpos($url,'?') === false) {
        $url .= "?";
    }
    $url .= "&Page=";
    $url = str_replace('?&','?',$url);
    $url = str_replace('??','?',$url);
    
    if ( $nb_pages > 1 ) {
        if ( $current_page > 1 )
            array_push($pagination, array('Text'=>'<','link'=>$url.sprintf('%d', $current_page-1) , 'active'=>'' ));
   
        // On boucle autour de la page courante
        if ($current_page < 4) {
            $start = 1;
            $end = min(array($nb_pages, 5));
        }
        else if ($current_page > ($nb_pages - 4)) {
            $start = ($nb_pages - 4);
            $end = $nb_pages;
        }
        else {
            $start = $current_page-$around;
            $end = min(array($current_page+$around, $nb_pages));
        }
        for ( $i=$start ; $i<=$end ; $i++ ) {
            if ( $i==$current_page )
                // $pagination .= '<span class="current">'.$i.'</span>';
                array_push($pagination, array('Text'=>"$i",'link'=>'' , 'active'=>'active' ));
            else
                // $pagination .= '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
                array_push($pagination, array('Text'=>"$i",'link'=> $url.sprintf('%d', $i) , 'active'=>'' ));
        }
    
        // Lien suivant
        if ( $current_page < $nb_pages )
            // $pagination .= ' <a class="prevnext" href="'..'" title="Page suivante">Suivant &raquo;</a>';
            array_push($pagination, array('Text'=>">",'link'=>$url.sprintf('%d', ($current_page+1)) , 'active'=>'' ));

    }
    
    return $pagination;
}

?>
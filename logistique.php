<?php


@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}


/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

$smarty->assign('DESFR1',$_SESSION['DESFR1']);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listerchapitre');


if (!empty ($TabData) ){
     
    $smarty->assign('Lstchp',$TabData);

}

unset($TabData);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$ResData = $Result->call('Listernomenclature', array(
    'code' => '01'
));

if (!empty ($ResData) ){
     
    $smarty->assign('Lstnmclt',$ResData);

}


$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$ResDatas = $Result->call('Listerpays');


if (!empty ($ResDatas) ){
     
    $smarty->assign('Lstpays',$ResDatas);

}


$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('CondETLogistique', array(
    'pre_ref' => $ref
));

if (!empty ($TabData) ){
    $smarty->assign('A00C25',trim($TabData['A00C25']));
    $smarty->assign('A00C50',trim($TabData['A00C50']));
    $smarty->assign('A00C75',trim($TabData['A00C75']));
    $smarty->assign('RESPAL',trim($TabData['RESPAL']));
    $smarty->assign('PALSEP',trim($TabData['PALSEP']));
    $smarty->assign('MRQCNF',trim($TabData['MRQCNF']));
    $smarty->assign('VOLLIT',round(trim($TabData['VOLLIT']), 2));
    $smarty->assign('A00C95',trim($TabData['A00C95']));    
    $smarty->assign('A00D10',trim($TabData['A00D10']));
    $smarty->assign('A00C90',trim($TabData['A00C90']));
    $smarty->assign('NOMNTR',trim($TabData['NOMNTR']));
    $smarty->assign('A00C92',trim($TabData['A00C92']));
}

unset($TabData);

//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('refetatpages', array(
    'idref' => $ref
));


if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE3'] == 'O'){
        $smarty->assign('classvis',"btn btn-success");
    }else{
        $smarty->assign('classvis',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE5'] == 'O'){
        $smarty->assign('classlog',"btn btn-success");
    }else{
        $smarty->assign('classlog',"btn btn-danger");
    }
    if($TabData['PAGE6'] == 'O'){
        $smarty->assign('classmar',"btn btn-success");
    }else{
        $smarty->assign('classmar',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classvis',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classlog',"btn btn-danger");
    $smarty->assign('classmar',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}



/* 
 $smarty->assign('classmar',"btn btn-success");
 $smarty->assign('classeco',"btn btn-success");*/
//////////////////////////////////////////////////////////////////

$smarty->display('logistique.tpl');

?>
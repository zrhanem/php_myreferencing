<?php
/*if (file_exists('maintenance.html') == true){
    header('Location: maintenance.htm');
}*/
@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}


require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

$_SESSION['CURREF'] = '';
$_SESSION['MODIF'] = 'O';
$_SESSION['DESFR1'] = '';

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;


$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

/*$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listerreference', array(
    'sidentifiant' => $_SESSION['IDFRS'],
    'iStatus' => 1 
));*/

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Listerreference($_SESSION['IDFRS'],1);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

$referencetab = array();

if (!empty ($TabData) ){

    foreach ($TabData as $ligne) {
        $monarray[]=array(
            trim($ligne['C00001']),
            trim($ligne['C00002']),
            trim($ligne['C00003']),
            trim($ligne['C00007']),
            trim($ligne['C00026']),
            trim($ligne['C00028']),
            trim($ligne['C00016']),
            trim($ligne['A00A10']),
            trim($ligne['A00A15']),
            trim($ligne['donnee']),
            trim($ligne['PAGE1']),
            trim($ligne['PAGE2']),
            trim($ligne['PAGE3']),
            trim($ligne['PAGE4']),
            trim($ligne['PAGE5']),
            trim($ligne['PAGE6']),
            trim($ligne['PAGE7']),
            trim($ligne['COMPLET'])
        );
        
        $referencetab[] = trim($ligne['C00016']);
        
    }
    
    $smarty->assign('ListeRef',$monarray);
}

if(isset($referencetab) && !empty($referencetab)){
    $_SESSION['REFFRS'] = $referencetab;
}

$smarty->display('home.tpl');

?>
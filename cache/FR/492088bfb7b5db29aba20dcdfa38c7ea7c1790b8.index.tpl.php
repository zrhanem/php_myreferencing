<?php /*%%SmartyHeaderCode:827294351560245ae998812-89241321%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '492088bfb7b5db29aba20dcdfa38c7ea7c1790b8' => 
    array (
      0 => '/www/zendsvr6/htdocs/laurent/mychomette/templates/index.tpl',
      1 => 1442911580,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '827294351560245ae998812-89241321',
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_560245af5750c7_96042475',
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_560245af5750c7_96042475')) {function content_560245af5750c7_96042475($_smarty_tpl) {?><!DOCTYPE html>
<html class="no-js">
    <head>
        <title>MyChomette</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"
              href="//fonts.googleapis.com/css?family=Open+Sans:400,600">

        <link rel="stylesheet" href="assets/css/main.min.css">
        <link rel="stylesheet" href="assets/css/mybootstrap.css">
		<link rel="stylesheet" href="assets/css/mymodal.css">
        <script src="assets/js/vendors/modernizr.min.js"></script>
        <!--[if IE 8]>
            <script type="text/javascript" src="assets/js/polyfills/html5shiv.min.js"></script>
            <script type="text/javascript" src="assets/js/polyfills/respond.min.js"></script>
            <script type="text/javascript" src="assets/js/polyfills/selectivizr.min.js"></script><![endif]-->

	<style>
      .form-submit { text-indent:-9999em; border:0; width:539px; height:170px; background:url("assets/images/idenvous.png") no-repeat 0 0 ; line-height:0 font-size:0; }
    </style>

    </head>
    <body>
        <div class="offcanvas">
            <div id="wrapper" class="wrapper">
                <header role="banner" class="header">
                    <div class="header-top hidden-xs hidden-sm">
                        <div class="container">
                            <div class="header-top__contact">
                                <ul class="list-inline">
                                    <li class="header-top__contact--tel"><span><img src="assets/images/0825024023.png"></span>Du lundi au vendredi de 8h30 � 12h30 et de 14h � 17h30</li>
                                    <li class="header-top__contact--social"><a href="#" target="blank"
                                                                               title="Retrouvez-nous sur Facebook"><i
                                                class="icon icon-facebook"></i><span class="sr-only">Retrouvez-nous
											sur Facebook</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="header-container container">
                        <h1 class="header-logo">
                            <a href="#"><span>MyChomette</span></a>
                        </h1>
                        <div class="header-links">
                            <div class="header-links__item header-links__item--account">
                                <a href="#loginmodal" data-toggle="modal"
                                   data-target="#loginmodal"><i class="icon icon-account"></i><strong>Mon
									compte</strong><span>Acc&egrave;s &agrave; mes donn&eacute;es
									personnelles</span></a>
                            </div>
                            <div class="header-links__item header-links__item--mercuriale">
                                <a href="#loginmodal" data-toggle="modal"
                                   data-target="#loginmodal"><i class="icon icon-cart"></i><strong>Ma
									mercuriale</strong></a>
                            </div>
                            <div class="header-links__item header-links__item--cart">
                                <a href="#loginmodal" data-toggle="modal"
                                   data-target="#loginmodal"><i class="icon icon-bag"></i><strong>Mon
									panier</strong><span><em>articles</em></span><span>0,00&euro;
                                        HT</span></a>
                            </div>
                        </div>
                        <button id="toggle-offcanvas" type="button"
                                class="header-toggle hidden-md hidden-lg">
                            <span class="icon-burger"></span>
                        </button>
                        <form role="search" class="header-search" data-toggle="modal"
                              data-target="#loginmodal">
                            <input type="search" placeholder="Rechercher" class="form-control">
                            <button type="submit" class="btn">OK</button>
                        </form>
                        <div class="header-welcome">
                            <small>Le sp�cialiste francais au service<br>des professionnels de
							l'h�tellerie<br>et de la restauration
                            </small>
                        </div>
                    </div>
                    <nav role="navigation" class="header-navigation hidden-xs hidden-sm">
                        <div class="container">
                            <ul class="list-unstyled">
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">Tous nos produits</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">Ma mercuriale</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">Mes favoris</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">Mes kits</a></li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <div class="container">
                    <div class="row" id="ecf">

                        <div class="col-md-12"
                             style="background-color: #eee; border: 1px solid #ddd; border-radius: 4px;  padding-bottom : 10px; background-image:url(assets/images/fdpidx.jpg); background-size: 100% auto;">
                            <div class="quick-login" style="opacity: 0.9; filter: alpha(opacity=90);">
                                <div>
                                    <h2 style="color:#a23c28;">Acc�dez � votre espace client</h2>
                                    <p style="font-weight: normal; color:#000000;">(Exclusivement r�serv� aux clients Chomette)</p>
                                </div>
                                <div class="quick-login__label"><br></div>
                            </div>

							<div class="quick-login row" style="background-color:#a23c28; opacity: 0.9; filter: alpha(opacity=90);">
								<div class="quick-login__container col-md-6" style="border:none;">
                                    <h4 style=" font-size:1.5em; margin-top : 10px; font-weight: bold; color : #fff; border-bottom-style : solid; border-bottom-color : #fff; border-bottom-width : 2px;">S'identifier</h4>

                                    <div class="alert alert-error collapse" role="alert" id="Errorlogin">
  										<span><p>Looks like the passwords you entered don't match!</p></span>
									</div>

                                    <form role="form" method="POST" id='loginfrm' name='loginfrm'>
                                        <div class="form-group">
                                            <label for="CltEmail">Identifiant ou Email</label> <input
                                                type="email" class="form-control" id="CltEmail"
                                                placeholder="saisir votre email" name="CltEmail">
                                        </div>
                                        <div class="form-group">
                                            <label for="CltPass">Mot de passe</label> <input
                                                type="password" class="form-control" id="CltPass"
                                                placeholder="Votre mot de passe"  name="CltPass">                                                
                                        </div>
                                        
                                        <div class="row" style="text-align: right; margin-right: 5px;">
                                            <span style="float: left; margin-left: 10px;">:										<a href="getids.php" style="font-weight: normal; color:#fff;">Demandez vos identifiants</label></a></span>
                                            :										<a href="lostpassword.php" style="font-weight: normal; color:#fff;">Mot de passe oubli� ?</label></a>
                                        </div>  <br>
                                       <img src = "assets/images/idenvous.png" id="loginbtn1" name="loginbtn1" style="cursor:pointer;"> 
                                       
								
                                    </form>

								</div>
                                <div class="quick-login__container col-md-6 " style="border:none;">
                                    <h4 style=" font-size:1.5em; margin-top : 10px; font-weight: bold; color : #fff; border-bottom-style : solid; border-bottom-color : #fff; border-bottom-width : 2px;"><em>my</em>Chomette, un monde de services s'offre � vous</h4>
                                    <br>
                                    <ul>
                                    	<li  style="font-weight: normal; color:#fff; font-size:1.5em;">Tarifs personnalis�s</li>
                                        <li  style="font-weight: normal; color:#fff; font-size:1.5em;">Conditions commerciales avantageuses</li>
                                        <li  style="font-weight: normal; color:#fff; font-size:1.5em;">Suivi de commande</li>
                                        <li  style="font-weight: normal; color:#fff; font-size:1.5em;">Promotions permanentes</li>
                                        <li  style="font-weight: normal; color:#fff; font-size:1.5em;">Et plus encore</li>
                                    </ul>
                                    <br><br>

									

                                </div>
                            </div>

							<div class="row" style="padding-left: 15px; padding-right: 15px;">
                            <div class="quick-login" style="background-color: #999;">
                                <div class="quick-login__container" style="background-color: #999;">
                                    <h3><center>Vous n'�tes pas encore client Chomette ?</center></h3>
                                    <h3><center><a href="http://www.chomette.com/customer/account/create/">Inscrivez-vous</a></center></h3>
                                </div>
                            </div>
							</div>

                        </div>
<div class="modal fade " tabindex="-1" role="dialog" id="loginmodal" aria-labelledby="loginmodal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">MyChomette</h4><br>
      <p>Veuillez vous connecter pour acc�der � ces menus</p>
    </div>
  </div>
</div>
                    </div>
                    <footer class="footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3">
                                    <ul class="list-inline">
                                        <li><a href="footer/faq_FR.php">FAQ</a></li>
                                        <li><a href="footer/cgv_FR.pdf">CGV</a></li>
                                        <li><a href="footer/credit_FR.php">Cr&eacute;dits</a></li>
                                        <li><a href="footer/legal_FR.php">Mentions L&eacute;gales</a></li>
                                        <li><a href="footer/perso_FR.php">Donn&eacute;es personnelles</a></li>
                                        <li><a href="http://www.ecf.fr/files/pdf/dev_durable.pdf" target="blank">Rapport d&eacute;veloppement durable</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            <script type="text/javascript" src="assets/js/jquery/1.11.1/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
            <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
            <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
            <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
            <script type="text/javascript" src="assets/js/main.min.js"></script>
            
            
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40009067-1', 'auto');
  ga('send', 'pageview');
</script>   
<!-- START IADVIZE LIVECHAT -->
	<script type='text/javascript'>
		(function() {
			var idz = document.createElement('script'); idz.type = 'text/javascript'; idz.async = true;
			idz.src = document.location.protocol + "//lc.iadvize.com/iadvize.js?sid=10923";
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(idz, s);
		})();
	</script>
<!-- END IADVIZE LIVECHAT -->             
            <script type="text/javascript">
              $(document).ready(function(){
              function trim(str){
				var str=str.replace(/^\s+|\s+$/,'');
				return str;
			   }
 //               $("#btnConnexion").click(function(){
				$( "#loginbtn1" ).click(function(){
                  $.ajax({
					 cache: false,
                     type: 'POST',
                     url: 'ajax/ajax_login.php',
                     data: 'CltEmail='+$('#CltEmail').val()+'&CltPass='+$('#CltPass').val(),
                     dataType: "html",
                     success: function(datas_result){
                      var data_result=trim(datas_result);
						            if (data_result == '5'){
                          $("#Errorlogin").html("Ce compte est d�sactiv�");
                        	$('#Errorlogin').show();
                        }else if(data_result != '1' && data_result != '11' && data_result != '10') {
                        	$("#Errorlogin").html("Indentifiant ou Mot de passe incorrect...");
                        	$('#Errorlogin').show();
							
                         }
                         else {
              							 if(data_result == '1') {
                                       		window.location.href="home.php";
              						     }
              							 else if(data_result == '10') {
              	                         		window.location.href="homefirst.php";
              							 }
              							 else {
              								window.location.href="supervisor_home.php";
              							}
                          };
                     },
                     beforeSend:function()
                     {
                     	$("#Errorlogin").html("Loading...");
                      	$('#Errorlogin').show();
                     }
                  });

                  return false;
                });
              });
            </script>
            
    </body>
</html><?php }} ?>

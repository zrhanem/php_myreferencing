<?php
if (file_exists('maintenance.html') == true){
    header('Location: maintenance.html');
}
session_start();

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
//require_once('bibliotheque/nusoap/lib/nusoap.php');
//require_once('inc/language_trans.php');

if (!isset($_SESSION['LANG'])) {
    $_SESSION['LANG']='FR';
}
if (isset($_SESSION['USER'])) {
    header('Location: home.php');
}


/*
 * smarty
 */

$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->setCacheDir('./cache/'.$_SESSION['LANG']);
//$smarty->caching = false;
$smarty->assign('error_cnx',FALSE); // by default : no error

/*
 * display smarty template
 */
$smarty->display('lostpassword.tpl');

?>


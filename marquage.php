<?php


@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');


if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

$smarty->assign('DESFR1',$_SESSION['DESFR1']);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listermarquage', array(
    'pre_ref' => $ref
));

if (!empty ($TabData) ){
    
    $smarty->assign('Lstmarq',$TabData);
    
}


unset($TabData);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listergaranties', array(
    'pre_ref' => $ref
));

if (!empty ($TabData) ){

    $smarty->assign('Lstgrt',$TabData);
    
    if($TabData['A00E45'] != ''){
        $btrouve = 0;
        $file='';
        $handle = opendir('Uploaded_files/Docs/Pieces_Detache/');
        
        if ($handle) {
        
            /* Ceci est la fa�on correcte de traverser un dossier. */
            while (false !== ($entry = readdir($handle))) {
                if(is_file('Uploaded_files/Docs/Pieces_Detache/'.$entry)){
                    $file = explode(".",$entry);
                    if($file[0] == ('ECF_'.$_SESSION['CURREF'].'_eclates') ){
                        $btrouve = 1;
                        break;
                    }
                }
            }
        
            closedir($handle);
        }
        
        //Affichage de la fiche de production si elle xiste
        if ($btrouve == 1) {
            $smarty->assign('FEP',$file[0].'.'.$file[1]);
        } else {
            $smarty->assign('FEP',"");
        }
    }else{
        $smarty->assign('FEP',"");
    }
    
    if($TabData['A00E50'] != ''){
        $btrouve = 0;
        $file='';
        $handle = opendir('Uploaded_files/Docs/Mode_Emploi/');
        
        if ($handle) {
        
            //    Ceci est la fa�on correcte de traverser un dossier.
            while (false !== ($entry = readdir($handle))) {
                if(is_file('Uploaded_files/Docs/Mode_Emploi/'.$entry)){
                    $file = explode(".",$entry);
                    if($file[0] == ('ECF_'.$_SESSION['CURREF'].'_modeemploi') ){
                        $btrouve = 1;
                        break;
                    }
                }
            }
        
            closedir($handle);
        }
        
        //Affichage de la fiche de production si elle xiste
        if ($btrouve == 1) {
            $smarty->assign('ME',$file[0].'.'.$file[1]);
        } else {
            $smarty->assign('ME',"");
        }
    }else{
        $smarty->assign('ME',"");
    }

}

unset($TabData);

//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('refetatpages', array(
    'idref' => $ref
));


if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE3'] == 'O'){
        $smarty->assign('classvis',"btn btn-success");
    }else{
        $smarty->assign('classvis',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE5'] == 'O'){
        $smarty->assign('classlog',"btn btn-success");
    }else{
        $smarty->assign('classlog',"btn btn-danger");
    }
    if($TabData['PAGE6'] == 'O'){
        $smarty->assign('classmar',"btn btn-success");
    }else{
        $smarty->assign('classmar',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classvis',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classlog',"btn btn-danger");
    $smarty->assign('classmar',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}



/* 
 $smarty->assign('classeco',"btn btn-success");*/
//////////////////////////////////////////////////////////////////





 


$smarty->display('marquage.tpl');

?>
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#loginmodal').on('hidden.bs.modal', function () {
        $("#formError").html("");
        $("#formErrorEmail").html("");
        $("#formErrorPasswd").html("");
    });
$("#formlogin").validate({
    
        // Specify the validation rules
        rules: {
            email: {
                required: true
            },
            passwd: {
                required: true,
                minlength: 6
            },

        },
        
        // Specify the validation error messages
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            email: "Please enter a valid email address"
        }
        
 //       submitHandler: function(form) {
 //           form.submit();
 //       }
    });
    $("button#LoginValidation").click(function () {
        
        
        
        $.ajax({
            type: "POST",
            url: "ajax/ajax_login.php",
            data: $('form.loginform').serialize(),
            success: function (msg) {
                if (msg == 0) {
                    $("#formError").html("<font color ='red'><center>L'identifiant et/ou le mot de passe n'ont pas �t� reconnus<center></font>");
                }
                else {
                    window.location = msg;
                }
            },
            error: function () {
                alert("failure");
            }
        });
    });
});



<?php
@session_start();
unset($_SESSION);
session_unset();
header("Location: ../index.php");
die("Redirecting to: index.php");
?>
<?php

//require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

$result = 0;

$data['file'] = $_FILES;
$data['text'] = $_POST;

$destination_path_ambiance = "../Uploaded_files/Images/Ambiances/";
$destination_path_visuel = "../Uploaded_files/Images/Visuels/";
$destination_path_logo = "../Uploaded_files/Images/Logos/";


$TabDescriptif = array();

$TabDescriptif['idpref'] = $_SESSION['CURREF'];

if(isset($_FILES['Visuel1']['name']) && !empty($_FILES['Visuel1']['name'])){
    $filename = explode(".",basename( $_FILES['Visuel1']['name']));
    
    $newfilename      = "ECF_".$_SESSION['CURREF']."_F20.".strtolower($filename[1]);
    
    $target_path = $destination_path_visuel . $newfilename;
    
    if(!@move_uploaded_file($_FILES['Visuel1']['tmp_name'], $target_path)) {
        $result = 1;
    }else {
        chmod($target_path, 0664);
        $TabDescriptif['A00F20'] = $newfilename ;        
    }
}else if(isset($_POST['Visact1']) && !empty($_POST['Visact1'])){
    $TabDescriptif['A00F20'] = $_POST['Visact1'];
}else{
    $TabDescriptif['A00F20'] = '' ;
}

if(isset($_FILES['Visuel2']['name']) && !empty($_FILES['Visuel2']['name'])){
    $filename = explode(".",basename( $_FILES['Visuel2']['name']));
    
    $newfilename      = "ECF_".$_SESSION['CURREF']."_F90.".strtolower($filename[1]);
    
    $target_path = $destination_path_visuel . $newfilename;
    
    if(!@move_uploaded_file($_FILES['Visuel2']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F90'] = $newfilename ;        
    }
}else if(isset($_POST['Visact2']) && !empty($_POST['Visact2'])){
    $TabDescriptif['A00F90'] = $_POST['Visact2'] ;
}else{
    $TabDescriptif['A00F90'] = '' ;
}

if(isset($_FILES['Visuel3']['name']) && !empty($_FILES['Visuel3']['name'])){
    $filename = explode(".",basename( $_FILES['Visuel3']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_F92.".strtolower($filename[1]);

    $target_path = $destination_path_visuel . $newfilename;

    if(!@move_uploaded_file($_FILES['Visuel3']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F92'] = $newfilename ;
    }
}else if(isset($_POST['Visact3']) && !empty($_POST['Visact3'])){
    $TabDescriptif['A00F92'] = $_POST['Visact3'] ;
}else{
    $TabDescriptif['A00F92'] = '' ;
}



if(isset($_FILES['Ambiance1']['name']) && !empty($_FILES['Ambiance1']['name'])){
    $filename = explode(".",basename( $_FILES['Ambiance1']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_F30.".strtolower($filename[1]);

    $target_path = $destination_path_ambiance . $newfilename;

    if(!@move_uploaded_file($_FILES['Ambiance1']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F30'] = $newfilename ;
    }
}else if(isset($_POST['Ambact1']) && !empty($_POST['Ambact1'])){
    $TabDescriptif['A00F30'] = $_POST['Ambact1'] ;
}else{
    $TabDescriptif['A00F30'] = '' ;
}

if(isset($_FILES['Ambiance2']['name']) && !empty($_FILES['Ambiance2']['name'])){
    $filename = explode(".",basename( $_FILES['Ambiance2']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_F91.".strtolower($filename[1]);

    $target_path = $destination_path_ambiance . $newfilename;

    if(!@move_uploaded_file($_FILES['Ambiance2']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F91'] = $newfilename ;
    }
}else if(isset($_POST['Ambact2']) && !empty($_POST['Ambact2'])){
    $TabDescriptif['A00F91'] = $_POST['Ambact2'] ;
}else{
    $TabDescriptif['A00F91'] = '' ;
}


if(isset($_FILES['Ambiance3']['name']) && !empty($_FILES['Ambiance3']['name'])){
    $filename = explode(".",basename( $_FILES['Ambiance3']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_F93.".strtolower($filename[1]);

    $target_path = $destination_path_ambiance . $newfilename;

    if(!@move_uploaded_file($_FILES['Ambiance3']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F93'] = $newfilename ;
    }
}else if(isset($_POST['Ambact3']) && !empty($_POST['Ambact3'])){
    $TabDescriptif['A00F93'] = $_POST['Ambact3'] ;
}else{
    $TabDescriptif['A00F93'] = '' ;
}

if(isset($_FILES['Logo']['name']) && !empty($_FILES['Logo']['name'])){ //Si une nouvelle image a �t� s�lectionn�e
    $filename = explode(".",basename( $_FILES['Logo']['name']));

    $newfilename      = "LOGO".$_SESSION['CURREF'].".".strtolower($filename[1]);

    $target_path = $destination_path_logo . $newfilename;

    if(!@move_uploaded_file($_FILES['Logo']['tmp_name'], $target_path)) {
        $result = 1;
    }else{
        chmod($target_path, 0664);
        $TabDescriptif['A00F40'] = $newfilename ;
    }
}else if(isset($_POST['Logo1']) && !empty($_POST['Logo1'])){ //Si pas de nouvelle image s�lectionn�e mais une image existe d�j�
    $TabDescriptif['A00F40'] = $_POST['Logo1'] ;
}else{
    $TabDescriptif['A00F40'] = '' ; //Aucune image n'existe et aucune n'a �t� s�lectionn�e
}


If (isset($_POST['A00F50'])){$TabDescriptif['A00F50'] = $_POST['A00F50'];}else{ $TabDescriptif['A00F50'] = 0; };
If (isset($_POST['A00F55'])){$TabDescriptif['A00F55'] = $_POST['A00F55'];}else{ $TabDescriptif['A00F55'] = 0; };


If (isset($_POST['datepicker1']) && !empty($_POST['datepicker1']) && trim($_POST['datepicker1']) != '//'){
    $DateCde = $_POST['datepicker1'];
    $Year = substr($DateCde,6,4);
    $Month = substr($DateCde,3,2);
    $Day = substr($DateCde,0,2);
    $DateCde = $Year."-".$Month."-".$Day;
    $TabDescriptif['A00F60'] = $DateCde;
}else{ $TabDescriptif['A00F60'] = '1900-01-01'; };



If (isset($_POST['A00F65'])){$TabDescriptif['A00F65'] = $_POST['A00F65'];}else{ $TabDescriptif['A00F65'] = ''; };

/*error_log($TabDescriptif['A00F20'],0);
error_log($TabDescriptif['A00F90'],0);
error_log($TabDescriptif['A00F92'],0);
error_log($TabDescriptif['A00F30'],0);
error_log($TabDescriptif['A00F91'],0);
error_log($TabDescriptif['A00F93'],0);
error_log($TabDescriptif['A00F40'],0);*/

/*$fp = fopen ("compteur.txt", "a+");
// Instruction 5
fputs ($fp, $TabDescriptif['A00F20']."\n");
fputs ($fp, $TabDescriptif['A00F90']."\n");
fputs ($fp, $TabDescriptif['A00F92']."\n");
fputs ($fp, $TabDescriptif['A00F30']."\n");
fputs ($fp, $TabDescriptif['A00F91']."\n");
fputs ($fp, $TabDescriptif['A00F93']."\n");
fputs ($fp, $TabDescriptif['A00F40']."\n");
fputs ($fp, $TabDescriptif['A00F60']."\n");
fputs ($fp, $TabDescriptif['A00F65']."\n");
fputs ($fp, $TabDescriptif['A00F50']."\n");
fputs ($fp, $TabDescriptif['A00F55']."\n");
fputs ($fp, $_POST['STS']."\n");

fputs ($fp, '-------------------------------------------------------'."\n");

fclose ($fp);*/

$wsdl = WSDIR."majvisuel/wsmajvisuel.php?wsdl";

try {
    $MajVisuel = new SoapClient($wsdl);
    $MajVisuel->majvis($TabDescriptif, $_POST['STS']);
    echo 0;
} catch (Exception $e) {
    echo 999;
}

/*$Result = new nusoap_client($wsdl, true);
$resData = $Result->call('majvis', array(
    'ref_vis' => $TabDescriptif,
    'status'  => $_POST['STS']
));

if( $result != 1){
    
    echo $resData;        
    
}else{
    //Erreur de copie de fichiers physiques
    echo 999;
}*/



?>
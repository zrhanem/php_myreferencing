<?php

@session_start();

require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');
/*
 * Test $_POST. if user is knew then go home.php
 *
 */

If (isset($_POST['status'])) { 
    $status = trim($_POST['status']);
    $idfrs = $_SESSION['IDFRS'];
    

    if ($status == "") {
        echo '2';
    } else {

        $retour = '';
        
        $wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

        $Result = new nusoap_client($wsdl, true);
        $TabData = $Result->call('Listerreference', array(
            'sidentifiant' => $idfrs,
            'iStatus' => $status
        ));

                
        if (!empty ($TabData) ){
        
            
            foreach ($TabData as $ligne) {
                
                    $retour .= '<div id="store-1" class="supervisor-list__row">';
                    $retour .= '<div class="supervisor-list__cell supervisor-list__cell--store">'.trim($ligne['C00001']).'</div>';
                    $retour .= '<div class="supervisor-list__cell supervisor-list__cell--num">'.trim($ligne['C00002']).'</div>';
                    $retour .= '<div class="supervisor-list__cell supervisor-list__cell--city">'.trim($ligne['C00003']).'</div>';
                    $retour .= '<div class="supervisor-list__cell supervisor-list__cell--city">'.trim($ligne['donnee']).'</div>';
                    $retour .= '<div class="supervisor-list__cell supervisor-list__cell--num">'.trim($ligne['C00026']).'</div>';
                            
                    if ($status == 1){
                        $_SESSION['MODIF'] = 'O';
                        $retour .= '<button type="button" data-toggle="collapse" data-target="#ActChoices-'.trim($ligne['C00016']).'" class="btn btn-default collapsed"><span>'._("Modifier ").'</span></button>';                        
                        $retour .= '<div class="btn-group">';
                        $retour .= '<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'._("Actions").'<span class="caret"></span></a>';
                        $retour .= '<ul class="dropdown-menu" role="menu">';
                        $retour .= '<li><a href="#" id="Copy-'.trim($ligne['C00016']).'">'._("Copier").'</a></li>';                        
                        if(trim($ligne['COMPLET']) == 'O' ){
                            $retour .= '<li><a href="#" >'._("Valider et transmettre").'</a></li>';
                        }                                                
                        $retour .= '</ul>';
                        $retour .= '</div>';
                        if(trim($ligne['COMPLET']) == 'O' ){
                            $retour .= '<span class="label label-success">'._("R&eacute;f&eacute;rence compl&egrave;te").'</span>';
                        }else{
                            $retour .= '<span class="label label-danger">'._("R&eacute;f&eacute;rence incompl&egrave;te").'</span>';
                        }                        
                    }else{
                        $_SESSION['MODIF'] = 'N';
                        $retour .= '<button type="button" data-toggle="collapse" data-target="#ActChoices-'.trim($ligne['C00016']).'" class="btn btn-default collapsed"><span>'._("Consulter ").'</span></button>';
                    }
                    
                    
                    $retour .= '<div class="panel">';
                    $retour .= '<div id="ActChoices-'.trim($ligne['C00016']).'" class="supervisor-stores__panel supervisor-stores__panel--orders collapse">';
                    $retour .= '<div class="supervisor-stores__panel-container">';
                    $retour .= '<div class="quick-links-wrapper">';
                    $retour .= '<div class="quick-links">';
                    $retour .= '<div class="quick-links__container">';
                    $retour .= '<div class="quick-links__item quick-links__item--reload" style=background-color: #0000FF;><a href="descriptif.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-reload"></i><span>'._("Descriptif").'</span></a></div>';
                    $retour .= '<div class="quick-links__item quick-links__item--catalog"><a href="condition.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-cart"></i><span>'._("Condition").'</em></span></a></div>';
                    $retour .= '<div class="quick-links__item quick-links__item--catalog"><a href="visuels.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-catalog"></i><span>'._("Visuels").'</span></a></div>';
                    $retour .= '<div class="quick-links__item quick-links__item--reload"><a href="securite.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-star"></i><span>'._("S�curit�").'</span></a></div>';
                    $retour .= '</div>';
                    $retour .= '<div class="quick-links__container">';
                    $retour .= '<div class="quick-links__item quick-links__item--reload"><a href="logistique.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-star"></i><span>'._("Logistique").'</span></a></div>';
                    $retour .= '<div class="quick-links__item quick-links__item--catalog"><a href="marquage.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-kits"></i><span>'._("Marquage et garantie").'</span></a></div>';
                    $retour .= '<div class="quick-links__item quick-links__item--catalog"><a href="ecopart.php?ref='.trim($ligne['C00016']).'"><i class="icon icon-validate"></i><span>'._("Eco-participation").'</span></a></div>';                    
                    $retour .= '</div>';
                    $retour .= '</div>';
                    $retour .= '</div>';
                    $retour .= '<button type="button" data-toggle="collapse" data-target="#ActChoices-'.trim($ligne['C00016']).'" class="close"><span aria-hidden="true">&times;</span></button>';
                    $retour .= '</div>';
                    $retour .= '</div>';
                    $retour .= '</div>';                                     
                    $retour .= '</div>';
                    
            }
                        
        }else{
            
            $retour .= '<div id="store-1" class="supervisor-list__row">';
            $retour .= '<div class="supervisor-list__cell supervisor-list__cell--city">'._("Aucune r&eacute;f&eacute;rence.").'</div>';
            $retour .= '</div>';            
        
        }

        
     echo $retour;    
        
    }
} else {
    /* Error */
    echo '4';
}

?>
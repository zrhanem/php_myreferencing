<?php
//require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

$result = 0;

$TabDescriptif = array();
$TabDescriptif['idpref'] = $_SESSION['CURREF'];

$destination_path = "../Uploaded_files/Docs/Pieces_Detache/";


if(isset( $_FILES['Ficheeclpec']['name']) && !empty( $_FILES['Ficheeclpec']['name'])){
    $filename = explode(".",basename(  $_FILES['Ficheeclpec']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_eclates.".$filename[1];

    $target_path = $destination_path . $newfilename;

    if(!@move_uploaded_file($_FILES['Ficheeclpec']['tmp_name'], $target_path)) {
        $result = 1;
    }else {
        $TabDescriptif['A00E45'] = $newfilename ;
    }
}else if(isset($_POST['FEP']) && !empty($_POST['FEP'])){
    $TabDescriptif['A00E45'] = $_POST['FEP'];
}else{
    $TabDescriptif['A00E45'] = '' ;
}

$destination_path = "../Uploaded_files/Docs/Mode_Emploi/";

if(isset( $_FILES['FicheME']['name']) && !empty( $_FILES['FicheME']['name'])){
    $filename = explode(".",basename(  $_FILES['FicheME']['name']));

    $newfilename      = "ECF_".$_SESSION['CURREF']."_modeemploi.".$filename[1];

    $target_path = $destination_path . $newfilename;

    if(!@move_uploaded_file($_FILES['FicheME']['tmp_name'], $target_path)) {
        $result = 1;
    }else {
        $TabDescriptif['A00E50'] = $newfilename ;
    }
}else if(isset($_POST['ME']) && !empty($_POST['ME'])){
    $TabDescriptif['A00E50'] = $_POST['ME'];
}else{
    $TabDescriptif['A00E50'] = '' ;
}


If (isset($_POST['X00001'])){$TabDescriptif['X00001'] = 1;}else{ $TabDescriptif['X00001'] = 0; };
If (isset($_POST['X00002'])){$TabDescriptif['X00002'] = 1;}else{ $TabDescriptif['X00002'] = 0; };
If (isset($_POST['X00003'])){$TabDescriptif['X00003'] = 1;}else{ $TabDescriptif['X00003'] = 0; };
If (isset($_POST['X00004'])){$TabDescriptif['X00004'] = 1;}else{ $TabDescriptif['X00004'] = 0; };
If (isset($_POST['X00005'])){$TabDescriptif['X00005'] = 1;}else{ $TabDescriptif['X00005'] = 0; };
If (isset($_POST['X00006'])){$TabDescriptif['X00006'] = 1;}else{ $TabDescriptif['X00006'] = 0; };
If (isset($_POST['X00007'])){$TabDescriptif['X00007'] = 1;}else{ $TabDescriptif['X00007'] = 0; };
If (isset($_POST['X00008'])){$TabDescriptif['X00008'] = 1;}else{ $TabDescriptif['X00008'] = 0; };
If (isset($_POST['X00009'])){$TabDescriptif['X00009'] = 1;}else{ $TabDescriptif['X00009'] = 0; };
If (isset($_POST['X00010'])){$TabDescriptif['X00010'] = 1;}else{ $TabDescriptif['X00010'] = 0; };
If (isset($_POST['X00011'])){$TabDescriptif['X00011'] = 1;}else{ $TabDescriptif['X00011'] = 0; };
If (isset($_POST['X00012'])){$TabDescriptif['X00012'] = 1;}else{ $TabDescriptif['X00012'] = 0; };
If (isset($_POST['X00013'])){$TabDescriptif['X00013'] = 1;}else{ $TabDescriptif['X00013'] = 0; };
If (isset($_POST['X00014'])){$TabDescriptif['X00014'] = 1;}else{ $TabDescriptif['X00014'] = 0; };
If (isset($_POST['X00015'])){$TabDescriptif['X00015'] = 1;}else{ $TabDescriptif['X00015'] = 0; };
If (isset($_POST['X00016'])){$TabDescriptif['X00016'] = 1;}else{ $TabDescriptif['X00016'] = 0; };
If (isset($_POST['A00D90'])){$TabDescriptif['A00D90'] = $_POST['A00D90'];}else{ $TabDescriptif['A00D90'] = 0; };
If (isset($_POST['A00D95'])){$TabDescriptif['A00D95'] = $_POST['A00D95'];}else{ $TabDescriptif['A00D95'] = 0; };
If (isset($_POST['A00E10'])){$TabDescriptif['A00E10'] = $_POST['A00E10'];}else{ $TabDescriptif['A00E10'] = 0; };
If (isset($_POST['A00E25']) && !empty($_POST['A00E25'])){$TabDescriptif['A00E25'] = $_POST['A00E25'];}else{ $TabDescriptif['A00E25'] = 0; };
If (isset($_POST['A00E30'])){$TabDescriptif['A00E30'] = $_POST['A00E30'];}else{ $TabDescriptif['A00E30'] = 0; };
If (isset($_POST['A00E40'])){$TabDescriptif['A00E40'] = $_POST['A00E40'];}else{ $TabDescriptif['A00E40'] = 0; };
If (isset($_POST['A00E55'])){$TabDescriptif['A00E55'] = $_POST['A00E55'];}else{ $TabDescriptif['A00E55'] = 0; };
If (isset($_POST['A00E60'])){$TabDescriptif['A00E60'] = $_POST['A00E60'];}else{ $TabDescriptif['A00E60'] = ''; };
If (isset($_POST['DUGAMO']) && !empty($_POST['DUGAMO'])){$TabDescriptif['DUGAMO'] = $_POST['DUGAMO'];}else{ $TabDescriptif['DUGAMO'] = 0; };
If (isset($_POST['DEPLAC'])){$TabDescriptif['DEPLAC'] = $_POST['DEPLAC'];}else{ $TabDescriptif['DEPLAC'] = ''; };
If (isset($_POST['MA00E35'])){$TabDescriptif['MA00E35'] = $_POST['MA00E35'];}else{ $TabDescriptif['MA00E35'] = ''; };


$wsdl = WSDIR."majmarquage/wsmajmarquage.php?wsdl";

try {
    $MajMarquage = new SoapClient($wsdl);
    $MajMarquage->majmarquage($TabDescriptif, $_POST['STS']);
    echo 0;
} catch (Exception $e) {
    echo 999;
}


/*$Result = new nusoap_client($wsdl, true);
$resData = $Result->call('majmarquage', array(
    'ref_mar' => $TabDescriptif,
    'status'  => $_POST['STS']
));

if( $result != 1){

    echo $resData;

}else{
    //Erreur de copie de fichiers physiques
    echo 999;
}*/




?>
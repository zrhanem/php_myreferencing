<?php

require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

if(isset($_POST['idref']) && !empty($_POST['idref'])){
    
    $result = '';
    
    //Fiche produits    
    $dir = '../Uploaded_files/Docs/Fiche_Produits/';
    $dstfile='../transmitted_files/Docs/Fiche_Produits/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_FicheProd"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_FicheProd.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_FicheProd.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }
    
    //Pieces detach�
    $dir = '../Uploaded_files/Docs/Pieces_Detache/';
    $dstfile='../transmitted_files/Docs/Pieces_Detache/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_eclates"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_eclates.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_eclates.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }

    //mode emploi
    $dir = '../Uploaded_files/Docs/Mode_Emploi/';
    $dstfile='../transmitted_files/Docs/Mode_Emploi/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_modeemploi"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_modeemploi.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_modeemploi.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }
    
    
    //ce
    $dir = '../Uploaded_files/Docs/ce/';
    $dstfile='../transmitted_files/Docs/ce/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_certificataliment"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_certificataliment.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_certificataliment.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }
    
    //ineris
    $dir = '../Uploaded_files/Docs/Ineris/';
    $dstfile='../transmitted_files/Docs/Ineris/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_ineris"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_ineris.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_ineris.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }

    //securit�
    $dir = '../Uploaded_files/Docs/Fiche_Securite/';
    $dstfile='../transmitted_files/Docs/Fiche_Securite/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_FicheSecurite"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_FicheSecurite.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_FicheSecurite.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }
    
    //fiche tech
    $dir = '../Uploaded_files/Docs/Fiche_Tech/';
    $dstfile='../transmitted_files/Docs/Fiche_Tech/';
    $dh = opendir($dir);
    if ($dh !== false) {
    
        // boucler tant que quelque chose est trouve
        while (($file = readdir($dh)) !== false) {
    
            $filename = explode(".",basename($file));
            if($filename[0] == "ECF_".$_POST['idref']."_FicheTechnique"){
                if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_FicheTechnique.".$filename[1])) {
                    $result .= "La copie du fichier ".$file." a �chou�...\n";
                }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_FicheTechnique.".$filename[1] , 0664);
                }
                break;
            }
        }
         
        // on ferme la connection
        closedir($dh);
    }
   
    //Visuels visuels       
   $dir = '../Uploaded_files/Images/Visuels/';
   $dstfile='../transmitted_files/Images/Visuels/';
   $dh = opendir($dir);
   if ($dh !== false) {

       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
            
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F20"){               
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F20.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F20.".$filename[1] , 0664);
                } 
               break;
           }                      
       }
       
       // on ferme la connection
       closedir($dh);
   }

   $dh = opendir($dir);
   if ($dh !== false) {

        
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
   
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F90"){
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F90.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F90.".$filename[1] , 0664);
                }
               break;
           }
       }
        
       // on ferme la connection
       closedir($dh);
   
   }
   
   $dh = opendir($dir);
   if ($dh !== false) {
        
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
   
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F92"){
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F92.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F92.".$filename[1] , 0664);
                }
               break;
           }
       }
        
       // on ferme la connection
       closedir($dh);
   
   }
    

   //Visuels Ambiance
   $dir = '../Uploaded_files/Images/Ambiances/';
   $dstfile='../transmitted_files/Images/Ambiances/';
   $dh = opendir($dir);
   if ($dh !== false) {
   
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
   
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F30"){
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F30.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F30.".$filename[1] , 0664);
                }
               break;
           }
       }
        
       // on ferme la connection
       closedir($dh);
   }
   
   $dh = opendir($dir);
   if ($dh !== false) {
   
   
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
            
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F91"){
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F91.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F91.".$filename[1] , 0664);
                }
               break;
           }
       }
   
       // on ferme la connection
       closedir($dh);
        
   }
    
   $dh = opendir($dir);
   if ($dh !== false) {
   
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
            
           $filename = explode(".",basename($file));
           if($filename[0] == "ECF_".$_POST['idref']."_F93"){
               if (!copy($dir.$file, $dstfile."ECF_".$_POST['idref']."_F93.".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."ECF_".$_POST['idref']."_F93.".$filename[1] , 0664);
                }
               break;
           }
       }
   
       // on ferme la connection
       closedir($dh);
        
   }
    
   //Visuels Logo
   $dir = '../Uploaded_files/Images/Logos/';
   $dstfile='../transmitted_files/Images/Logos/';
   $dh = opendir($dir);
   if ($dh !== false) {
        
       // boucler tant que quelque chose est trouve
       while (($file = readdir($dh)) !== false) {
            
           $filename = explode(".",basename($file));
           if($filename[0] == "LOGO".$_POST['idref']){
               if (!copy($dir.$file, $dstfile."LOGO".$_POST['idref'].".".$filename[1])) {
                   $result .= "La copie du fichier ".$file." a �chou�...\n";
               }else{
                    chmod( $dstfile."LOGO".$_POST['idref'].".".$filename[1] , 0664);
                }
               break;
           }
       }
   
       // on ferme la connection
       closedir($dh);
   }

          
    if($result == ''){
        
        $wsdl = WSDIR."enrtransmission/wsenrtransmission.php?wsdl";
        $Result = new nusoap_client($wsdl, true);
        $resData = $Result->call('transmettre', array(
            'idpref' => trim($_POST['idref']) ,
            'idfrs' => $_SESSION['IDFRS'] 
        ));
        
        echo $resData;
        
    }else{
        echo '4|'.$result;
    }  
    

  
    
}else{
    echo '2';
}



?>


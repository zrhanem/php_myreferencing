<?php

//require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

$result = 0;

$TabDescriptif = array();
$TabDescriptif['idpref'] = $_SESSION['CURREF'];
 

///////////ECF
If (isset($_POST['A00A25']) && !empty($_POST['A00A25'])){$TabDescriptif['A00A25'] = $_POST['A00A25'];}else{ $TabDescriptif['A00A25'] = ''; };//Unit� d'achat
If (isset($_POST['A00A26']) && !empty($_POST['A00A26'])){$TabDescriptif['A00A26'] = $_POST['A00A26'];}else{ $TabDescriptif['A00A26'] = ''; };//autre(unit� d'achat)
If (isset($_POST['A00A30']) && !empty($_POST['A00A30'])){$TabDescriptif['A00A30'] = $_POST['A00A30'];}else{ $TabDescriptif['A00A30'] = 0 ; };//Nb pi�ces par unit� d'achat
If (isset($_POST['A00A35']) && !empty($_POST['A00A35'])){$TabDescriptif['A00A35'] = $_POST['A00A35'];}else{ $TabDescriptif['A00A35'] = ''; };//Unit� de facturation
If (isset($_POST['A00A36']) && !empty($_POST['A00A36'])){$TabDescriptif['A00A36'] = $_POST['A00A36'];}else{ $TabDescriptif['A00A36'] = ''; };//autre(unit� facturation)
If (isset($_POST['A00A40']) && !empty($_POST['A00A40'])){$TabDescriptif['A00A40'] = $_POST['A00A40'];}else{ $TabDescriptif['A00A40'] = 0 ; };//Nb pi�ces par unit� de facturation
If (isset($_POST['A00A45']) && !empty($_POST['A00A45'])){$TabDescriptif['A00A45'] = $_POST['A00A45'];}else{ $TabDescriptif['A00A45'] = 0 ; };//Prix achat par unit� de facturation
If (isset($_POST['FRANC1']) && !empty($_POST['FRANC1'])){$TabDescriptif['FRANC1'] = $_POST['FRANC1'];}else{ $TabDescriptif['FRANC1'] = 0 ; };//Franco
If (isset($_POST['A00A65']) && !empty($_POST['A00A65'])){$TabDescriptif['A00A65'] = $_POST['A00A65'];}else{ $TabDescriptif['A00A65'] = 0 ; };//Remise1
If (isset($_POST['A00A60']) && !empty($_POST['A00A60'])){$TabDescriptif['A00A60'] = $_POST['A00A60'];}else{ $TabDescriptif['A00A60'] = 0 ; };//rem1
If (isset($_POST['A00A67']) && !empty($_POST['A00A67'])){$TabDescriptif['A00A75'] = $_POST['A00A67'];}else{ $TabDescriptif['A00A75'] = 0 ; };//Remise2
If (isset($_POST['A00A70']) && !empty($_POST['A00A70'])){$TabDescriptif['A00A70'] = $_POST['A00A70'];}else{ $TabDescriptif['A00A70'] = 0 ; };//rem2
If (isset($_POST['A00A85']) && !empty($_POST['A00A85'])){$TabDescriptif['A00A85'] = $_POST['A00A85'];}else{ $TabDescriptif['A00A85'] = 0 ; };//Remise3
If (isset($_POST['A00A80']) && !empty($_POST['A00A80'])){$TabDescriptif['A00A80'] = $_POST['A00A80'];}else{ $TabDescriptif['A00A80'] = 0 ; };//rem3
If (isset($_POST['A00A95']) && !empty($_POST['A00A95'])){$TabDescriptif['A00A95'] = $_POST['A00A95'];}else{ $TabDescriptif['A00A95'] = 0 ; };//Remise4
If (isset($_POST['A00A90']) && !empty($_POST['A00A90'])){$TabDescriptif['A00A90'] = $_POST['A00A90'];}else{ $TabDescriptif['A00A90'] = 0 ; };//rem4
If (isset($_POST['A00A50']) && !empty($_POST['A00A50'])){$TabDescriptif['A00A50'] = $_POST['A00A50'];}else{ $TabDescriptif['A00A50'] = 0 ; };//Eco participation � la pi�ce(DEEE)
If (isset($_POST['A00A55']) && !empty($_POST['A00A55'])){$TabDescriptif['A00A55'] = $_POST['A00A55'];}else{ $TabDescriptif['A00A55'] = 0 ; };//Taxe g�n�rale sur les activit� polluantes(TGAP)
If (isset($_POST['TVAPRD']) && !empty($_POST['TVAPRD'])){$TabDescriptif['TVAPRD'] = $_POST['TVAPRD'];}else{ $TabDescriptif['TVAPRD'] = 0 ; };//TVA
If (isset($_POST['A00B10']) && !empty($_POST['A00B10'])){$TabDescriptif['A00B10'] = $_POST['A00B10'];}else{ $TabDescriptif['A00B10'] = 0 ; };//Sueil minimum de commande
If (isset($_POST['A00B25']) && !empty($_POST['A00B25'])){$TabDescriptif['A00B25'] = $_POST['A00B25'];}else{ $TabDescriptif['A00B25'] = 0 ; };//Quantit� multiple achat
If (isset($_POST['A00F80']) && !empty($_POST['A00F80'])){$TabDescriptif['A00F80'] = $_POST['A00F80'];}else{ $TabDescriptif['A00F80'] = 0 ; };//Quantit� multiple achat
If (isset($_POST['A00B15']) && !empty($_POST['A00B15'])){$TabDescriptif['A00B15'] = $_POST['A00B15'];}else{ $TabDescriptif['A00B15'] = 0 ; };//PPI
If (isset($_POST['COMENT']) && !empty($_POST['COMENT'])){$TabDescriptif['COMENT'] = $_POST['COMENT'];}else{ $TabDescriptif['COMENT'] = ''; };//Commentaire sur prix
///////////////////ECOTEL
If (isset($_POST['EJRVNN6']) && !empty($_POST['EJRVNN6'])){$TabDescriptif['EJRVNN6'] = $_POST['EJRVNN6'];}else{ $TabDescriptif['EJRVNN6'] = 0; };//Livraison directe?
If (isset($_POST['EJRAZP4']) && !empty($_POST['EJRAZP4'])){$TabDescriptif['EJRAZP4'] = $_POST['EJRAZP4'];}else{ $TabDescriptif['EJRAZP4'] = 0; };//Prix d'achat dans l'unit� facturation
If (isset($_POST['EJRA1P4']) && !empty($_POST['EJRA1P4'])){$TabDescriptif['EJRA1P4'] = $_POST['EJRA1P4'];}else{ $TabDescriptif['EJRA1P4'] = 0; };//Franco
If (isset($_POST['EJRRUPC']) && !empty($_POST['EJRRUPC'])){$TabDescriptif['EJRRUPC'] = $_POST['EJRRUPC'];}else{ $TabDescriptif['EJRRUPC'] = 0; };//Remise1
If (isset($_POST['EJRVON6']) && !empty($_POST['EJRVON6'])){$TabDescriptif['EJRVON6'] = $_POST['EJRVON6'];}else{ $TabDescriptif['EJRVON6'] = 0; };//rem1
If (isset($_POST['EJRRVPC']) && !empty($_POST['EJRRVPC'])){$TabDescriptif['EJRRVPC'] = $_POST['EJRRVPC'];}else{ $TabDescriptif['EJRRVPC'] = 0; };//Remise2
If (isset($_POST['EJRVPN6']) && !empty($_POST['EJRVPN6'])){$TabDescriptif['EJRVPN6'] = $_POST['EJRVPN6'];}else{ $TabDescriptif['EJRVPN6'] = 0; };//rem2
If (isset($_POST['EJRRWPC']) && !empty($_POST['EJRRWPC'])){$TabDescriptif['EJRRWPC'] = $_POST['EJRRWPC'];}else{ $TabDescriptif['EJRRWPC'] = 0; };//Remise3
If (isset($_POST['EJRVQN6']) && !empty($_POST['EJRVQN6'])){$TabDescriptif['EJRVQN6'] = $_POST['EJRVQN6'];}else{ $TabDescriptif['EJRVQN6'] = 0; };//rem3
If (isset($_POST['EJRRXPC']) && !empty($_POST['EJRRXPC'])){$TabDescriptif['EJRRXPC'] = $_POST['EJRRXPC'];}else{ $TabDescriptif['EJRRXPC'] = 0; };//Remise4
If (isset($_POST['EJRVRN6']) && !empty($_POST['EJRVRN6'])){$TabDescriptif['EJRVRN6'] = $_POST['EJRVRN6'];}else{ $TabDescriptif['EJRVRN6'] = 0; };//rem4
If (isset($_POST['EJRQ7Q1']) && !empty($_POST['EJRQ7Q1'])){$TabDescriptif['EJRQ7Q1'] = $_POST['EJRQ7Q1'];}else{ $TabDescriptif['EJRQ7Q1'] = 0; };//Sueil minimum de commande
If (isset($_POST['EJRAXN7']) && !empty($_POST['EJRAXN7'])){$TabDescriptif['EJRAXN7'] = $_POST['EJRAXN7'];}else{ $TabDescriptif['EJRAXN7'] = ''; };//Commentaire sur prix
///////LACORPO
If (isset($_POST['LJRVNN6']) && !empty($_POST['LJRVNN6'])){$TabDescriptif['LJRVNN6'] = $_POST['LJRVNN6'];}else{ $TabDescriptif['LJRVNN6'] = 0; };//Livraison directe?
If (isset($_POST['LJRAZP4']) && !empty($_POST['LJRAZP4'])){$TabDescriptif['LJRAZP4'] = $_POST['LJRAZP4'];}else{ $TabDescriptif['LJRAZP4'] = 0; };//Prix d'achat dans l'unit� facturation
If (isset($_POST['LJRA1P4']) && !empty($_POST['LJRA1P4'])){$TabDescriptif['LJRA1P4'] = $_POST['LJRA1P4'];}else{ $TabDescriptif['LJRA1P4'] = 0; };//Franco
If (isset($_POST['LJRRUPC']) && !empty($_POST['LJRRUPC'])){$TabDescriptif['LJRRUPC'] = $_POST['LJRRUPC'];}else{ $TabDescriptif['LJRRUPC'] = 0; };//Remise1
If (isset($_POST['LJRVON6']) && !empty($_POST['LJRVON6'])){$TabDescriptif['LJRVON6'] = $_POST['LJRVON6'];}else{ $TabDescriptif['LJRVON6'] = 0; };//rem1
If (isset($_POST['LJRRVPC']) && !empty($_POST['LJRRVPC'])){$TabDescriptif['LJRRVPC'] = $_POST['LJRRVPC'];}else{ $TabDescriptif['LJRRVPC'] = 0; };//Remise2
If (isset($_POST['LJRVPN6']) && !empty($_POST['LJRVPN6'])){$TabDescriptif['LJRVPN6'] = $_POST['LJRVPN6'];}else{ $TabDescriptif['LJRVPN6'] = 0; };//rem2
If (isset($_POST['LJRRWPC']) && !empty($_POST['LJRRWPC'])){$TabDescriptif['LJRRWPC'] = $_POST['LJRRWPC'];}else{ $TabDescriptif['LJRRWPC'] = 0; };//Remise3
If (isset($_POST['LJRVQN6']) && !empty($_POST['LJRVQN6'])){$TabDescriptif['LJRVQN6'] = $_POST['LJRVQN6'];}else{ $TabDescriptif['LJRVQN6'] = 0; };//rem3
If (isset($_POST['LJRRXPC']) && !empty($_POST['LJRRXPC'])){$TabDescriptif['LJRRXPC'] = $_POST['LJRRXPC'];}else{ $TabDescriptif['LJRRXPC'] = 0; };//Remise4
If (isset($_POST['LJRVRN6']) && !empty($_POST['LJRVRN6'])){$TabDescriptif['LJRVRN6'] = $_POST['LJRVRN6'];}else{ $TabDescriptif['LJRVRN6'] = 0; };//rem4
If (isset($_POST['LJRQ7Q1']) && !empty($_POST['LJRQ7Q1'])){$TabDescriptif['LJRQ7Q1'] = $_POST['LJRQ7Q1'];}else{ $TabDescriptif['LJRQ7Q1'] = 0; };//Sueil minimum de commande
If (isset($_POST['CJRAXN7']) && !empty($_POST['CJRAXN7'])){$TabDescriptif['CJRAXN7'] = $_POST['CJRAXN7'];}else{ $TabDescriptif['CJRAXN7'] = ''; };//Commentaire sur prix


/*error_log($TabDescriptif['A00A25'],0);
error_log($TabDescriptif['A00A26'],0);
error_log($TabDescriptif['A00A30'],0);
error_log($TabDescriptif['A00A35'],0);
error_log($TabDescriptif['A00A36'],0);
error_log($TabDescriptif['A00A40'],0);
error_log($TabDescriptif['A00A45'],0);
error_log($TabDescriptif['FRANC1'],0);
error_log($TabDescriptif['A00A65'],0);
error_log($TabDescriptif['A00A60'],0);
error_log($TabDescriptif['A00A75'],0);
error_log($TabDescriptif['A00A70'],0);
error_log($TabDescriptif['A00A85'],0);
error_log($TabDescriptif['A00A80'],0);
error_log($TabDescriptif['A00A95'],0);
error_log($TabDescriptif['A00A90'],0);
error_log($TabDescriptif['A00A50'],0);
error_log($TabDescriptif['A00A55'],0);
error_log($TabDescriptif['TVAPRD'],0);
error_log($TabDescriptif['A00B10'],0);
error_log($TabDescriptif['A00B25'],0);
error_log($TabDescriptif['A00F80'],0);
error_log($TabDescriptif['A00B15'],0);
error_log($TabDescriptif['COMENT'],0);
error_log($TabDescriptif['EJRVNN6'],0);
error_log($TabDescriptif['EJRAZP4'],0);
error_log($TabDescriptif['EJRA1P4'],0);
error_log($TabDescriptif['EJRRUPC'],0);
error_log($TabDescriptif['EJRVON6'],0);
error_log($TabDescriptif['EJRRVPC'],0);
error_log($TabDescriptif['EJRVPN6'],0);
error_log($TabDescriptif['EJRRWPC'],0);
error_log($TabDescriptif['EJRVQN6'],0);
error_log($TabDescriptif['EJRRXPC'],0);
error_log($TabDescriptif['EJRVRN6'],0);
error_log($TabDescriptif['EJRQ7Q1'],0);
error_log($TabDescriptif['EJRAXN7'],0);
error_log($TabDescriptif['LJRVNN6'],0);
error_log($TabDescriptif['LJRAZP4'],0);
error_log($TabDescriptif['LJRA1P4'],0);
error_log($TabDescriptif['LJRRUPC'],0);
error_log($TabDescriptif['LJRVON6'],0);
error_log($TabDescriptif['LJRRVPC'],0);
error_log($TabDescriptif['LJRVPN6'],0);
error_log($TabDescriptif['LJRRWPC'],0);
error_log($TabDescriptif['LJRVQN6'],0);
error_log($TabDescriptif['LJRRXPC'],0);
error_log($TabDescriptif['LJRVRN6'],0);
error_log($TabDescriptif['LJRQ7Q1'],0);
error_log($TabDescriptif['CJRAXN7'],0);*/

/*$fp = fopen ("compteur.txt", "a+");
// Instruction 5
fputs ($fp, $TabDescriptif['A00A25']."\n");
fputs ($fp, $TabDescriptif['A00A26']."\n");
fputs ($fp, $TabDescriptif['A00A30']."\n");
fputs ($fp, $TabDescriptif['A00A35']."\n");
fputs ($fp, $TabDescriptif['A00A36']."\n");
fputs ($fp, $TabDescriptif['A00A40']."\n");
fputs ($fp, $TabDescriptif['A00A45']."\n");
fputs ($fp, $TabDescriptif['FRANC1']."\n");
fputs ($fp, $TabDescriptif['A00A65']."\n");
fputs ($fp, $TabDescriptif['A00A60']."\n");
fputs ($fp, $TabDescriptif['A00A70']."\n");
fputs ($fp, $TabDescriptif['A00A75']."\n");
fputs ($fp, $TabDescriptif['A00A80']."\n");
fputs ($fp, $TabDescriptif['A00A85']."\n");
fputs ($fp, $TabDescriptif['A00A90']."\n");
fputs ($fp, $TabDescriptif['A00A95']."\n");
fputs ($fp, $TabDescriptif['A00A50']."\n");
fputs ($fp, $TabDescriptif['A00A55']."\n");
fputs ($fp, $TabDescriptif['TVAPRD']."\n");
fputs ($fp, $TabDescriptif['A00B10']."\n");
fputs ($fp, $TabDescriptif['A00B25']."\n");
fputs ($fp, $TabDescriptif['A00F80']."\n");
fputs ($fp, $TabDescriptif['A00B15']."\n");
fputs ($fp, $TabDescriptif['COMENT']."\n");
fputs ($fp, $TabDescriptif['idpref']."\n");
fputs ($fp, '-------------------------------------------------------'."\n");

fclose ($fp);*/




$wsdl = WSDIR."majcondition/wsmajcondition.php?wsdl";


try {
    $MajCondition = new SoapClient($wsdl);
    $MajCondition->majcond($TabDescriptif, $_POST['STS']);
    echo 0;
} catch (Exception $e) {
    echo 1;
}



/*

$Result = new nusoap_client($wsdl, true);
$resData = $Result->call('majcond', array(
    'ref_cond' => $TabDescriptif,
        'status'  => $_POST['STS']
));

if($resData == 0){

}else{
    $result = $resData;
}

echo $result; 
*/

?>

<?php

//require_once ('../bibliotheque/nusoap/lib/nusoap.php');
require_once ('../inc/config.inc.php');

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

$TabDescriptif = array();
$TabDescriptif['idpref'] = $_SESSION['CURREF'];


if(isset($_POST['A00C90']) && !empty($_POST['A00C90'])){
    $TabDescriptif['A00C90'] = $_POST['A00C90'];
}else{
    $TabDescriptif['A00C90'] = '';
}
if(isset($_POST['A00C92']) && !empty($_POST['A00C92'])){
    $TabDescriptif['A00C92'] = $_POST['A00C92'];
}else{
    $TabDescriptif['A00C92'] = '';
}
if(isset($_POST['A00C95']) && !empty($_POST['A00C95'])){
    $TabDescriptif['A00C95'] = $_POST['A00C95'];
}else{
    $TabDescriptif['A00C95'] = '';
}
if(isset($_POST['A00D10']) && !empty($_POST['A00D10'])){
    $TabDescriptif['A00D10'] = $_POST['A00D10'];
}else{
    $TabDescriptif['A00D10'] = '';
}
if(isset($_POST['A00C25']) && !empty($_POST['A00C25'])){
    $TabDescriptif['A00C25'] = $_POST['A00C25'];
}else{
    $TabDescriptif['A00C25'] = 0 ;
}
if(isset($_POST['A00C50']) && !empty($_POST['A00C50'])){
    $TabDescriptif['A00C50'] = $_POST['A00C50'];
}else{
    $TabDescriptif['A00C50'] = 0 ;
}
if(isset($_POST['PAL']) && !empty($_POST['PAL'])){
    $TabDescriptif['A00C75'] = $_POST['PAL'];
}else{
    $TabDescriptif['A00C75'] = 0 ;
}
if(isset($_POST['VOLLIT']) && !empty($_POST['VOLLIT'])){
    $TabDescriptif['VOLLIT'] = $_POST['VOLLIT'];
}else{
    $TabDescriptif['VOLLIT'] = 0 ;
}

if(isset($_POST['NOMNTR']) && !empty($_POST['NOMNTR'])){
    $TabDescriptif['NOMNTR'] = $_POST['NOMNTR'];
}else{
    $TabDescriptif['NOMNTR'] = '';
}
if(isset($_POST['RESPAL']) && !empty($_POST['RESPAL'])){
    $TabDescriptif['RESPAL'] = $_POST['RESPAL'];
}else{
    $TabDescriptif['RESPAL'] = 0 ;
}
if(isset($_POST['PALSEP']) && !empty($_POST['PALSEP'])){
    $TabDescriptif['PALSEP'] = $_POST['PALSEP'];
}else{
    $TabDescriptif['PALSEP'] = 0 ;
}
if(isset($_POST['MRQCNF']) && !empty($_POST['MRQCNF'])){
    $TabDescriptif['MRQCNF'] = $_POST['MRQCNF'];
}else{
    $TabDescriptif['MRQCNF'] = 0 ;
}

$wsdl = WSDIR."majlogistique/wsmajlogistique.php?wsdl";

try {
    $MajLogistique = new SoapClient($wsdl);
    $MajLogistique->majlog($TabDescriptif, $_POST['STS']);
    echo 0;
} catch (Exception $e) {    
    echo 1;
}




/*$Result = new nusoap_client($wsdl, true);
$resData = $Result->call('majlog', array(
    'ref_log' => $TabDescriptif,
    'status'  => $_POST['STS']
));

if($resData == 0){
    echo 0;
}else{
    echo 1;
}*/


?>
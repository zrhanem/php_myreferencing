<?php

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

$smarty->assign('DESFR1',$_SESSION['DESFR1']);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

/*$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listersecurite', array(
    'pre_ref' => $ref
));*/

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Listersecurite($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){   
    $smarty->assign('Lstsecur',$TabData);
    $smarty->assign('QTEM1',round(trim($TabData['QTEMD1']), 2));
    
    if($TabData['A00D40'] != ''){
        $btrouve = 0;
        $file='';
        $handle = opendir('Uploaded_files/Docs/ce/');
        
        if ($handle) {
        
            //    Ceci est la façon correcte de traverser un dossier.
            while (false !== ($entry = readdir($handle))) {
                if(is_file('Uploaded_files/Docs/ce/'.$entry)){
                    $file = explode(".",$entry);
                    if($file[0] == ('ECF_'.$_SESSION['CURREF'].'_certificataliment') ){
                        $btrouve = 1;
                        break;
                    }
                }
            }
        
            closedir($handle);
        }
        
        //Affichage du nom du certificat d'alimentarité si elle xiste
        if ($btrouve == 1) {
            $smarty->assign('CE',$file[0].'.'.$file[1]);
        } else {
            $smarty->assign('CE',"");
        }
    }else{
        $smarty->assign('CE',"");
    }
}


$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

//Lister Chapitres
/*try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->Listerchapitre();    
} catch ( Exception $e ) {    
    echo $e->getMessage ();
}*/
$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listerchapitre');

if (!empty ($TabData) ){
    $smarty->assign('Lstchp',$TabData);  
}

//Lister les nomenclatures
/*try {
    $Result = new SoapClient ( $wsdl );
    $ResData = $Result->Listernomenclature('01');    
} catch ( Exception $e ) {
    echo $e->getMessage ();
}*/
$Result = new nusoap_client($wsdl, true);
$ResData = $Result->call('Listernomenclature', array(
    'code' => '01'
));
if (!empty ($ResData) ){
    $smarty->assign('Lstnmclt',$ResData);
}

//Lister Pays
try {
    $Result = new SoapClient ( $wsdl );
    $ResDatas = $Result->Listerpays();
} catch ( Exception $e ) {
    echo $e->getMessage ();
}
if (!empty ($ResDatas) ){    
    $smarty->assign('Lstpays',$ResDatas);    
}


//Lister Pays
try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->CondETLogistique($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}
if (!empty ($TabData) ){
    $smarty->assign('A00C25',trim($TabData['A00C25']));
    $smarty->assign('A00C50',trim($TabData['A00C50']));
    $smarty->assign('A00C95',trim($TabData['A00C95']));
    $smarty->assign('A00D10',trim($TabData['A00D10']));
    $smarty->assign('A00C90',trim($TabData['A00C90']));
    $smarty->assign('NOMNTR',trim($TabData['NOMNTR']));
    $smarty->assign('A00C92',trim($TabData['A00C92']));
    $smarty->assign('EANUNITE',trim($TabData['EANUNIT']));
}


//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->refetatpages($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}

//////////////////////////////////////////////////////////////////
unset($TabData);

$smarty->display('securite.tpl');

?>
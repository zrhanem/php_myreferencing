<?php

@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}

require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

$smarty->assign('DESFR1',$_SESSION['DESFR1']);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('LstFctDecret');


if (!empty ($TabData) ){
     
    $smarty->assign('LstFct',$TabData);

}

unset($TabData);

$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('FctEcopart', array(
    'pre_ref' => $ref
));

if (!empty ($TabData) ){
     
    $smarty->assign('LEWXNA',trim($TabData['LEWXNA']));
    $smarty->assign('LEWYNA',trim($TabData['LEWYNA']));
    $smarty->assign('LEWZNA',trim($TabData['LEWZNA']));
    $smarty->assign('LEYCN6',round(trim($TabData['LEYCN6']), 4));

}

unset($TabData);

//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->refetatpages($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}

//////////////////////////////////////////////////////////////////

$smarty->display('ecopart.tpl');

?>
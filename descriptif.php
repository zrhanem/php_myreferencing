<?php
@session_start();

if (!isset($_SESSION['IDFRS'])) {
    header('location: index.php');
    exit();
}



require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');

/*
 * smarty
 */
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->caching = false;

if (isset($_GET['ref']) && !empty($_GET['ref'])){
    if ( isset($_SESSION['REFFRS']) && !empty($_SESSION['REFFRS']) ){
        
        $pos = array_search($_GET['ref'], $_SESSION['REFFRS']);
        
        if($pos !== false){
            $ref = $_GET['ref'];
            $_SESSION['CURREF'] = $_GET['ref'];
        }else{
            $_SESSION['CURREF'] = "";
            header('location: home.php');
            exit();
        }
        
    }else{
        $_SESSION['CURREF'] = "";
        header('location: home.php');
        exit();
    }
    
}else{
    $_SESSION['CURREF'] = "";
    header('location: home.php');
    exit();
}


$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Ref_Descriptif', array(
    'pre_ref' => $ref
));


/*try {
    $Result = new SoapClient($wsdl);
    $TabData = $Result->Ref_Descriptif($ref);    
} catch (Exception $e) {
    echo $e->getMessage ();
}*/





if (!empty ($TabData) ){                
    
        $smarty->assign('DESFR1',$TabData['A00A15']);
        $_SESSION['DESFR1'] = $TabData['A00A15'];
        $smarty->assign('DESGB1',$TabData['A00A20']);
        $smarty->assign('DESNL1',$TabData['DESINL']);
        $smarty->assign('DESFR2',$TabData['DESFR2']);
        $smarty->assign('DESGB2',$TabData['DESGB2']);
        $smarty->assign('DESNL2',$TabData['DESNL2']);
        $smarty->assign('BENEF1',$TabData['T00004']);
        $smarty->assign('BENEF2',$TabData['T00005']);
        $smarty->assign('BENEF3',$TabData['T00006']);
        $smarty->assign('DELAI',$TabData['A00C20']);
        $smarty->assign('DESREF',$TabData['A00A10']);
        $smarty->assign('T00008',$TabData['T00008']);
        $smarty->assign('DESCRIPTIF',$TabData['DESC']);
        $DateCde = $TabData['A00C10'];
        $Year = substr($DateCde,0,4);
        $Month = substr($DateCde,5,2);
        $Day = substr($DateCde,8,2);
        $DateCde = $Day."/".$Month."/".$Year;        
        $smarty->assign('DATED', $DateCde);
        $DateCde = $TabData['A00C15'];
        $Year = substr($DateCde,0,4);
        $Month = substr($DateCde,5,2);
        $Day = substr($DateCde,8,2);
        $DateCde = $Day."/".$Month."/".$Year;
        $smarty->assign('DATEF',$DateCde);
        
        
        if($TabData['A00F70'] != ''){
            $btrouve = 0;
            $file='';
            $handle = opendir('Uploaded_files/Docs/Fiche_Produits/');
            
            if ($handle) {
            
                /* Ceci est la fa�on correcte de traverser un dossier. */
                while (false !== ($entry = readdir($handle))) {
                    if(is_file('Uploaded_files/Docs/Fiche_Produits/'.$entry)){
                        $file = explode(".",$entry);
                        if($file[0] == ('ECF_'.$_SESSION['CURREF'].'_FicheProd') ){
                            $btrouve = 1;
                            break;
                        }
                    }
                }
            
                closedir($handle);
            }
            
            //Affichage de la fiche de production si elle xiste
            if ($btrouve == 1) {
                $smarty->assign('FP',$file[0].'.'.$file[1]);
            } else {
                $smarty->assign('FP',"");
            } 
        }else{
            $smarty->assign('FP',"");
        }
        
        
}

unset($TabData);

//$wsdl = WSDIR."actionsreference/wsactionsreference.php?wsdl";

/*$Result = new nusoap_client($wsdl, true);
$TabData = $Result->call('Listercaracteristiques', array(
    'pre_ref' => $ref
));*/

/*try {
    $Result = new SoapClient($wsdl);
    $TabData = $Result->Listercaracteristiques($ref);
} catch (Exception $e) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){
     
    $smarty->assign('LstCaract',$TabData);    
    $_SESSION['NewDTO'] = $TabData[0]['NewDTO'];    
}

unset($TabData); */

//////////////Gestion des couleurs
$wsdl = WSDIR."etatpages/wsetatpages.php?wsdl";

try {
    $Result = new SoapClient ( $wsdl );
    $TabData = $Result->refetatpages($ref);
} catch ( Exception $e ) {
    echo $e->getMessage ();
}

if (!empty ($TabData) ){
    if($TabData['PAGE1'] == 'O'){
        $smarty->assign('classdes',"btn btn-success");
    }else{
        $smarty->assign('classdes',"btn btn-danger");
    }
    if($TabData['PAGE2'] == 'O'){
        $smarty->assign('classcond',"btn btn-success");
    }else{
        $smarty->assign('classcond',"btn btn-danger");
    }
    if($TabData['PAGE4'] == 'O'){
        $smarty->assign('classsec',"btn btn-success");
    }else{
        $smarty->assign('classsec',"btn btn-danger");
    }
    if($TabData['PAGE7'] == 'O'){
        $smarty->assign('classeco',"btn btn-success");
    }else{
        $smarty->assign('classeco',"btn btn-danger");
    }
}else{
    $smarty->assign('classdes',"btn btn-danger");
    $smarty->assign('classcond',"btn btn-danger");
    $smarty->assign('classsec',"btn btn-danger");
    $smarty->assign('classeco',"btn btn-danger");
}


//////////////////////////////////////////////////////////////////






$smarty->display('descriptif.tpl');

?>
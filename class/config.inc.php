<?php
/*
 * @author p06602
 * @date 09/12/2014
 *
 * Modifications :
 *
 *
 */
define('MODE_DEVELOPPEMENT',false);

if (MODE_DEVELOPPEMENT == true) {
    /*
     * Databases connexions
     */
    define("CNXBDD", "MYSQL");
    define("SERVER", "serverdev01");
    define("PORT", 3306);
    define("USER_BDD", "root");
    define("PWD_BDD", "");
    define("DB_NAME",'mychomette');

    /*
     * MYCHOMETTE ROOT DIRECTORY
     */
    define('MYCHOMETTE_ROOT', dirname(__DIR__).'/');

    /*
     * emails expeditor
     */
    define("SERVICECLIENT", "ctrlinfo@ecf.fr");

}
else {
    /*
     * Databases connexions
     */
    define("CNXBDD", "MYSQL");
    define("SERVER", "127.0.0.1");
    define("PORT", 3306);
    define("USER_BDD", "mychomette");
    define("PWD_BDD", 'Fu$ruhECR3');
    define("DB_NAME",'mychomette');

    /*
     * MYCHOMETTE ROOT DIRECTORY
     */
    define('MYCHOMETTE_ROOT', $_SERVER['DOCUMENT_ROOT'].'/');

    /*
     * emails expeditor
    */
    define("SERVICECLIENT", "serviceclients@chomette.com");

}



/*
 * images directory
 */
define("PHOTOS",MYCHOMETTE_ROOT . 'photos/');
define("DIRCLASS",MYCHOMETTE_ROOT . 'class/');


/*
 * mail templates directory
 */
define("MAIL_TEMPLATES", MYCHOMETTE_ROOT . 'inc/mail_templates');

/*
 * Smarty directories
 */
define("SMARTY_CONFIG_DIR", MYCHOMETTE_ROOT . 'inc/smarty/configs/');
define("SMARTY_TEMPLATE_DIR", MYCHOMETTE_ROOT . 'templates/');
define("SMARTY_COMPILE_DIR",MYCHOMETTE_ROOT . 'templates_c/');
define("SMARTY_CACHE_DIR", MYCHOMETTE_ROOT . 'cache/');


/*
 * Webservices directory
 */
define("WSDIR",'http://172.16.128.5:10080/ws/e-referencing/' );
//define("WSDIRGEN",'http://172.16.128.4:10080/wsprod/_gen/' );






// définition des moyens de paiement
define("PAY_CB",1);
define("PAY_CB30J",2);
define("PAY_CB4FOIS",3);
define("PAY_VIRONLINE",4);
define("PAY_PRELEVEMENT",5);





?>

<?php

/*if (file_exists('maintenance.html') == true){
    header('Location: maintenance.htm');
}*/ 

@session_start();

    
require_once('inc/config.inc.php');
require_once('assets/smarty/libs/Smarty.class.php');
require_once('bibliotheque/nusoap/lib/nusoap.php');
if (!isset($_SESSION['LANG'])) {
    $_SESSION['LANG']='FR';
}

if (isset($_SESSION['IDFRS'])) {
    header('location: home.php');   
}


/*
* smarty
*/
$smarty = new Smarty();
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;

//$smarty->caching = false;
$smarty->assign('error_cnx',FALSE); // by default : no error

/*
 * display smarty template
*/
$smarty->display('index.tpl');

?>


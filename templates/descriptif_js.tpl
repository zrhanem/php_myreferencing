    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    {* <script type="text/javascript" src="assets/js/validator/jquery.validate.min.js"></script> *}
    {* <script type="text/javascript" src="assets/js/validator/jquery.validate.js"></script> *}
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
     <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>
        
        <!-- Ces fichiers sont pour le daterange picker -->
        <script src="assets/js/moment.min.js"></script>
        <script src="assets/js/moment.js"></script>
        <script src="assets/js/package.js"></script>
        <script src="assets/js/daterangepicker.js"></script>
        
        <!-- Ces fichiers sont pour le upload file -->
        <script src="assets/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
              
        {include file='googleanalytics.tpl'}
                
        {literal}                
        <script type="text/javascript">
			$(function() {
			    $('input[name="daterange"]').daterangepicker();
			});
		</script>
		
	      <script type="text/javascript">
	         $(function() {	     	            	         	 
	          $( "#datepicker1" ).datepicker({ dateFormat: 'dd-mm-yyyy' });
	          });
	         $(function() {	         	        
	          $( "#datepicker2" ).datepicker({ dateFormat: 'dd-mm-yyyy' });
	        });
	      </script>		
        {/literal}
   
                        
        
        {literal}      
        <script type="text/JavaScript">
			function valid(f) {
			!(/^[A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]*$/i).test(f.value)?f.value = f.value.replace(/[^A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]/ig,''):null;
			} 
		</script>
		<script type="text/JavaScript">
			function validNumber(f) {		
			!(/^[0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9]/ig,''):null;
			} 
		</script>
			<script type="text/javascript">
			$(document).ready(function(){	
			  $( "#btnValid" ).on( "click", function() {			  	
				  $('form#my_form').submit();		  
				});
			});			  			  
			</script>		
				
			<script>			
			//D�rouler l'accord�on au chargement de la page
			$(window).load(function(){
			    $('#accord2').click();
			    $('#accord3').click();					      
			});
			</script>
		{/literal}
		
        {literal}                
			<script type="text/javascript">
			$(function () {
				    $('form#my_form').on('submit', function (e) {

    			        // On emp�che le navigateur de soumettre le formulaire
				        e.preventDefault();

	   					var msg = "";
						var err = 0;
						
						if($.trim($('#DesREF').val()) == '' ){						
							msg += "Vous devez saisir une r�f�rence"+"<br>";
							err = 1;
						} 
						if($.trim($('#DesGB1').val()) == '' ){						
							msg += "Vous devez saisir une description en anglais"+"<br>";
							err = 1;
						}   
						
						if($.trim($('#DesFR1').val()) == '' ){									
							msg += "Vous devez saisir une description en fran�ais"+"<br>";
							err = 1;
						} 
						if($.trim($('#DesGB2').val()) == '' ){						
							msg += "Vous devez saisir une description en anglais"+"<br>";
							err = 1;
						}   
						
						if($.trim($('#DesFR2').val()) == '' ){									
							msg += "Vous devez saisir une description en fran�ais"+"<br>";
							err = 1;
						} 																													
						
						if(err == 1){
     						$('#STS').val("N");
							$('#MissingErr').removeClass('hidden');	
							$('#MissingErr').html("");
							$('#MissingErr').html("<strong>"+msg+"</strong>");
						
		                	var r = confirm("Vous avez des champs obligatoires non remplis, enregistrer?");
        					if (r == true) {
        						$('#divLoading').show();						        
						        var formData = new FormData($('form#my_form')[0]);
						        
						        $.ajax({
						            url: 'ajax/ajax_fileupload_Des.php',
						            type: 'POST',
						            data: formData,
								    async: false,
								    cache: false,
								    contentType: false,
								    processData: false,				            
						            success: function (response) {							            	                                   						           
						            		            		            	
						                // La r�ponse du serveur	
						                $('#divLoading').hide();							                	               
						                if(response == 0){
						                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'success',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });	
						            		$('#my_form').find('input[name="FichePrd"]').val('');				                	
						                	$('#divficheact').removeClass('hidden');	
						                	location.reload();							                				                			                
						                }else{						                	
						                	if(response == 999){
						                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
						                	}else{
						                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						                	}	
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'error',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });	
									        location.reload();	
						                }						        	               				                
						            }
						        });
						        
						        	
        					}else{
			                	var CodeRetour = "Enregistrement annul�";
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'warning',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'top',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });	
 								return false;
        					}
						}else{								
    							$('#STS').val("O");
						        $('#divLoading').show();
        						$('#MissingErr').addClass('hidden');
								$('#MissingErr').empty();
						        
						        var formData = new FormData($('form#my_form')[0]);
						        
							    $.ajax({
						            url: 'ajax/ajax_fileupload_Des.php',
						            type: 'POST',
						            data: formData,
								    async: false,
								    cache: false,
								    contentType: false,
								    processData: false,				            
						            success: function (response) {							            					            					            
						            			            		            	
						                // La r�ponse du serveur	
						                $('#divLoading').hide();					                
						                if(response == 0){
						                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'success',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });	
						            		$('#my_form').find('input[name="FichePrd"]').val('');				                	
						                	$('#divficheact').removeClass('hidden');	
						                	location.reload();							                			                
						                }else{					                	
						                	if(response == 999){
						                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
						                	}else{
						                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						                	}	
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'error',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });	
									        location.reload();	
						                }						        	               				                
						            }
						        });		     
  						
						}	

				    });
				});
			</script>
			
	
		{/literal}		
		
        
  </body>
</html>








				




 {include file='header.tpl'} 
        <div class="container">
          <div class="row">

          <div class="col-md-12">          
			 	<div class="form-inline" >
	               <div class="form-inline" >                
	                    <div id="date-form" class="supervisor-date-form input-daterange">
	                       <div class="form-group">
          						<label for="selectstatus">{_("Consulter vos r�f�rences :")}</label>	          						 
      							<select id="selectstatus" class="form-control input-sm">
      								<option value="1">{_("A valider")}</option>
      								<option value="2">{_("Valid�es")}</option>
      								<option value="0">{_("Refus�es")}</option>
      								<option value="3">{_("Toutes")}</option>                                          
                        		</select>                        		       					
       						 </div>      						
	                    </div>
          			</div>
          			<br>
				</div> 

              <div id="divLoading" style="display: none;">
                <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
              </div>
              
              	{* Dialogue modal pour actions *}
				<div class="modal fade in" id="responsive" tabindex="-1" role="dialog" aria-labelledby="dialValid" aria-hidden="true" style="position:fixed;">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Annuler"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">{_("Demande de Copie de r�f�rences")}</h4>
				      </div>
				      <div class="modal-body">					
						<div>{_("Veuillez cocher, ci-dessous, les r�f�rences dans lesquelles vous voulez copier les caract�ristiques de la r�f�rence en cours")}</div>
						
						<div class="checkbox">
							<form name="frmChkForm" id="frmChkForm">
							<input type="hidden" id="reforiginale" value="">	
	  	  				   {foreach $ListeRef as $val}
		                        <label id="LBLCHK-{$val[6]}">
		                          <input class="chkGroup" name="copy_list" type="checkbox" id="CHK-{$val[6]}" value="{$val[6]}"/> {$val[1]}
		                        </label>
		                         <br>	                         
							{/foreach}
							</form> 
	                      </div>
	
				      </div>
				      <div class="modal-body">
				      	<div><span class="label label-danger">{_("NB :")}</span></div>
				      	<div>
				      		<p  class="text-danger">{_("- La copie n'inclue pas les fichiers visuels et les autres fichiers, � savoir, certificat d'alimentarit�, Ineris, Fiche s�curit�, fiche technique....etc")}</p>
				      	</div>
				      </div>				      
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">{_("Annuler")}</button>
				        <button type="button" class="btn btn-primary" id="btnValid">{_("Lancer la copie")}</button>
				      </div>
				    </div>
				  </div>
				</div>
				{* Fin Dialogue modal por validation commande *}  									
			
                <div class="supervisor-stores-container">
                  <div class="supervisor-stores__head">
                    <h3 class="supervisor-stores__title">{_("Liste de vos r&eacute;f&eacute;rences")}</h3>
                  </div>
                   <div class="supervisor-list__table">
                    <div class="supervisor-list__thead">
                      <div class="supervisor-list__row">
                        <span class="supervisor-list__cell supervisor-list__cell--store">{_("Dossier")}</span>
                        <span class="supervisor-list__cell supervisor-list__cell--num">{_("R�f�rence")}</span>
                        <span class="supervisor-list__cell supervisor-list__cell--city">{_("D�signation")}</span>
                        <span class="supervisor-list__cell supervisor-list__cell--city">{_("Commentaire")}</span>
                        <span class="supervisor-list__cell supervisor-list__cell--num">{_("Date cr�ation")}</span>
                        {* <span class="supervisor-list__cell supervisor-list__cell--num">{_("Option")}</span> *}
                        {* <span class="supervisor-list__cell supervisor-list__cell--num">Action</span> *}
                      </div>
                    </div>
                        
               <div class="supervisor-list__tbody" id="corpstbl">
               {if !empty($ListeRef)}
                  	{foreach $ListeRef as $val}              
                            <div id="store-1" class="supervisor-list__row">                        
                              <div class="supervisor-list__cell supervisor-list__cell--store">{$val[0]}</div>
                              <div class="supervisor-list__cell supervisor-list__cell--num">{$val[1]}</div>
                              <div class="supervisor-list__cell supervisor-list__cell--city">{$val[2]}</div>
                              <div class="supervisor-list__cell supervisor-list__cell--city">{$val[9]}</div>
                              <div class="supervisor-list__cell supervisor-list__cell--num">{$val[4]}</div>
                              {* <div class="supervisor-list__cell supervisor-list__cell--data">{_(Copier)}</div> *}
                              {* <div class="supervisor-list__cell supervisor-list__cell--data"> *}
                                {* <button type="button" data-toggle="collapse" data-target="#ActChoices-{$val[1]}" class="btn btn-default collapsed"><span>{_("Modifier ")}</span></button> *}
                              {* </div> *}
                              
                              <button type="button" data-toggle="collapse" data-target="#ActChoices-{$val[6]}" class="btn btn-default collapsed"><span>{_("Modifier ")}</span></button>
								<div class="btn-group">
								        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{_("Actions")}<span class="caret"></span></a>
								        <ul class="dropdown-menu" role="menu">
								          <li><a href="#" id="Copy-{$val[6]}">{_("Copier")}</a></li>								          								          
								          {if $val[17] eq 'O'} <li><a href="#" id="btnsave-{$val[6]}">{_("Valider et transmettre")}</a></li> {/if}
								        </ul>
								</div>
							 {if $val[17] eq 'O'}	
							 	<span class="label label-success">{_("R�f�rence compl�te")}</span>							 	
							 {else}
								<span class="label label-danger">{_("R�f�rence incompl�te")}</span>							 
							 {/if}
                                  <div class="panel">
      		                          <div id="ActChoices-{$val[6]}" class="supervisor-stores__panel supervisor-stores__panel--orders collapse">
      		                            <div class="supervisor-stores__panel-container">
      		                            <button type="button" data-toggle="collapse" data-target="#ActChoices-{$val[6]}" class="close"><span aria-hidden="true">&times;</span></button>
                                            <div class="quick-links-wrapper">
          						                <div class="quick-links">
          						                  <div class="quick-links__container">						                
          						                    <div class="quick-links__item quick-links__item--reload"><a href="descriptif.php?ref={$val[6]}"><i class="icon icon-catalog"></i><span>{_("Descriptif")}</span></a></div>
          					                      	<div class="quick-links__item quick-links__item--reload"><a href="condition.php?ref={$val[6]}"><i class="icon icon-cart"></i><span>{_("Condition")}</span></a></div>                    								                    													
          						                    <div class="quick-links__item quick-links__item--reload"><a href="securite.php?ref={$val[6]}"><i class="icon icon-kits"></i><span>{_("S�curit� & logistique")}</span></a></div>          						                    
          						                    <div class="quick-links__item quick-links__item--catalog"><a href="ecopart.php?ref={$val[6]}"><i class="icon icon-reload"></i><span>{_("Eco-participation")}</span></a></div>     						                    
          						                  </div>
          						                </div>
                  						     </div>      		                              
      		                            </div>
      		                          </div>
      		                        </div>
                                                                    
                            </div>               
      					     {/foreach}
              {else}
                 <div id="store-1" class="supervisor-list__row">                        
                    <div class="supervisor-list__cell supervisor-list__cell--city">{_("Aucune r�f�rence.")}</div>
                 </div>							
						  {/if}       
             </div>                     
              </div>

              </div> 
	
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='home_js.tpl'}

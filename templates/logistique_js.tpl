    
    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
    <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Ces fichiers sont pour le upload file -->
        <script src="assets/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
        {* <script src="assets/js/fileinput.min.js"></script>
        <script src="assets/js/fileinput.js"></script> *}

		{include file='googleanalytics.tpl'}

		{literal}	
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $(".clickable-row").click(function(){
		      var id = this.id;
		      $('#A00C90').val(id);		      
		      $('#myModal').hide();						              
		        });                                                                  
		  });
		</script>
		<script type="text/JavaScript">
			function validNumber(f) {		
			!(/^[0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9]/ig,''):null;
			} 
		</script>
		<script type="text/JavaScript">
			function validFloat(f) {		
			!(/^[0-9.]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9.]/ig,''):null;
			} 
		</script>
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $("#nomDisplay").click(function(){		      
		      $('#myModal').show();						              
		        });                                                                  
		  });
		</script>
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $("#closemodal").click(function(){		      
		      $('#myModal').hide();						              
		        });                                                                  
		  });
		</script>
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $("#btnclosemodal").click(function(){		      
		      $('#myModal').hide();						              
		        });                                                                  
		  });
		</script>
		
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $("#SELCHPT").change(function(){				      	 
		      	$('#tblnmt').empty();		
		       	$('#divLoading').show();   		      		      	     
					$.ajax({
					 cache: false,
                     type: 'POST',
                     url: 'ajax/ajax_nomenclature.php',
                     data: "schpt="+$("#SELCHPT").val(),
                     dataType: "html",
                     success: function(datas_result){                             	     
                     	$('#divLoading').hide();
                     	$('#tblnmt').append(datas_result);            	                     	                     
					 },					 
					 complete: function(data) {     
					    $(function() {
								$(".clickable-row").click(function(){
										      var id = this.id;
										      $('#A00C90').val(id);		      
										      $('#myModal').hide();						              
										        });          
					    });
					  }                  
                  });   
		      	
		      	
		      		      						              
		        });                                                                  
		  });
		</script>
		
		
		{/literal}
		
		{literal}       
            <script type="text/javascript">
            function ajaxindicatorstart(text){ 
            	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){ 
            		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="photos/icons/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>'); 
            		} 
        		jQuery('#resultLoading').css({ 
        			'width':'100%','height':'100%','position':'fixed', 'z-index':'10000000','top':'0',
        			'left':'0','right':'0','bottom':'0','margin':'auto'
        			 }); 
      			jQuery('#resultLoading .bg').css({ 
      			 	'background':'#000000','opacity':'0.7','width':'100%','height':'100%','position':'absolute',
      			 	'top':'0' 
      			 	}); 
    			 	jQuery('#resultLoading>div:first').css({
    			 		'width': '250px','height':'75px','text-align': 'center','position': 'fixed','top':'0', 
    			 		'left':'0','right':'0', 'bottom':'0','margin':'auto','font-size':'16px','z-index':'10','color':'#ffffff'
    			 		}); 
    		 		jQuery('#resultLoading .bg').height('100%');
    		 		jQuery('#resultLoading').fadeIn(300);
    		 		jQuery('body').css('cursor', 'wait'); 
            } 
            
            function ajaxindicatorstop(){
            	jQuery('#resultLoading .bg').height('100%');
            	jQuery('#resultLoading').fadeOut(300);
            	jQuery('body').css('cursor', 'default');
            	} 

            function MessageAffiche(text){ 
            	if(jQuery('body').find('#messageAlert').attr('id') != 'messageAlert'){ 
            		jQuery('body').append('<div id="messageAlert" style="display:none"><div>'+text+'</div></div>'); 
            		} 
            } 
            </script>
        {/literal}
		
		{literal}      
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $('#btnValid').click(function(){						
			      
  					var msg = "";
					var err = 0;
					
					if($('#A00C25').val() <= 0 || $('#A00C25').val() == ''){									
						msg += "Vous devez saisir le conditionnement Primaire (au contact du produit)"+"<br>";
						err = 1;
					}
					
					if($('#A00C25').val() != '' && $('#A00C25').val() > 0 && $('#A00C95').val().length != 13 ){									
						msg += "Le code EAN du produit doit contenir 13 chiffres"+"<br>";
						err = 1;
					}
					
					if($('#A00C50').val() != '' && $('#A00C50').val() > 0 && $('#A00D10').val().length != 13 ){									
						msg += "Le code EAN du conditionnement doit contenir 13 chiffres"+"<br>";
						err = 1;
					}
					
					if($('#A00C90').val().length != 8 && $('#NOMNTR').val().length != 8 ){									
						msg += "Vous devez saisir la nomenclature douani�re valide, Celle-ci est compos�e de 8 chiffres"+"<br>";
						err = 1;
					}
					
					if($('#A00C92').val() < 1 ){									
						msg += "Vous devez indiquer le pays du produit."+"<br>";
						err = 1;
					}
					
 					if(err == 1){
						$('#MissingErr').removeClass('hidden');	
						$('#MissingErr').html("");
						$('#MissingErr').html("<strong>"+msg+"</strong>");
						
		                var r = confirm("Vous avez des champs obligatoires non remplis, enregistrer?");
        				if (r == true) {
        				
	        				jQuery(document).ajaxStart(function () { 
		      		     	//show ajax indicator
		      		     	ajaxindicatorstart('Saving datas...please wait...');
		      		     	 }).ajaxStop(function () { 
		      		     	 	//hide ajax indicator
		      		     	 	ajaxindicatorstop();
		      		     	 	});
							var nomntr 	= $('#NOMNTR').val();
							var A00C90 	= $('#A00C90').val();
							var A00C92	= $('#A00C92').val();
							var A00C95 	= $('#A00C95').val();
							var A00D10 	= $('#A00D10').val();
							var A00C25	= $('#A00C25').val();
							var A00C50	= $('#A00C50').val();
							var vollit 	= $('#VOLLIT').val();
							var pal 	= $('#A00C75').val();
							var respal 	= $('#RESPAL').val();
							var palsep 	= $('#PALSEP').val();
							var mrqcnf 	= $('#MRQCNF').val();
							var Chaine 	= 'NOMNTR='+nomntr+'&A00C90='+A00C90+'&A00C92='+A00C92+'&A00C95='+A00C95+'&A00D10='+A00D10+'&A00C25='+A00C25+'&A00C50='+A00C50+'&VOLLIT='+vollit+'&PAL='+pal+'&RESPAL='+respal+'&PALSEP='+palsep+'&MRQCNF='+mrqcnf+'&STS=N';
							
			                $.ajax({
		                       type: 'POST',
		                       url: 'ajax/ajax_logistique.php',
		                       data: Chaine,
		                       dataType: "html",
		                       success: function(add_result){	    			                                               		                     						       	                       			       				                       					                  	                                                                                                                              		                 
		                        	jQuery.ajax({global: false});                          	                       	              
		                        	if(add_result == 0){
					                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'success',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });                
								        location.reload();		       		                        		
		                        	}else{
					                	var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'error',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });	
								        location.reload();
		                        	}
		
		        		        }
		                    });		
        				
        				}else{
		                	var CodeRetour = "Enregistrement annul�";
					        var n = noty({
					            text        : CodeRetour,
					            type        : 'warning',
					            dismissQueue: true,
					            timeout     : 5000,
					            closeWith   : ['click'],
					            layout      : 'top',
					            theme       : 'defaultTheme',
					            maxVisible  : 5
					        });	
        				}
						
					}else{
						
						$('#MissingErr').addClass('hidden');
						$('#MissingErr').empty();
						
						jQuery(document).ajaxStart(function () { 
	      		     	//show ajax indicator
	      		     	ajaxindicatorstart('Saving datas...please wait...');
	      		     	 }).ajaxStop(function () { 
	      		     	 	//hide ajax indicator
	      		     	 	ajaxindicatorstop();
	      		     	 	});
						var nomntr 	= $('#NOMNTR').val();
						var A00C90 	= $('#A00C90').val();
						var A00C92	= $('#A00C92').val();
						var A00C95 	= $('#A00C95').val();
						var A00D10 	= $('#A00D10').val();
						var A00C25	= $('#A00C25').val();
						var A00C50	= $('#A00C50').val();
						var vollit 	= $('#VOLLIT').val();
						var pal 	= $('#A00C75').val();
						var respal 	= $('#RESPAL').val();
						var palsep 	= $('#PALSEP').val();
						var mrqcnf 	= $('#MRQCNF').val();
						var Chaine 	= 'NOMNTR='+nomntr+'&A00C90='+A00C90+'&A00C92='+A00C92+'&A00C95='+A00C95+'&A00D10='+A00D10+'&A00C25='+A00C25+'&A00C50='+A00C50+'&VOLLIT='+vollit+'&PAL='+pal+'&RESPAL='+respal+'&PALSEP='+palsep+'&MRQCNF='+mrqcnf+'&STS=O';
						
		                $.ajax({
	                       type: 'POST',
	                       url: 'ajax/ajax_logistique.php',
	                       data: Chaine,
	                       dataType: "html",
	                       success: function(add_result){	    			                                               		                     						       	                       			       				                       					                  	                                                                                                                              		                 
	                        	jQuery.ajax({global: false});                          	                       	              
	                        	if(add_result == 0){
				                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
							        var n = noty({
							            text        : CodeRetour,
							            type        : 'success',
							            dismissQueue: true,
							            timeout     : 5000,
							            closeWith   : ['click'],
							            layout      : 'top',
							            theme       : 'defaultTheme',
							            maxVisible  : 5
							        });     
	                        		location.reload();	                  	
	                        	}else{
				                	var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
							        var n = noty({
							            text        : CodeRetour,
							            type        : 'error',
							            dismissQueue: true,
							            timeout     : 5000,
							            closeWith   : ['click'],
							            layout      : 'top',
							            theme       : 'defaultTheme',
							            maxVisible  : 5
							        });	
							        location.reload();
	                        	}
	
	        		        }
	                    });					
					
					}	
					
   		     		
						               
		        });                                                                  
			  });
			</script>		
		{/literal}

  </body>
</html>






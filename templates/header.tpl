<!DOCTYPE html>
<html class="no-js">
  <head>
    <title>My Referencing</title>
{* <meta charset="utf-8"> *}
 	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,600">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="assets/css/mybootstrap.css"  type="text/css" />
	<link rel="stylesheet" href="assets/css/mymodal.css">	
	{* <link rel="stylesheet" href="assets/css/validator/screen.css"> *}   
		
    <link rel="stylesheet" href="assets/fancybox/source/jquery.fancybox.css">
    <script src="assets/js/vendors/modernizr.min.js"></script>
    <!--[if IE 8]>
    <link rel="stylesheet" href="assets/css/ie/ie8.min.css">
    <script type="text/javascript" src="assets/js/polyfills/html5shiv.min.js"></script>
    <script type="text/javascript" src="assets/js/polyfills/respond.min.js"></script>
    <![endif]--><!--[if IE 9]>
    <link rel="stylesheet" href="assets/css/ie/ie9.min.css"><![endif]-->
	<style>
      .btn-large { width:100%; }
    </style>

    
  </head>
  <body>
    <div class="offcanvas">
      <div id="wrapper" class="wrapper">
        <header role="banner" class="header">

          <div class="header-container container">
            <h1 class="header-logo" >
            {if $smarty.session.IDFRS eq ''}
                <a href="index.php" style="background-color: transparent;">
            {/if}    
            <a href="home.php" style="background-color: transparent;">
            <img src="photos/logos/ECF_logo.jpg">My Referecing</a>
            </h1>
            <div class="header-links">
 
              <div class="header-links__item header-links__item--account">
                <a href="pdf/Help_Myreferencing.pdf" target="blank">            
                <i class="icon icon-bag"></i><strong>{_("Documentation")}</strong><span>{_("Aide")}</span></a>
              </div>
              
              <div class="header-links__item header-links__item--cart"><a href="ajax/ajax_logout.php"><i class="icon icon-cancel"></i><strong>{_("Déconnexion")}</strong></a></div>               
              </div>
            <button id="toggle-offcanvas" type="button" class="header-toggle hidden-md hidden-lg"><span class="icon-burger"></span></button>

            <div class="header-welcome"><span>{_("Bienvenue")}!</span><small>{_("Vous &ecirc;tes connect&eacute;(e) &agrave; votre espace Partenaire.")}</small></div>
          </div>
          <nav role="navigation" class="header-navigation hidden-xs hidden-sm">
            <div class="container">
               <ul>
                <li><a href="home.php">{_("Accueil")}</a></li>
              </ul> 
            </div>
          </nav>
        </header>
<!DOCTYPE html>
<html class="no-js">
    <head>    	
        <title>e-Referencing</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"
              href="//fonts.googleapis.com/css?family=Open+Sans:400,600">

		<script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>

        <link rel="stylesheet" href="assets/css/main.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"> 
        {* <link rel="stylesheet" href="assets/css/mylogin.css"> *}
        <link rel="stylesheet" href="assets/css/mybootstrap.css">
		<link rel="stylesheet" href="assets/css/mymodal.css">
				
        <script src="assets/js/vendors/modernizr.min.js"></script>
         <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>

        <!--[if IE 8]>
            <script type="text/javascript" src="assets/js/polyfills/html5shiv.min.js"></script>
            <script type="text/javascript" src="assets/js/polyfills/respond.min.js"></script>
            <script type="text/javascript" src="assets/js/polyfills/selectivizr.min.js"></script><![endif]-->

	<style>
      .form-submit { text-indent:-9999em; border:0; width:539px; height:170px; background:url("assets/images/idenvous.png") no-repeat 0 0 ; line-height:0 font-size:0; }
    </style>

    </head>
    <body>
        <div class="offcanvas">
            <div id="wrapper" class="wrapper">
                <header role="banner" class="header">
                    <div class="header-container container">
                        <h1 class="header-logo" >
                            <a href="index.php" style="background-color: transparent;"><img src="photos/logos/ECF_logo.jpg">My Referencing</a>
                        </h1>
                        <div class="header-links">
                            <div class="header-links__item header-links__item--account">
                                <a href="#loginmodal" data-toggle="modal"
                                   data-target="#loginmodal"><i class="icon icon-account"></i><strong>{_("Mon
									compte</strong><span>Acc&egrave;s &agrave; mes donn&eacute;es
									personnelles")}</span></a>
                            </div>

                          <div class="header-links__item header-links__item--account">
			                <a href="pdf/Help_Myreferencing.pdf" target="blank">            
			                <i class="icon icon-bag"></i><strong>{_("Documentation")}</strong><span>{_("Aide")}</span></a>
			              </div>
             

                        </div>
                        <button id="toggle-offcanvas" type="button"
                                class="header-toggle hidden-md hidden-lg">
                            <span class="icon-burger"></span>
                        </button>

                        <div class="header-welcome">
                        	<br><br>	
                            <small>{_("Leader europ�en de la distribution de petit mat�riel et consommables aux<br>professionnels de 
							l'h�tellerie, de la restauration et des m�tiers de bouche")}
                            </small>
                        </div>
                    </div>
                    <nav role="navigation" class="header-navigation hidden-xs hidden-sm">
                        <div class="container">
                           &nbsp;<br>&nbsp;
                           
                            <ul class="list-unstyled">{*
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">{_("Tous nos produits")}</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">{_("Ma mercuriale")}</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">{_("Mes favoris")}</a></li>
                                <li><a href="#loginmodal" data-toggle="modal"
                                       data-target="#loginmodal">{_("Mes kits")}</a></li>
                            *}</ul>
                            
                        </div>
                        
                    </nav>
                </header>
                <div class="container">
                    <div class="row" id="ecf">

                        <div class="col-md-12"
                             style="background-color: #eee; border: 1px solid #ddd; border-radius: 4px;  padding-bottom : 10px; background-image:url(assets/images/image-siege-grigny.jpg); background-size: 100% auto;">

							 <div class="quick-login row" style="padding:100px;">
								<div class="quick-login__container col-md-6" style="background-color:#f2f2f2; border:none;">
                                    <h4 style=" font-size:1.5em; margin-top : 10px; font-weight: bold; color : #5E5E61; border-bottom-style : solid; border-bottom-color : #fff; border-bottom-width : 2px;">{_("S'identifier")}</h4>

                                    <div class="alert alert-error collapse" role="alert" id="Errorlogin">
  										<span><p>{_("Looks like the passwords you entered don't match!")}</p></span>
									</div>

                                    <form role="form" method="POST" id='loginfrm' name='loginfrm'>
                                        <div class="form-group">
                                            <label for="CltEmail" style="color:#000;">{_("Identifiant")}</label> <input
                                                type="email" class="form-control" id="CltEmail"
                                                placeholder="{_("saisir votre adresse mail")}" name="CltEmail" > {* onkeyup="valid(this)" onblur="valid(this)"> *}
                                        </div>
                                        <div class="form-group">
                                            <label for="CltPass" style="color:#000;">{_("Mot de passe")}</label> <input
                                                type="password" class="form-control" id="CltPass"
                                                placeholder="{_("Votre mot de passe")}"  name="CltPass">                                                
                                        </div>
																	                                                                                                                             
                                    </form>		
                                    <span style="float: left; margin-left: 10px;"><button class="btn btn-primary" id="loginbtn1" style="background-color:#6188CC;border-color:#595959;">{_("IDENTIFIEZ-VOUS")}</button></span>
                                    <a href="lostpassword.php" style="font-weight: normal; color:black;text-align: right;" class=" pull-right btn-link">{_("Mot de passe oubli&eacute; ?")}</a>
                                    
                                    							  									
								</div>

								<div class="quick-login__container col-md-6" style="background-color:#f2f2f2; border:100px;">
                                    <h4 style=" font-size:1.5em; margin-top : 10px; font-weight: bold; color : #5E5E61; border-bottom-style : solid; border-bottom-color : #fff; border-bottom-width : 2px;">{_("Requirements")}</h4>
 									<p style="color:black;font-size:90%;">Navigateurs compatibles : </p> 									 									
 									<ul>
 										<li style="color:black;font-size:90%;background-image: url('assets/images/IE.jpg');background-repeat: no-repeat;background-position: 0 0.2em;padding-left: 1.5em;">Internet Explorer(10,11 et plus)</li>
 										<li style="color:black;font-size:90%;background-image: url('assets/images/Chrome.jpg');background-repeat: no-repeat;background-position: 0 0.2em;padding-left: 1.5em;">Chrome</li>
 										<li style="color:black;font-size:90%;background-image: url('assets/images/Mozilla.jpg');background-repeat: no-repeat;background-position: 0 0.2em;padding-left: 1.5em;">Mozilla</li>
 									</ul>
 									<p style="color:black;font-size:90%;">Restrictions sur les fichiers : </p>
 									<ul>
 										<li style="color:black;font-size:90%;">Les images support�es (*.jpg, *.jpeg, *.eps, *.tif, *.png)</li>
 										<li style="color:black;font-size:90%;">Les fichiers support�s (*.pdf)</li>
 										<li style="color:black;font-size:90%;">La taille maximale d'un fichier (image ou autre) est de 10 Mo</li>
 									</ul> 		
								</div>

                            </div>


                        </div>
                        

                        
<div class="modal fade " tabindex="-1" role="dialog" id="loginmodal" aria-labelledby="loginmodal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">e-Referencing</h4><br>
      <p>{_("Veuillez vous connecter pour acc�der � ces menus")}</p>
    </div>
  </div>
</div>
                    </div>
                    <footer class="footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3">
                                {*
                                    <ul class="list-inline">
                                        <li><a href="footer/faq_{$smarty.session.LANG}.php">{_("FAQ")}</a></li>
                                        <li><a href="footer/cgv_{$smarty.session.LANG}.pdf">{_("CGV")}</a></li>
                                        <li><a href="footer/credit_{$smarty.session.LANG}.php">{_("Cr&eacute;dits")}</a></li>
                                        <li><a href="footer/legal_{$smarty.session.LANG}.php">{_("Mentions L&eacute;gales")}</a></li>
                                        <li><a href="footer/perso_{$smarty.session.LANG}.php">{_("Donn&eacute;es personnelles")}</a></li>
                                        <li><a href="http://www.ecf.fr/files/pdf/dev_durable.pdf" target="blank">{_("Rapport d&eacute;veloppement durable")}</a></li>
                                    </ul>
                                 *}
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            <script type="text/javascript" src="assets/js/jquery/1.11.1/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
            <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
            <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
            <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
            <script type="text/javascript" src="assets/js/main.min.js"></script>
            {* Script pour connexion en ajax *}
        
        {include file='googleanalytics.tpl'}   
     
      	{literal}	
  	       <script type="text/javascript">
            function ajaxindicatorstart(text){ 
            	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){ 
            		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="photos/icons/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>'); 
            		} 
        		jQuery('#resultLoading').css({ 
        			'width':'100%','height':'100%','position':'fixed', 'z-index':'10000000','top':'0',
        			'left':'0','right':'0','bottom':'0','margin':'auto'
        			 }); 
      			jQuery('#resultLoading .bg').css({ 
      			 	'background':'#000000','opacity':'0.7','width':'100%','height':'100%','position':'absolute',
      			 	'top':'0' 
      			 	}); 
    			 	jQuery('#resultLoading>div:first').css({
    			 		'width': '250px','height':'75px','text-align': 'center','position': 'fixed','top':'0', 
    			 		'left':'0','right':'0', 'bottom':'0','margin':'auto','font-size':'16px','z-index':'10','color':'#ffffff'
    			 		}); 
    		 		jQuery('#resultLoading .bg').height('100%');
    		 		jQuery('#resultLoading').fadeIn(300);
    		 		jQuery('body').css('cursor', 'wait'); 
            } 
            
            function ajaxindicatorstop(){
            	jQuery('#resultLoading .bg').height('100%');
            	jQuery('#resultLoading').fadeOut(300);
            	jQuery('body').css('cursor', 'default');
            	} 

            function MessageAffiche(text){ 
            	if(jQuery('body').find('#messageAlert').attr('id') != 'messageAlert'){ 
            		jQuery('body').append('<div id="messageAlert" style="display:none"><div>'+text+'</div></div>'); 
            		} 
            } 


            </script>
        {/literal}
     
     
            
      {literal}		
      	<script type="text/javascript">
	      	function valid(f) {
				!(/^[A-Z_0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^A-Z_0-9]/ig,''):null;
			} 
      	</script>
      	<script>      	
	      	function validateEmail(sEmail) {	
	 			   var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
					if (filter.test(sEmail)) { return true; }
	    			else {	return false;}
			}      	
      	</script>
      	
      	
      	
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     	
		     $("#loginbtn1").click(function(){
		     		var chainemdp = $.trim( $('#CltPass').val() );	     														     		     				    
              		if($('#CltEmail').val() == ''){							                	
				        var n = noty({
				            text        : "Vous devez saisir une adresse mail",
				            type        : 'error',
				            dismissQueue: true,
				            timeout     : 5000,
				            closeWith   : ['click'],
				            layout      : 'center',
				            theme       : 'defaultTheme',
				            maxVisible  : 4
				        });							
					}else if(chainemdp == ''){							                	
				        var n = noty({
				            text        : "Vous devez saisir un mot de passe",
				            type        : 'error',
				            dismissQueue: true,
				            timeout     : 5000,
				            closeWith   : ['click'],
				            layout      : 'center',
				            theme       : 'defaultTheme',
				            maxVisible  : 4
				        });							
					}else{					
					
						if( !validateEmail( $('#CltEmail').val() ) ){
					        var n = noty({
					            text        : "Adresse mail non conforme",
					            type        : 'error',
					            dismissQueue: true,
					            timeout     : 5000,
					            closeWith   : ['click'],
					            layout      : 'center',
					            theme       : 'defaultTheme',
					            maxVisible  : 4
					        });	
							return;
						}

	       		     	jQuery(document).ajaxStart(function () { 
	      		     	//show ajax indicator
	      		     	ajaxindicatorstart('Authentification...please wait...');
	      		     	 }).ajaxStop(function () { 
	      		     	 	//hide ajax indicator
	      		     	 	ajaxindicatorstop();
	      		     	 	});
									
	                  $.ajax({						 
	                     type: 'POST',
	                     url: 'ajax/ajax_login.php',
	                     data: "FrsLogin="+$('#CltEmail').val()+"&FrsPwd="+chainemdp,
	                     dataType: "html",
	                     success: function(response){     	                     
	                        jQuery.ajax({global: false}); 	                     		                     	                     	                     	
							if (response == '2'){	                                    	                	
						        var n = noty({
						            text        : "Vous devez saisir un identifiant et un mot de passe",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	                   
						    }else if(response == '8') {
 						        var n = noty({
						            text        : "Vous n'avez pas le droit de vous connecter avec ce mot de passe",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	    	
	                        }else if(response == '5') {	                        	        	                	
						        var n = noty({
						            text        : "Votre compte a �t� d�sactiv�, Veuillez contacter E.CF pour le r�activer",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	                          							
	                        }else if(response == '3' || response == '4') {	                        	        	                	
						        var n = noty({
						            text        : "Un probl�me technique est survenu au moment de la tentative de connexion",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	                    							
	                        }else if(response == '6') {	                             	                	
						        var n = noty({
						            text        : "Login ou mot de passe incorrecte",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	                    							
	                        }else if(response == '7') {	                             	                	
						        var n = noty({
						            text        : "Compte d�sactiv�, veuillez contacter E.CF pour le r�activer",
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	                    							
	                        }else {  
						        var n = noty({
						            text        : "Connexion, please wait...",
						            type        : 'success',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 4
						        });	               				                        	
	                            document.location.href ="home.php";
	                        }                  	
						 }                     
	                  }); 
					}      
		      });                                                                   
		  });
		</script>		
		{/literal}
    </body>
</html>
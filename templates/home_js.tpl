    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
     <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
     
        {include file='googleanalytics.tpl'} 
              
		{literal}       
            <script type="text/javascript">
            function ajaxindicatorstart(text){ 
            	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){ 
            		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="photos/icons/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>'); 
            		} 
        		jQuery('#resultLoading').css({ 
        			'width':'100%','height':'100%','position':'fixed', 'z-index':'10000000','top':'0',
        			'left':'0','right':'0','bottom':'0','margin':'auto'
        			 }); 
      			jQuery('#resultLoading .bg').css({ 
      			 	'background':'#000000','opacity':'0.7','width':'100%','height':'100%','position':'absolute',
      			 	'top':'0' 
      			 	}); 
    			 	jQuery('#resultLoading>div:first').css({
    			 		'width': '250px','height':'75px','text-align': 'center','position': 'fixed','top':'0', 
    			 		'left':'0','right':'0', 'bottom':'0','margin':'auto','font-size':'16px','z-index':'10','color':'#ffffff'
    			 		}); 
    		 		jQuery('#resultLoading .bg').height('100%');
    		 		jQuery('#resultLoading').fadeIn(300);
    		 		jQuery('body').css('cursor', 'wait'); 
            } 
            
            function ajaxindicatorstop(){
            	jQuery('#resultLoading .bg').height('100%');
            	jQuery('#resultLoading').fadeOut(300);
            	jQuery('body').css('cursor', 'default');
            	} 

            function MessageAffiche(text){ 
            	if(jQuery('body').find('#messageAlert').attr('id') != 'messageAlert'){ 
            		jQuery('body').append('<div id="messageAlert" style="display:none"><div>'+text+'</div></div>'); 
            		} 
            } 
            </script>
        {/literal}
        
		{literal}	
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $('a[id*="Copy-"]').click(function(){
		      	var id = this.id;
		      	var ref = id.substring(5);
		      	$('#reforiginale').val(ref);
		      	$(".chkGroup").attr("checked", false);
		      	$(".chkGroup").prop("disabled", false);			      				      	 
		      	 $('label[id^="LBLCHK-"]').removeClass('hidden');
		      	 $("#LBLCHK-"+ref).addClass('hidden');
		     	  window.scrollTo(0, 0);  								     
	              $('#responsive').modal('show'); 					               
		        });                                                                  
		  });
		</script>
		
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $('#btnValid').click(function(){
		      		      
	      			var ref = $('#reforiginale').val();
	 				var valeurs = [];
					$('input:checked[name=copy_list]').each(function() {
					  valeurs.push($(this).val());
					});	               
					
					if(valeurs.length == 0){
						alert("rien n'a �t� s�lectionn�");							
					}else{	
					
	 		     		jQuery(document).ajaxStart(function () { 
	      		     	//show ajax indicator
	      		     	ajaxindicatorstart('Copie en cours...veuillez patienter...');
	      		     	 }).ajaxStop(function () { 
	      		     	 	//hide ajax indicator
	      		     	 	ajaxindicatorstop();
	      		     	 	});
									   
 					   $('#responsive').modal('hide');
 			           $.ajax({
                       type: 'POST',
                       url: 'ajax/ajax_copie_ref.php',
                       data: 'ref='+ref+'&copierefs='+valeurs,
                       dataType: "html",
                       success: function(response){                        		 	     
                       		jQuery.ajax({global: false});                            
                       		//alert(response);
			                if(response == 0){					                	
			                	var CodeRetour = "Copie termin�e.";			
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'success',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });							        	                			               
			                }else{					                	
			                	var CodeRetour = "Copie termin�e avec des erreurs.";
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'center',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });	
			                }                            		                                 		                    			                  				                       					                     												
	
        			      }
	                  }); 
					}									
		        });                                                                  
		  });
		</script>
		
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $('a[id*="btnsave-"]').click(function(){
		      	var id = this.id;
		      	var ref = id.substring(8);
				var CodeRetour = "Toutes les donn�es obligatoires ont �t� remplies (100%). Ces donn�es peuvent donc �tre transmises � E.CF et ce dossier ne sera donc plus modifiable"+"\n";
				CodeRetour +=  "Confirmez vous la validation de ces fiches ?";
		        var n = noty({
		            text        : CodeRetour,
		            type        : 'warning',
		            dismissQueue: true,
		            layout      : 'center',
		            theme       : 'defaultTheme',
		            buttons     : [
		                {addClass: 'btn btn-primary', text: 'Oui', onClick: function ($noty) {
		                    $noty.close();
		                    
    			            $.ajax({
		                       type: 'POST',
		                       url: 'ajax/ajax_transmettre.php',
		                       data: 'idref='+ref,
		                       dataType: "html",
		                       success: function(add_result){    
		                       		//alert(add_result);	       	                                      		                    			                  				                       					                     													
      								if(add_result.charAt(0) == '0' || add_result.charAt(0) == 0)  {
										noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: 'R�f�rence enregistr�e et transmise � ECF', type: 'success'});
										document.location.href = 'home.php';
	              	                } else if(add_result.charAt(0) == '4' || add_result.charAt(0) == 4) {
										noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: add_result, type: 'error',timeout : 3000 });
	              	                } else {
										noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: "Une erreur s'est prdouite lors de la transmission", type: 'error',timeout : 3000 });
	              	                }		
		        			      }
			                  });  
		                    
		                    
		                }
		                },
		                {addClass: 'btn btn-danger', text: 'Non', onClick: function ($noty) {
		                    $noty.close();
		                    noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: 'Transmission annul�e', type: 'error',timeout : 1000 });
		                }
		                }
		            ]
		        });
				
				
				     
		        });                                                                  
		  });
		</script>
		{/literal}
		{literal}	
		<script type="text/javascript">
		  $(document).ready(function(){					   		     	     			     		  
		      $('#selectstatus').change(function(){		       
		        $('#corpstbl').empty();		
		       	$('#divLoading').show();   		      		      	     
					$.ajax({
					 cache: false,
                     type: 'POST',
                     url: 'ajax/ajax_listeref.php',
                     data: "status="+$('#selectstatus').val(),
                     dataType: "html",
                     success: function(datas_result){                                	
                     	$('#divLoading').hide();
                     	$('#corpstbl').append(datas_result);            	                     	                     
					 },					 
					 complete: function(data) {     
					    $(function() {
						$('a[id*="Copy-"]').click(function(){
								      	var id = this.id;
								      	var ref = id.substring(5);
								      	$(".chkGroup").attr("checked", false);
								      	$(".chkGroup").prop("disabled", false);			      		
			      				      	$('label[id^="LBLCHK-"]').removeClass('hidden');
		      	 						$("#LBLCHK-"+ref).addClass('hidden');
								     	  window.scrollTo(0, 0);  								     
							              $('#responsive').modal('show'); 					               
					        });    
					    });
					  }                  
                  });   
                  				     	              				              
		        });                                                                  
		  });
		</script>
		{/literal}
  </body>
</html>






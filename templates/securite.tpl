



 {include file='header_ref.tpl'} 
        <div class="container">
          <div class="row">			
            <!-- Modal -->
  <div class="modal" id="myModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="closemodal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
          <label>{_("Chap�tre :")}</label>													              										          					
			<select id="SELCHPT">
			{foreach $Lstchp as $val}
				<option value="{$val['Q4N7C1']}">{$val['LIB']}</option>
			{/foreach}	                                   							          							              							                                    
   			</select>   
        </div>
        <div class="modal-body" >


      <div id="divLoading" style="display: none;">
        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
      </div>      
		  <table class="table table-hover">
		    <thead>
		      <tr>
		      	<th>{_("Code")}</th>
		        <th>{_("Libell�")}</th>        
		      </tr>
		    </thead>
		    <tbody  id="tblnmt">
		    {foreach $Lstnmclt as $val}
		      <tr class='clickable-row'  id="{$val['Q5HWT2']}">
		        <td>{$val['Q5HWT2']}</td>        
		        <td>{$val['Q5HXT2']}+{$val['Q5HYT2']}+{$val['Q5HZT2']}+{$val['Q5H0T2']}</td>
		      </tr>
		    {/foreach}
		    </tbody>
		  </table>   			   			         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="btnclosemodal">Close</button>
        </div>
      </div>
    </div>
  </div>   
  <!-- Fin Modal -->
  			<ol class="breadcrumb">                            
                {*<li>{_("Dossier : ")}</li>*}
                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
          	</ol> 	
			<div class="col-md-36">
							
				<div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title">
	                        <div class="pull-left">{_("S�curit� & logistique")}</div>
	                        {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div>{/if}
	                        <div class="clearfix"></div>
	                    </h3>
	                </div>
	                <form id="my_form"  method="post" enctype="multipart/form-data">
		                <div class="panel-body">
	                      <div id="divLoading" style="display: none;">
	                      	<p align="center">{_("Saving Datas...Please wait...")}</p>
					        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
					      </div>
	                	  <div class="form-inline">                	  
							<div class="form-error alert alert-danger hidden" id="MissingErr">																															
							</div>					
							<input type="hidden" name="STS" id="STS" value="N">	
						  </div>
						  
	 	                  <div class="well">
			          		<div class="form-inline">							                        									
								<h4><span class="label label-primary">{_("S�curit�")}</span></h4>													              										          					
		                	</div>
		                	<br>
			          		<div class="form-inline">
		                        <div class="form-group">							                        	
									<label><FONT size="2pt" color="red">{_("Produit comportant une mati�re dangereuse ?")}</FONT></label>													              										          					
	      							<select name="PRDDNG" id="PRDDNG" {if $Lstsecur['PRDDNG'] eq 3} disabled{/if}>
	          							<option value="1" {if $Lstsecur['PRDDNG'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstsecur['PRDDNG'] eq 2} selected{/if}>{_("Non")}</option>
	          							<option value="1" {if $Lstsecur['PRDDNG'] eq 3} selected{/if}>{_("Oui")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
		          				{* <span class="label label-danger">{_("Dans le cas d'un produit dangereux, tous les champs doivent �tre renseign�s")}</span> *}
		                	</div>
		                	<br>		                	
			          		<div class="form-inline">
		                        <div class="form-group">							                        	
									<label><FONT size="2pt" color="red">{_("Produit food")}</FONT></label>													              										          					
	      							<select name="A00D20" id="selalim">
	          							<option value="1" {if $Lstsecur['A00D20'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstsecur['A00D20'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
		                        <div class="form-group" id="divper">							                        	
									<label>{_("Produit p�rissable")}</label>													              										          					
	      							<select name="A00D25" id="selper">
	          							<option value="1" {if $Lstsecur['A00D25'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstsecur['A00D25'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
			          			<div class="form-group {if $Lstsecur['A00D25'] eq 2}hidden{/if}" id="lblper">  										          				  	          	
									  <label>{_("Dur�e de vie (en mois)")}</label>														  
									  <input type="text" size = "10" name ="A00D30" id="A00D30" value="{$Lstsecur['A00D30']}" onkeyup="validNumber(this)" onblur="validNumber(this)" >														  														
								</div>
		          			</div>	
		                	<br>
		                	
			          		<div class="form-inline">
		                        <div class="form-group">							                        	
									<label>{_("Produit certifi� conforme au contact alimentaire? ")}</label>													              										          					
	      							<select name="A00D35" id="selCE">
	          							<option value="1" {if $Lstsecur['A00D35'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstsecur['A00D35'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
		          				<div class="form-group {if $Lstsecur['A00D35'] eq 2}hidden{/if}" id="lblCE">							                        	
									<label>{_("Veuillez ins�rer votre certificat")}</label>													              										          					 																			               				                                               				
		          				</div>
		          				<div class="form-group {if $Lstsecur['A00D35'] eq 2}hidden{/if}" id="fileCE">							                        														              										          					 											
									<input type="file" class="form-control" name="FicheCE" id="FicheCE" accept=".pdf"><button type="button" id="btnannulerCE" class="btn btn-default btn-primary hidden">{_("Annuler")}</button>
									 <div class="form-group" style="margin-bottom: 0;">
		                                <div id="image_preview" class="col-lg-10 col-lg-offset-2">
		                                    <div class="thumbnail hidden">
		                                        <img src="http://placehold.it/5" alt="">
		                                        <div class="caption">
		                                            <h4></h4>
		                                            <p></p>
		                                            <p><button type="button" class="btn btn-default btn-danger">Annuler</button></p>
		                                        </div>
		                                    </div>
		                                </div>
		                              </div>              				                                               					
		          				</div>
		                	</div>
							<br>
							<div class="form-inline">
								<div class="input-group {if $CE eq ""}hidden{/if}" id="divficheactCE" > 
									<span class="input-group-addon" id="sizing-addon2">{_("Certificat alimentaire actuel : ")}</span><input type="text" id="ficheactCE" class="form-control" name="CE"  value="{$CE}" aria-describedby="sizing-addon2" disabled>														
								</div>								
							</div> 		                	
						  </div>
						  
		                	<div class="well">
				          		<div class="form-inline">							                        									
									<h4><span class="label label-primary">{_("Pays et Douane")}</span></h4>													              										          					
			                	</div>
			                	<br>
				          		<div class="form-inline">
			                        <div class="form-group">							                        	
										<label>{_("Nomenclature douani�re")}</label>	
										<input type="text" size = "8" maxlength="8" name="A00C90" value="{$A00C90}" id="A00C90" onkeyup="validNumber(this)" onblur="validNumber(this)">		
										 <button type="button" class="bbtn btn-primary" id="nomDisplay" >...</button>																			              										          															                                               				                                               				
			          				</div>
			                        <div class="form-group">							                        	
										<label>{_("Pays de fabrication")}</label>													              										          					
		      							<select name="A00C92" id="A00C92">
		      							{foreach $Lstpays as $val}
		          							<option value="{$val['P00001']}" {if $val['P00001'] eq $A00C92} selected{/if}>{$val['P00002']}</option>
		          						{/foreach}	                                        							          							              							                                     
		                       			</select>	                                               				                                               					
			          				</div>
			                        <div class="form-group">							                        	
										<label>{_("Si code non trouv�")}</label>													              										          					
	                                    <input type="text" size = "10" maxlength="20" name="NOMNTR" value="{$NOMNTR}" id="NOMNTR" onkeyup="validNumber(this)" onblur="validNumber(this)">           				                                               					
			          				</div>
			                	</div>
		                	</div>
		                	<div class="well">
				          		<div class="form-inline">							                        									
									<h4><span class="label label-primary">{_("Conditionnement")}</span></h4>																					              										          				
			                	</div>
			                	<br>		                	
			                	<div class="form-inline">	   	
				          			<div class="form-group">						                        	
										<label>{_("Code EAN de l'UNITE")}</label>		
										<input type="text" size = "14" maxlength="13" name="EANUNITE" value="{$EANUNITE}" id="EANUNITE" onkeyup="validNumber(this)" onblur="validNumber(this)"> 
									</div>     										              										          					
			                	</div>
			                	<br>	
				          		<div class="form-inline">	
				          			<div class="form-group">						                        	
										<label>{_("INNER : Nombre d'unit�")}</label>		
										<input type="text" size = "10" value="{$A00C25}" name="A00C25" id="A00C25" onkeyup="validNumber(this)" onblur="validNumber(this)">	
									</div>   	
				          			<div class="form-group">						                        	
										<label>{_("Code EAN de l'INNER")}</label>		
										<input type="text" size = "14" maxlength="13" name="A00C95" value="{$A00C95}" id="A00C95" onkeyup="validNumber(this)" onblur="validNumber(this)">
									</div>
				          			<div class="form-group">						                        										
										<span class="label label-danger">{_("Les codes EAN sont constitu�s de 13 chiffres")}</span>								
									</div>   	   										              										          					
			                	</div>
			                	<br>
				          		<div class="form-inline">	
				          			<div class="form-group">						                        	
										<label>{_("MASTER : Nombre d'unit�")}</label>		
										<input type="text" size = "10" name="A00C50" value="{$A00C50}" id="A00C50" onkeyup="validNumber(this)" onblur="validNumber(this)">
									</div>   	
				          			<div class="form-group">						                        	
										<label>{_("Code EAN du MASTER")}</label>		
										<input type="text" size = "14" maxlength="13" name="A00D10" value="{$A00D10}" id="A00D10" onkeyup="validNumber(this)" onblur="validNumber(this)">
									</div>
				          			<div class="form-group">						                        										
										<span class="label label-danger">{_("Les codes EAN sont constitu�s de 13 chiffres")}</span>								
									</div>     										              										          					
			                	</div>
		                	</div>
						  						  						  						  	
				        </div>
			        </form>
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='securite_js.tpl'}

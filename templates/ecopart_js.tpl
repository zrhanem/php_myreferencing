    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
    <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>
		
		{include file='googleanalytics.tpl'}
		
		{literal}       
            <script type="text/javascript">
            function ajaxindicatorstart(text){ 
            	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){ 
            		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="photos/icons/ajax-loader.gif"><div>'+text+'</div></div><div class="bg"></div></div>'); 
            		} 
        		jQuery('#resultLoading').css({ 
        			'width':'100%','height':'100%','position':'fixed', 'z-index':'10000000','top':'0',
        			'left':'0','right':'0','bottom':'0','margin':'auto'
        			 }); 
      			jQuery('#resultLoading .bg').css({ 
      			 	'background':'#000000','opacity':'0.7','width':'100%','height':'100%','position':'absolute',
      			 	'top':'0' 
      			 	}); 
    			 	jQuery('#resultLoading>div:first').css({
    			 		'width': '250px','height':'75px','text-align': 'center','position': 'fixed','top':'0', 
    			 		'left':'0','right':'0', 'bottom':'0','margin':'auto','font-size':'16px','z-index':'10','color':'#ffffff'
    			 		}); 
    		 		jQuery('#resultLoading .bg').height('100%');
    		 		jQuery('#resultLoading').fadeIn(300);
    		 		jQuery('body').css('cursor', 'wait'); 
            } 
            
            function ajaxindicatorstop(){
            	jQuery('#resultLoading .bg').height('100%');
            	jQuery('#resultLoading').fadeOut(300);
            	jQuery('body').css('cursor', 'default');
            	} 

            function MessageAffiche(text){ 
            	if(jQuery('body').find('#messageAlert').attr('id') != 'messageAlert'){ 
            		jQuery('body').append('<div id="messageAlert" style="display:none"><div>'+text+'</div></div>'); 
            		} 
            } 
            </script>
        {/literal}
		
		{literal} 
			<script type="text/JavaScript">
				function validNumber(f) {		
				!(/^[0-9.]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9.]/ig,''):null;
				} 
			</script>      
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $('#btnValid').click(function(){						
					
   		     		jQuery(document).ajaxStart(function () { 
      		     	//show ajax indicator
      		     	ajaxindicatorstart('Saving datas...please wait...');
      		     	 }).ajaxStop(function () { 
      		     	 	//hide ajax indicator
      		     	 	ajaxindicatorstop();
      		     	 	});
					
					var dec = $('#seldecret').val();
					var fam = $('#selfam').val();
					var mat = $('#selmat').val();
					var poids = $('#poids').val();
					var Chaine = 'FAM='+fam+'&DEC='+dec+'&MAT='+mat+'&PDS='+poids+'&STS=O';
					
	                $.ajax({
                       type: 'POST',
                       url: 'ajax/ajax_ecop.php',
                       data: Chaine,
                       dataType: "html",
                       success: function(add_result){	    			                                               		                     						       	                       			       				                       					                  	                                                                                                                              		                 
                        	jQuery.ajax({global: false});                          	                       	             
                        	if(add_result == 0){    
			                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'success',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'top',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });
                        		location.reload();	
                        	}else if(add_result == 2){                        	                        		
			                	var CodeRetour = "Toutes les donn�es n'ont pas �t� envoy�es";
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'top',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });	  
						        location.reload();                		
                        	}else{
			                	var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'error',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'top',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });	
						        location.reload();
                        	}

        		        }
                    });					
						               
		        });                                                                  
			  });
			</script>		
		{/literal}
        
  </body>
</html>






    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/validator/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/validator/jquery.validate.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
     <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>
       
      {include file='googleanalytics.tpl'}  
             
      {literal}		
  			<script>						
			$(window).load(function(){
				$('#calc1').change();
				$('#Ecalc1').change();
				$('#CFcalc1').change();
			    $('#accordE').click();
			    $('#accordL').click();		
			    $('#selectuntach').change();	
			    $('#selectuntfac').change();    					      
			});
			</script>

			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#selectuntach').change(function(){		          		          		    
			     	var unite =  $("select[id='selectuntach'] > option:selected").text();
			     	unite += '(de ' + $('#A00A30').val() +  ')';  
			     	$("span[id*='sel']").text(unite);											           										           
			        });                                                                   
			  });
			</script>						 
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#selectuntfac').change(function(){		          		          		    
			     	var unite =  $("select[id='selectuntfac'] > option:selected").text();		     	        		          				   													           																	     
					$("span[id*='sefac']").text(unite);											           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#selectlivecotel').change(function(){		          		          		    
			     	var selval =  $("select[id='selectlivecotel'] > option:selected").val();		     	        		          				   													           																	     								
					$("select[id='selectlivcorpo']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			
			
			<script type="text/javascript">
			/* //////////////////////Dupplication des saisies sur le pav� des conditions Corpo */
			  $(document).ready(function(){					   		     	     	
			     $('#Ecalc1').change(function(){		          		          		    
			     	var selval =  $("select[id='Ecalc1'] > option:selected").val();		     	        		          				   													           																	     								
					$("select[id='calc1']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Ecalc2').change(function(){		          		          		    
			     	var selval =  $("select[id='Ecalc2'] > option:selected").val();		     	        		          				   													           																	     								
					$("select[id='calc2']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Ecalc3').change(function(){		          		          		    
			     	var selval =  $("select[id='Ecalc3'] > option:selected").val();		     	        		          				   													           																	     								
					$("select[id='calc3']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Ecalc4').change(function(){		          		          		    
			     	var selval =  $("select[id='Ecalc4'] > option:selected").val();		     	        		          				   													           																	     								
					$("select[id='calc4']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Efranco').change(function(){		          		          		    
			     	var selval =  $("input[id='Efranco']").val();		     	        		          				   													           																	     								
					$("input[id='franco']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Erem1').change(function(){		          		          		    
			     	var selval =  $("input[id='Erem1']").val();		     	        		          				   													           																	     								
					$("input[id='rem1']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Erem2').change(function(){		          		          		    
			     	var selval =  $("input[id='Erem2']").val();		     	        		          				   													           																	     								
					$("input[id='rem2']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Erem3').change(function(){		          		          		    
			     	var selval =  $("input[id='Erem3']").val();		     	        		          				   													           																	     								
					$("input[id='rem3']").val(selval);							           										           
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#Erem4').change(function(){		          		          		    
			     	var selval =  $("input[id='Erem4']").val();		     	        		          				   													           																	     								
					$("input[id='rem4']").val(selval);							           										           
			        });                                                                   
			  });
			</script>			
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#EJRAZP4').change(function(){		          		          		    
			     	var selval =  $("input[id='EJRAZP4']").val();		     	        		          				   													           																	     								
					$("input[id='LJRAZP4']").val(selval);			
					Lcalcul();				           										           
			        });                                                                   
			  });
			</script>			
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#EJRQ7Q1').change(function(){		          		          		    
			     	var selval =  $("input[id='EJRQ7Q1']").val();		     	        		          				   													           																	     								
					$("input[id='LJRQ7Q1']").val(selval);											           										          
			        });                                                                   
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     	
			     $('#EJRAXN7').change(function(){		          		          		    
			     	var selval =  $("textarea[id='EJRAXN7']").val();		     	        		          				   													           																	     								
					$("textarea[id='CJRAXN7']").val(selval);											           										          
			        });                                                                   
			  });
			  /* ///////////////////////////////////////////////////////////////////////////*/
			</script>			
			
			
			
			
	        <script type="text/JavaScript">
				function valid(f) {
				!(/^[A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]*$/i).test(f.value)?f.value = f.value.replace(/[^A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]/ig,''):null;
				} 
			</script>
			<script type="text/JavaScript">
				function validNumber(f) {		
				!(/^[0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9]/ig,''):null;
				} 
			</script>
			<script type="text/JavaScript">
				function validFloat(f) {		
				!(/^[0-9.]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9.]/ig,''):null;
				} 
			</script>
			<script type="text/javascript">
				  $(document).ready(function(){					   		     	     			     		  
				      $('#btnValid').click(function(){					      	
						$('form#my_form').submit();								              
				        });                                                                  
				  });
			</script>		
			<script type="text/javascript">
			(function( $ ){
			   $.fn.calcPNUC = function(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,societe) {
			      
							var va30 = $("#A00A30").val();
							var va40 = $("#A00A40").val();
							var va45 = $("#A00A45").val();	
							
              			    switch(societe) {
                  			    case "CF":                     
 									va45 = $("#A00A45").val();
                  			        break;
                  			    case "E":
									va45 = $("#EJRAZP4").val();
                  				      break;					
                				case "C":
									va45 = $("#LJRAZP4").val();
                    			      break;						 	
                  				}	
							
			      			var calculIntermediaire = 0;
				      		var PrixAchatBrutUnitaire = 0;
				      		var PA_NETCAL = 0;				      				      			
																					
							if(va40 > 0){
								PrixAchatBrutUnitaire = (va45 / va40) * va30;
							}else{
								PrixAchatBrutUnitaire = va45;
							}
							
							PA_NETCAL=PrixAchatBrutUnitaire ;
							
							//alert(pust+'-'+pvst+'-'+pwst+'-'+pxst+'-'+jjnb+'-'+jknb+'-'+jlnb+'-'+jmnb);
														
							if((pust != 1) && (jjnb > 0)){							
								if(pust == 2){
									calculIntermediaire = (PrixAchatBrutUnitaire * jjnb)/100;
									PA_NETCAL = PrixAchatBrutUnitaire - calculIntermediaire ;
								}
								if(pust == 3){
									PA_NETCAL=PrixAchatBrutUnitaire * jjnb	;
								}							
							}
							
							if((pvst != 1) && (jknb > 0)){							
								if(pvst == 2){
									calculIntermediaire = (PA_NETCAL * jknb)/100;
									PA_NETCAL = PA_NETCAL - calculIntermediaire ;
								}
								if(pvst == 3){
									PA_NETCAL=PA_NETCAL * jknb	;
								}							
							}							
							
							if((pwst != 1) && (jlnb > 0)){							
								if(pwst == 2){
									calculIntermediaire = (PA_NETCAL * jlnb)/100;
									PA_NETCAL = PA_NETCAL - calculIntermediaire ;
								}
								if(pwst == 3){
									PA_NETCAL=PA_NETCAL * jlnb	;
								}							
							}
									
							if((pxst != 1) && (jmnb > 0)){							
								if(pxst == 2){
									calculIntermediaire = (PA_NETCAL * jmnb)/100;
									PA_NETCAL = PA_NETCAL - calculIntermediaire ;
								}
								if(pxst == 3){
									PA_NETCAL=PA_NETCAL * jmnb	;
								}							
							}
															
							
			      		return PA_NETCAL;
			   }; 
			})( jQuery );
			</script>

 
			<script type="text/javascript">
			/* calcul du prix net dans l'unit� de commande CORPO */
				  $(document).ready(function(){					   		     	     			     		  
				      $("[id^='calc']").change(function(){				
				      				      			      
			      			var pust = $("#calc1 option:selected").val();	
							var pvst = $("#calc2 option:selected").val();
							var pwst = $("#calc3 option:selected").val();
							var pxst = $("#calc4 option:selected").val();
							var jjnb = $("#calc-jjnb").val();	
							var jknb = $("#calc-jknb").val();
							var jlnb = $("#calc-jlnb").val();
							var jmnb = $("#calc-jmnb").val();
				      			      		      		
 							$("#LPNUC").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'C'));											      																			           
				        });                                                                  
				  });
			</script>
			
 
			<script type="text/javascript">
			/* calcul du prix net dans l'unit� de commande ECOTEL*/
				  $(document).ready(function(){					   		     	     			     		  
				      $("[id^='Ecalc']").change(function(){				
				      				      			      
			      			var pust = $("#Ecalc1 option:selected").val();	
							var pvst = $("#Ecalc2 option:selected").val();
							var pwst = $("#Ecalc3 option:selected").val();
							var pxst = $("#Ecalc4 option:selected").val();
							var jjnb = $("#Ecalc-jjnb").val();	
							var jknb = $("#Ecalc-jknb").val();
							var jlnb = $("#Ecalc-jlnb").val();
							var jmnb = $("#Ecalc-jmnb").val();
				      			      		      		
 							$("#EPNUC").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'E'));											      																			           
				        });                                                                  
				  });
			</script>
			

			<script type="text/javascript">
			/* calcul du prix net dans l'unit� de commande ECF */
				  $(document).ready(function(){					   		     	     			     		  
				      $("[id^='CFcalc']").change(function(){				
				      				      			      
			      			var pust = $("#CFcalc1 option:selected").val();	
							var pvst = $("#CFcalc2 option:selected").val();
							var pwst = $("#CFcalc3 option:selected").val();
							var pxst = $("#CFcalc4 option:selected").val();
							var jjnb = $("#CFcalc-jjnb").val();	
							var jknb = $("#CFcalc-jknb").val();
							var jlnb = $("#CFcalc-jlnb").val();
							var jmnb = $("#CFcalc-jmnb").val();
				      			      		      		
 							$("#PNUA").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'CF'));											      																			           
				        });                                                                  
				  });
			</script>
			<script type="text/JavaScript">
				function CFcalcul() {
	      			var pust = $("#calc1 option:selected").val();	
					var pvst = $("#calc2 option:selected").val();
					var pwst = $("#calc3 option:selected").val();
					var pxst = $("#calc4 option:selected").val();
					var jjnb = $("#calc-jjnb").val();	
					var jknb = $("#calc-jknb").val();
					var jlnb = $("#calc-jlnb").val();
					var jmnb = $("#calc-jmnb").val();
   		      		
					$("#PNUA").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'CF'));
						
				} 
			</script>
			<script type="text/JavaScript">
				function Ecalcul() {
		      			var pust = $("#Ecalc1 option:selected").val();	
						var pvst = $("#Ecalc2 option:selected").val();
						var pwst = $("#Ecalc3 option:selected").val();
						var pxst = $("#Ecalc4 option:selected").val();
						var jjnb = $("#Ecalc-jjnb").val();	
						var jknb = $("#Ecalc-jknb").val();
						var jlnb = $("#Ecalc-jlnb").val();
						var jmnb = $("#Ecalc-jmnb").val();
			      			      		      		
						$("#EPNUC").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'E'));	
							
					} 
			</script>
			<script type="text/JavaScript">
				function Lcalcul() {
		      			var pust = $("#calc1 option:selected").val();	
						var pvst = $("#calc2 option:selected").val();
						var pwst = $("#calc3 option:selected").val();
						var pxst = $("#calc4 option:selected").val();
						var jjnb = $("#calc-jjnb").val();	
						var jknb = $("#calc-jknb").val();
						var jlnb = $("#calc-jlnb").val();
						var jmnb = $("#calc-jmnb").val();
			      			      		      		
						$("#LPNUC").val($.fn.calcPNUC(pust,pvst,pwst,pxst,jjnb,jknb,jlnb,jmnb,'C'));	
							
					} 
			</script>
									
		{/literal}
		
		
		{literal}                
				
			<script type="text/javascript">	
			function myTrim(x) {
			    return x.replace(/^\s+|\s+$/gm,'');
			}
			$(document).ready(function(){			
			    $('form#my_form').on('submit', function (e) {

			        // On emp�che le navigateur de soumettre le formulaire
			        e.preventDefault();
			        			        
					var msg = "";
					var err = 0;			        			        
			        			        
					if ( $.trim($('#A00B15').val()) == '' || $.trim($('#A00B15').val()) == 0){
						msg += "Vous devez saisir le prix conseill� client final"+"<br>";
						err = 1;
					}
					if ( $.trim($('#A00F80').val()) == '' || $.trim($('#A00F80').val()) == 0){
						msg += "Vous devez saisir la quantit� multiple d'achat"+"<br>";
						err = 1;
					}
					if ( $.trim($('#A00B10').val()) == '' || $.trim($('#A00B10').val()) == 0){
						msg += "Vous devez saisir le seuil minimum de commande"+"<br>";
						err = 1;
					}
					if ( $.trim($('#A00A45').val()) == '' || $.trim($('#A00A45').val()) == 0){
						msg += "Vous devez saisir un prix d'achat dans l'unit� de facturation"+"<br>";
						err = 1;
					}
					if ( $.trim($('#A00A40').val()) == '' || $.trim($('#A00A40').val()) == 0){
						msg += "Vous devez saisir un nombre de pi�ce dans l'unit� de facturation"+"<br>";
						err = 1;
					}
					if ( $.trim($('#A00A30').val()) == '' ||  $.trim($('#A00A30').val()) == 0){
						msg += "Vous devez saisir un nombre de pi�ces par unit� d'achat"+"<br>";
						err = 1;
					}
					if ( ($('#selectuntfac').val() == '-')  &&  ($.trim($('#A00A36').val()) == '')){
						msg += "Vous devez saisir une unit� de facturation"+"<br>";
						err = 1;
					}
					if ( ($('#selectuntach').val() == '-')  &&  ($.trim($('#A00A26').val()) == '')){
						msg += "Vous devez saisir une unit� d'achat"+"<br>";
						err = 1;
					}										


					
					var B10 = $.trim($('#A00B10').val());
					var F80 = $.trim($('#A00F80').val());

					if (($.trim($('#A00B10').val()) != '') && ($.trim($('#A00B10').val()) > 0)) {
						if ( ($.trim($('#A00F80').val()) != '') && ($.trim($('#A00F80').val()) != '')) {
							if( ( B10 % F80 ) > 0 ){
								msg += "Le seuil minimum de commande doit �tre un multiple de la QMA(Quantit� Multiple d'Achat)"+"<br>";
								err = 1;							
							} 
						}
					}
					
					
					
					if($('#selectlivecotel').val() == 1){
						if ( $.trim($('#EJRQ7Q1').val()) == '' || $.trim($('#EJRQ7Q1').val()) == 0){
							msg += "Vous devez saisir un seuil minimum de commande ECOTEL"+"<br>";
							err = 1;
						}
						if ( $.trim($('#EJRAZP4').val()) == '' || $.trim($('#EJRAZP4').val()) == 0){
							msg += "Vous devez saisir un Prix d'achat dans l'unit� facturation ECOTEL"+"<br>";
							err = 1;
						}
					}
					
					if($('#selectlivcorpo').val() == 1){
						if ( $.trim($('#LJRQ7Q1').val()) == '' || $.trim($('#LJRQ7Q1').val()) == 0){
							msg += "Vous devez saisir un seuil minimum de commande CORPO"+"<br>";
							err = 1;
						}	
						if ( $.trim($('#LJRAZP4').val()) == '' || $.trim($('#LJRAZP4').val()) == 0){
							msg += "Vous devez saisir un Prix d'achat dans l'unit� facturation CORPO"+"<br>";
							err = 1;
						}
					}
					
					
													
										
					// Remises ECF
					if( $.trim($('#CFcalc-jjnb').val()) > 0 && $('#CFcalc1').val() == 1 ){
						msg += "Vous devez v�rifier vos remises Centrales ECF"+"<br>";
						err = 1;
					}else if( $.trim($('#CFcalc-jknb').val()) > 0 && $('#CFcalc2').val() == 1 ){
						msg += "Vous devez v�rifier vos remises Centrales ECF"+"<br>";
						err = 1;					
					}else if( $.trim($('#CFcalc-jlnb').val()) > 0 && $('#CFcalc3').val() == 1 ){
						msg += "Vous devez v�rifier vos remises Centrales ECF"+"<br>";
						err = 1;
					}else if( $.trim($('#CFcalc-jmnb').val()) > 0 && $('#CFcalc4').val() == 1 ){
						msg += "Vous devez v�rifier vos remises Centrales ECF"+"<br>";
						err = 1;
					}
					
					//Remises ECOTEL
					if($('#selectlivecotel').val() == 1){
						if( $.trim($('#Ecalc-jjnb').val()) > 0 && $('#Ecalc1').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct ECOTEL"+"<br>";
							err = 1;
						}else if( $.trim($('#Ecalc-jknb').val()) > 0 && $('#Ecalc2').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct ECOTEL"+"<br>";
							err = 1;					
						}else if( $.trim($('#Ecalc-jlnb').val()) > 0 && $('#Ecalc3').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct ECOTEL"+"<br>";
							err = 1;
						}else if( $.trim($('#Ecalc-jmnb').val()) > 0 && $('#Ecalc4').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct ECOTEL"+"<br>";
							err = 1;
						}
					}	
								
					//Remises CORPO
					if($('#selectlivcorpo').val() == 1){
						if( $.trim($('#calc-jjnb').val()) > 0 && $('#calc1').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct CORPO"+"<br>";
							err = 1;
						}else if( $.trim($('#calc-jknb').val()) > 0 && $('#calc2').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct CORPO"+"<br>";
							err = 1;					
						}else if( $.trim($('#calc-jlnb').val()) > 0 && $('#calc3').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct CORPO"+"<br>";
							err = 1;
						}else if( $.trim($('#calc-jmnb').val()) > 0 && $('#calc4').val() == 1 ){
							msg += "Vous devez v�rifier vos remises Condition direct CORPO"+"<br>";
							err = 1;
						}		
					}	
					
					if(err == 1){
						$('#STS').val("N");
						$('#MissingErr').removeClass('hidden');	
						$('#MissingErr').html("");
						$('#MissingErr').html("<strong>"+msg+"</strong>");		
						
		                var r = confirm("Vous avez des champs obligatoires non remplis, enregistrer?");
	        			if (r == true) {
							
							$('#divLoading').show(); 
		        			var formData = new FormData($('form#my_form')[0]);
			        	        	   					        					      
					        $.ajax({
					            url: 'ajax/ajax_fileupload_condition.php',
					            type: 'POST',
					            data: formData,
							    async: false,
							    cache: false,
							    contentType: false,
							    processData: false,				            
					            success: function (response) {	
					            	$('#divLoading').hide(); 				            					            						            						            		            	
					                // La r�ponse du serveur						                
					                if(response == 0){					                	
					                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'success',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });	
								        location.reload();										        				                			              
					                }else{					                	
					                	var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'error',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });	
								        location.reload();	
					                }						        	               				                
					            }
				        	});
				        		
	        			}else{        				        				
		                	var CodeRetour = "Enregistrement annul�";
					        var n = noty({
					            text        : CodeRetour,
					            type        : 'warning',
					            dismissQueue: true,
					            timeout     : 5000,
					            closeWith   : ['click'],
					            layout      : 'top',
					            theme       : 'defaultTheme',
					            maxVisible  : 5
					        });	  				
	        			}
	        														
					}else{
												
						$('#STS').val("O");
						$('#divLoading').show(); 
					
						$('#MissingErr').addClass('hidden');
						$('#MissingErr').empty();
						
	        			var formData = new FormData($('form#my_form')[0]);
		        	        	   					        					      
				        $.ajax({
				            url: 'ajax/ajax_fileupload_condition.php',
				            type: 'POST',
				            data: formData,
						    async: false,
						    cache: false,
						    contentType: false,
						    processData: false,				            
				            success: function (response) {	
				            	$('#divLoading').hide(); 				            					            						            						            		            	
				                // La r�ponse du serveur						                
				                if(response == 0){
				                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
							        var n = noty({
							            text        : CodeRetour,
							            type        : 'success',
							            dismissQueue: true,
							            timeout     : 5000,
							            closeWith   : ['click'],
							            layout      : 'top',
							            theme       : 'defaultTheme',
							            maxVisible  : 5
							        });				
							        location.reload();		
							                       			               
				                }else{					                	
				                	var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
							        var n = noty({
							            text        : CodeRetour,
							            type        : 'error',
							            dismissQueue: true,
							            timeout     : 5000,
							            closeWith   : ['click'],
							            layout      : 'top',
							            theme       : 'defaultTheme',
							            maxVisible  : 5
							        });
							        location.reload();		
				                }						        	               				                
				            }
			        	});
						
					}
										        				        
															        
			    });
			  });					
			</script>	
			
		{/literal}
		

        
  </body>
</html>






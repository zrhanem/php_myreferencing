<!DOCTYPE html>
<html class="no-js">
  <head>
    <title>My Referencing</title>
 {* <meta charset="utf-8"> *} 
 	 <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" /> 
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,600">          	    
   <link rel="stylesheet" href="assets/css/main.min.css">  
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">  				 
  <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">     	
  <link rel="stylesheet" href="assets/css/mybootstrap.css"  type="text/css" />
  <link rel="stylesheet" href="assets/css/mymodal.css">
  {* <link rel="stylesheet" href="assets/css/validator/screen.css"> *}   
     
 	

   
	<style>
      .btn-large { width:100%; }
    </style>

    
  </head>
  <body>
    <div class="offcanvas"> 
      <div id="wrapper" class="wrapper">
        <header role="banner" class="header">

          <div class="header-container container">
            <h1 class="header-logo" >
            {if $smarty.session.IDFRS eq ''}
                <a href="index.php" style="background-color: transparent;">
            {/if}    
            <a href="home.php" style="background-color: transparent;">
            <img src="photos/logos/ECF_logo.jpg"></a>
            </h1>
            <div class="header-links">
 
              <div class="header-links__item header-links__item--account">
                <a href="pdf/Help_Myreferencing.pdf" target="blank">            
                <i class="icon icon-bag"></i><strong>{_("Documentation")}</strong><span>{_("Aide")}</span></a>
              </div>
             
             <div class="header-links__item header-links__item--cart"><a href="ajax/ajax_logout.php"><i class="icon icon-cancel"></i><strong>{_("D�connexion")}</strong></a></div>               
              </div>
            <button id="toggle-offcanvas" type="button" class="header-toggle hidden-md hidden-lg"><span class="icon-burger"></span></button>
            {* <form role="search" class="header-search" method="get" action="search.php">
              <input type="search" placeholder="{_("Recherche rapide de produits (mots-cl&eacute;s, r&eacute;f&eacute;rence..)")}" class="form-control" id="query" name="query">
              <button type="submit" class="btn">OK</button>
            </form> *}
            <div class="header-welcome"><span>{_("Bienvenue")}!</span><small>{_("Vous &ecirc;tes connect&eacute;(e) &agrave; votre espace Partenaire.")}</small></div>
          </div>
          <nav role="navigation" class="header-navigation">
            <div class="container">
              <div class="btn-group btn-group-justified" role="group" aria-label="...">
				  <div class="btn-group" role="group">
				    <button type="button" class="btn btn-primary"  onclick="location.href='home.php';">{_("Accueil")}</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button type="button" class="{$classdes}" onclick="location.href='descriptif.php?ref={$smarty.session.CURREF}';">{_("Descriptif")}</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button type="button" class="{$classcond}" onclick="location.href='condition.php?ref={$smarty.session.CURREF}';">{_("Condition")}</button>
				  </div>
				   <div class="btn-group" role="group">
				    <button type="button" class="{$classsec}" onclick="location.href='securite.php?ref={$smarty.session.CURREF}';">{_("S�curit� & Logistique")}</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button type="button" class="{$classeco}" onclick="location.href='ecopart.php?ref={$smarty.session.CURREF}';">{_("Eco-Participation")}</button>
				  </div>
				</div> 
            </div>
          </nav>
        </header>
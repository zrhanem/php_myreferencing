


 {include file='header_ref.tpl'} 
        <div class="container">
          <div class="row">
			<ol class="breadcrumb">                            
                {* <li>{_("Dossier : ")}</li> *}
                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
          	</ol>    	
			<div class="col-md-36">
			
			{* Div pour infos cach�s *}
			<div class="modal fade in" id="dialValid" tabindex="-1" role="dialog" aria-labelledby="dialValid" aria-hidden="true" style="position:fixed;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			
			      </div>
			      <div class="modal-body">
			
					<input type="text"  size = "5" id="A00A25"  value="{$A00A25}">
					<input type="text"  size = "5" id="A00A35"  value="{$A00A35}">
					<input type="text"  size = "5" id="A00A60"  value="{$A00A60}">
					<input type="text"  size = "5" id="A00A70"  value="{$A00A70}">
					<input type="text"  size = "5" id="A00A80"  value="{$A00A80}">
					<input type="text"  size = "5" id="A00A90"  value="{$A00A90}">
					<input type="text"  size = "5" id="TVAPRD"  value="{$TVAPRD}">
					<input type="text"  size = "5" id="A00A50"  value="{$A00A50}">
					<input type="text"  size = "5" id="A00A55"  value="{$A00A55}">
					<input type="text"  size = "5" id="A00B25"  value="{$A00B25}">
			
			      </div>
			      <div class="modal-footer">
			
			      </div>
			    </div>
			  </div>
			</div>
			{* Fin Dialogue modal pour validation commande *} 
				
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <div class="pull-left">{_("Conditions")}</div>                                                                       
                                    {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div> {/if}
                                    <div class="clearfix"></div>
                                </h3>
                            </div>
                            <form id="my_form"  method="post" enctype="multipart/form-data">
                            <div class="panel-body">
        	                      <div id="divLoading" style="display: none;">
			                      	<p align="center">{_("Saving Datas...Please wait...")}</p>
							        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
							      </div>
        	                	<div class="form-inline">
									<div class="form-error alert alert-danger hidden" id="MissingErr">																															
									</div>	
									<input type="hidden" name="STS" id="STS" value="N">					
								</div>	
								<br>
                                <div id="accordion2" class="panel-group">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#collapse2One"  data-toggle="collapse" class="accordion-toggle">{_("Condition Centrale ECF")}</a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="collapse2One">
                                            <div class="panel-body">
                                            
	                                            <div class="form-inline">                
								                    <div class="supervisor-date-form input-daterange">								                    								                    
								                       <div class="form-group form-group-sm">
						              						<label for="selectsupervisestab">Unit� achat :</label>							          						 
						          							<select class="form-control" id="selectuntach" name="A00A25">
							          							{foreach $ListeUnt as $val}
							          								{if $val['M00001'] eq {$A00A25} }
							          									<option value="{$val['M00001']}" selected>{$val['M00002']}</option>
								          							{else}
								          								<option value="{$val['M00001']}">{$val['M00002']}</option>
								          							{/if}							          													          						
								          						{/foreach}	                                          							          							              							                                     
	                                               			</select>
								          				</div>
													    <div class="input-group input-group-sm">
													      <span class="input-group-addon">
													        <input type="checkbox"{if $A00A26 neq ''}  checked{/if}>{_(" Autre")}
													      </span>
													      <input type="text" class="form-control" id="A00A26" maxlength="100" name="A00A26" value="{$A00A26}">											      													      													     
													    </div>
													    
													  	<div class="input-group input-group-sm">
														  <span class="input-group-addon" id="sizing-addon3">{_("Nb pi�ces par unit� d'achat :")}</span>
														  <input type="text" class="form-control" aria-describedby="sizing-addon3" id="A00A30" name="A00A30" value="{$A00A30}" onkeyup="validNumber(this) ;CFcalcul();" onblur="validNumber(this)" >
														</div>

								                    </div>
								          		</div>
								          		<br>
	                                            <div class="form-inline">                
								                    <div class="supervisor-date-form input-daterange">
								                       <div class="form-group form-group-sm">
						              						<label for="selectsupervisestab">{_("Unit� facturation :")}</label>							          						 
						          							<select class="form-control" id="selectuntfac" name="A00A35">
						          								{foreach $ListeUnt as $val}
						          									{if $val['M00001'] eq {$A00A35} }
							          									<option value="{$val['M00001']}" selected>{$val['M00002']}</option>
							          								{else}
							          									<option value="{$val['M00001']}">{$val['M00002']}</option>
							          								{/if}
							          							{/foreach}	                                          							          							              							                                     
	                                               			</select>
								          				</div>
                                          			                                            	
													    <div class="input-group input-group-sm">
													      <span class="input-group-addon">
													        <input type="checkbox"{if $A00A36 neq ''}  checked{/if}>{_(" Autre")}
													      </span>
													      <input type="text" class="form-control" id="A00A36" maxlength="100"  name="A00A36" value="{$A00A36}">
													    </div>
	                                            		
													  	<div class="input-group input-group-sm">
														  <span class="input-group-addon" id="sizing-addon3">{_("Nb pi�ces par unit� de facturation :")}</span>
														  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" id="A00A40" name="A00A40" value="{$A00A40}" onkeyup="validNumber(this);CFcalcul();" onblur="validNumber(this)" >
														</div>
	                                            		
													  	<div class="input-group input-group-sm">
														  <span class="input-group-addon" id="sizing-addon3">{_("Prix achat par unit� de facturation :")}</span>
														  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" id="A00A45" name="A00A45" value="{$A00A45}" onkeyup="validFloat(this);CFcalcul();" onblur="validFloat(this)" >
														</div>
														
								                    </div>
								          		</div>
								          		<br>								          		
								          		<div class="form-inline">													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Franco")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "3" name="FRANC1" value="{$FRANC1}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 1")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="CFcalc-jjnb" name="A00A65" value="{$A00A65}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
																										
							                        <div class="form-group form-group-sm">							                        				              										          						
						          							<select class="form-control" name="A00A60" id="CFcalc1">
						          								<option value="1" {if $A00A60 eq '1'} selected{/if}></option>
							          							<option value="2" {if $A00A60 eq '2'} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $A00A60 eq '3'} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>	                                               				                                               					
							          				</div>

												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 2")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="CFcalc-jknb" name="A00A67" value="{$A00A75}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>

							                        <div class="form-group form-group-sm">							                        		              										          						
						          							<select class="form-control" name="A00A70" id="CFcalc2">
						          								<option value="1" {if $A00A70 eq '1'} selected{/if}></option>
							          							<option value="2" {if $A00A70 eq '2'} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $A00A70 eq '3'} selected{/if}>{_("X")}</option>                                         							          							              							                                     
	                                               			</select>
							          				</div>

												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 3")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="CFcalc-jlnb" name="A00A85" value="{$A00A85}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							          				
							                        <div class="form-group form-group-sm">							                        				              										          						
						          							<select class="form-control" name="A00A80" id="CFcalc3">
						          								<option value="1" {if $A00A80 eq '1'} selected{/if}></option>
							          							<option value="2" {if $A00A80 eq '2'} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $A00A80 eq '3'} selected{/if}>{_("X")}</option>                                        							          							              							                                     
	                                               			</select>
							          				</div>

												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 4")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="CFcalc-jmnb" name="A00A95" value="{$A00A95}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							                        <div class="form-group form-group-sm">							                        				              										          						
						          							<select class="form-control" name="A00A90" id="CFcalc4">
						          								<option value="1" {if $A00A90 eq '1'} selected{/if}></option>
							          							<option value="2" {if $A00A90 eq '2'} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $A00A90 eq '3'} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>	                                               				                                               				
							          				</div>
							          				
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Prix net unit� achat")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" id="PNUA" disabled>
													</div>

													
								          		</div>
								          		<br>								          		
								          		<div class="form-inline">

													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Eco participation � la pi�ce(DEEE)")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" name="A00A50" value="{$A00A50}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Taxe g�n�rale sur les activit� polluantes(TGAP) exprim�e dans l'unit� de commande")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" name="A00A55" value="{$A00A55}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
													
							                        <div class="form-group form-group-sm">							                        	
															<label>{_("TVA : ")}</label>																		              										          					
						          							<select class="form-control" name="TVAPRD" id="TVAPRD">
							          							<option value="0" {if $TVAPRD eq 0} selected{/if}>{_("0.0")}</option>							          														          							
							          							<option value="5.5" {if $TVAPRD eq 5.5} selected{/if}>{_("5.5")}</option>                                          					
							          							<option value="20" {if $TVAPRD eq 20} selected{/if}>{_("20.0")}</option>		          							              							                                     
	                                               			</select>	                                               				                                               				
							          				</div>
								          		</div>
								          		<br>
								          		<div class="form-inline">
													<div class="input-group">
													  <span class="input-group-addon">{_("Quantit� multiple achat  ")}</span>
													  <input type="text" class="form-control" name="A00F80" id="A00F80" value="{$A00F80}" onkeyup="validNumber(this)" onblur="validNumber(this)" >
													  <span class="input-group-addon"><span class="label label-info" id='sel1'>{$LBL2}</span></span>
													</div>													
								          		</div>								          		
								          		<div class="form-inline">
													<div class="input-group">
													  <span class="input-group-addon">{_("Seuil minimum de commande")}</span>
													  <input type="text" class="form-control" name="A00B10" id="A00B10" value="{$A00B10}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													  <span class="input-group-addon"><span class="label label-info" id='sel2'>{$LBL2}</span></span>
													</div>	
													{* <button type="button" class="btn btn-primary" id="btnValid">{_("V�rifier")}</button> *}
								          		</div>
								          		<div class="form-inline">
													<div class="input-group">
													  <span class="input-group-addon">{_("PPI : Prix Public Indicatif  ")}</span>
													  <input type="text" class="form-control" name="A00B15" id="A00B15" value="{$A00B15}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													  <span class="input-group-addon"><span class="label label-info" id='sefac1'>{$LBL1}</span></span>
													</div>	
								          		</div>
       			                                <br>
												<div class="input-group"> 
													<input type="hidden" name="A00B25" value="{$A00B25}">
													<span class="input-group-addon" id="sizing-addon3">{_("Commentaire sur prix")}</span> <textarea class="form-control" cols ="50" name="COMENT" maxlength="512"  onkeyup="valid(this)" onblur="valid(this)">{$COMENT}</textarea>
												</div>
                                            </div>

                                        </div>                                        
                                    </div>
{*ECOTEL*}                                    
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#collapse2Two" data-toggle="collapse" class="accordion-toggle collapsed" id="accordE">{_("Condition Direct Filiales France")}</a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapse2Two">
                                            <div class="panel-body">
	                                            <div class="form-inline">                
								                    <div class="supervisor-date-form input-daterange">
								                       <div class="form-group form-group-sm">						              						
						              						<label for="selectsupervisestab">{_("Livraison directe?")}</label>							          						 
						          							<select class="form-control" id="selectlivecotel" name="EJRVNN6">
							          							<option value="1" {if $EJRVNN6 eq 1} selected{/if}>{_("Oui")}</option>
							          							<option value="2" {if $EJRVNN6 eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                                               			</select>
								          				</div>
	                                            		
													  	<div class="input-group input-group-sm">
														  <span class="input-group-addon" id="sizing-addon3">{_(" Prix d'achat dans l'unit� facturation : ")}</span>
														  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" id="EJRAZP4" name="EJRAZP4" value="{$EJRAZP4}" onkeyup="validFloat(this);Ecalcul();" onblur="validFloat(this)" >
														</div>
	                                            		
								                    </div>
								          		</div>
								          		<br>								          		
								          		<div class="form-inline">												
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Franco")}</span>
													  <input type="text" class="form-control" id="Efranco" aria-describedby="sizing-addon3" size = "4" name="EJRA1P4" value="{$EJRA1P4}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 1")}</span>
													  <input type="text" class="form-control" id="Erem1" aria-describedby="sizing-addon3" size = "2" name="EJRRUPC" id="Ecalc-jjnb" value="{$EJRRUPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>													
							                        <div class="form-group form-group-sm">							                        					              										          						
						          							<select class="form-control" name="EJRVON6" id="Ecalc1">
						          								<option value="1" {if $EJRVON6 eq 1} selected{/if}></option>
							          							<option value="2" {if $EJRVON6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $EJRVON6 eq 3} selected{/if}>{_("X")}</option>                                         							          							              							                                     
	                                               			</select>	                                               				                                               					
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 2")}</span>
													  <input type="text" class="form-control" id="Erem2" aria-describedby="sizing-addon3" size = "2" name="EJRRVPC" id="Ecalc-jknb" value ="{$EJRRVPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>	
							                        <div class="form-group form-group-sm">							                        		              										          						
						          							<select class="form-control" name="EJRVPN6" id="Ecalc2">
						          								<option value="1" {if $EJRVPN6 eq 1} selected{/if}></option>
							          							<option value="2" {if $EJRVPN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $EJRVPN6 eq 3} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 3")}</span>
													  <input type="text" class="form-control" id="Erem3" aria-describedby="sizing-addon3" size = "2" name="EJRRWPC" id="Ecalc-jlnb" value ="{$EJRRWPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							                        <div class="form-group form-group-sm">							                        						              										          						
						          							<select class="form-control" name="EJRVQN6" id="Ecalc3">
						          								<option value="1" {if $EJRVQN6 eq 1} selected{/if}></option>
							          							<option value="2" {if $EJRVQN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $EJRVQN6 eq 3} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 4")}</span>
													  <input type="text" class="form-control" id="Erem4" aria-describedby="sizing-addon3" size = "2" name="EJRRXPC" id="Ecalc-jmnb" value ="{$EJRRXPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							                        <div class="form-group form-group-sm">							                        				              										          						
						          							<select class="form-control" name="EJRVRN6" id="Ecalc4">
						          								<option value="1" {if $EJRVRN6 eq 1} selected{/if}></option>
							          							<option value="2" {if $EJRVRN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $EJRVRN6 eq 3} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>	                                               				                                               				
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Prix net unit� commande")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="EPNUC" disabled>
													</div>							          				
								          		</div>
								          		<br>
								          		<div class="form-inline">																										
													<div class="input-group">
													  <span class="input-group-addon">{_("Seuil minimum de commande")}</span>
													  <input type="text" class="form-control" name="EJRQ7Q1" id="EJRQ7Q1" value ="{$EJRQ7Q1}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													  <span class="input-group-addon"><span class="label label-info" id='sel3'>{$LBL2}</span></span>
													</div>							
								          		</div>
	   			                                <br>
												<div class="input-group"> 
													<span class="input-group-addon" id="sizing-addon2">{_("Commentaire sur prix")}</span> <textarea class="form-control" cols ="50" maxlength="512" id="EJRAXN7" name="EJRAXN7" onkeyup="valid(this)" onblur="valid(this)">{$EJRAXN7}</textarea>
												</div>
                                            </div>
                                        </div>
                                    </div>
{*LACORPO*}                                    
                                 {*   <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#collapse2Three"  data-toggle="collapse" class="accordion-toggle collapsed" id="accordL">{_("La Corpo")}</a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapse2Three">
                                            <div class="panel-body">                                            
	                                            <div class="form-inline">                
								                    <div class="supervisor-date-form input-daterange">
								                       <div class="form-group form-group-sm">
						              						<label for="selectsupervisestab">{_("Livraison directe La Corpo?")}</label>							          						 
						          							<select class="form-control" id="selectlivcorpo" name="LJRVNN6" >
							          							<option value="1" {if $LJRVNN6 eq 1} selected{/if}>{_("Oui")}</option>
							          							<option value="2" {if $LJRVNN6 eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                                               			</select>
								          				</div>
													  	<div class="input-group input-group-sm">
														  <span class="input-group-addon" id="sizing-addon3">{_(" Prix d'achat dans l'unit� facturation : ")}</span>
														  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "5" id="LJRAZP4" name="LJRAZP4" value="{$LJRAZP4}" onkeyup="validFloat(this);Lcalcul();" onblur="validFloat(this)" >
														</div>
								                    </div>
								          		</div>
								          		<br>								          		
								          		<div class="form-inline">
												
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Franco")}</span>
													  <input type="text" class="form-control" id="franco" aria-describedby="sizing-addon3" size = "4" name="LJRA1P4" value="{$LJRA1P4}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>		
													
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 1")}</span>
													  <input type="text" class="form-control" id="rem1" aria-describedby="sizing-addon3" size = "2" id="calc-jjnb" name="LJRRUPC" value="{$LJRRUPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>		
													
							                        <div class="form-group form-group-sm">							                        				              										          						
						          							<select class="form-control" name="LJRVON6" id="calc1">
						          								<option value="1" {if $LJRVON6 eq 1} selected{/if}>{_("")}</option>
							          							<option value="2" {if $LJRVON6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $LJRVON6 eq 3} selected{/if}>{_("X")}</option>                                           							          							              							                                     
	                                               			</select>	                                               				                                               					
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 2")}</span>
													  <input type="text" class="form-control" id="rem2" aria-describedby="sizing-addon3" size = "2" id="calc-jknb" name="LJRRVPC" value ="{$LJRRVPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>	
							                        <div class="form-group form-group-sm">							                        	 										          						
						          							<select class="form-control" name="LJRVPN6" id="calc2">
						          								<option value="1" {if $LJRVPN6 eq 1} selected{/if}>{_("")}</option>
							          							<option value="2" {if $LJRVPN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $LJRVPN6 eq 3} selected{/if}>{_("X")}</option>                                         							          							              							                                     
	                                               			</select>
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 3")}</span>
													  <input type="text" class="form-control" id="rem3" aria-describedby="sizing-addon3" size = "2" id="calc-jlnb" name="LJRRWPC" value ="{$LJRRWPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							                        <div class="form-group form-group-sm">							                        					              										          						
						          							<select class="form-control" name="LJRVQN6" id="calc3">
						          								<option value="1" {if $LJRVQN6 eq 1} selected{/if}>{_("")}</option>
							          							<option value="2" {if $LJRVQN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $LJRVQN6 eq 3} selected{/if}>{_("X")}</option>                                          							          							              							                                     
	                                               			</select>
							          				</div>
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Remise 4")}</span>
													  <input type="text" class="form-control" id="rem4" aria-describedby="sizing-addon3" size = "2" id="calc-jmnb" name="LJRRXPC" value ="{$LJRRXPC}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													</div>
							                        <div class="form-group form-group-sm">							                        			              										          						
						          							<select class="form-control" name="LJRVRN6" id="calc4">
						          								<option value="1" {if $LJRVRN6 eq 1} selected{/if}>{_("")}</option>
							          							<option value="2" {if $LJRVRN6 eq 2} selected{/if}>{_("%")}</option>
							          							<option value="3" {if $LJRVRN6 eq 3} selected{/if}>{_("X")}</option>                                         							          							              							                                     
	                                               			</select>	                                               				                                               				
							          				</div>	
												  	<div class="input-group input-group-sm">
													  <span class="input-group-addon" id="sizing-addon3">{_("Prix net unit� commande")}</span>
													  <input type="text" class="form-control" aria-describedby="sizing-addon3" size = "2" id="LPNUC" disabled>
													</div>												
								          		</div>
								          		<br>
								          		<div class="form-inline">												
													<div class="input-group">
													  <span class="input-group-addon">{_("Seuil minimum de commande")}</span>
													  <input type="text" class="form-control" name="LJRQ7Q1" id="LJRQ7Q1" value ="{$LJRQ7Q1}" onkeyup="validFloat(this)" onblur="validFloat(this)" >
													  <span class="input-group-addon"><span class="label label-info" id='sel4'>{$LBL2}</span></span>
													</div>																																
								          		</div>
	   			                                <br>
												<div class="input-group"> 
													<span class="input-group-addon" id="sizing-addon2">{_("Commentaire sur prix")}</span> <textarea class="form-control" cols ="50" maxlength="512" id="CJRAXN7" name="CJRAXN7" onkeyup="valid(this)" onblur="valid(this)">{$CJRAXN7}</textarea>
												</div>                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div> *}                             
                        </div> 
			</div>
			</form>
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='condition_js.tpl'}

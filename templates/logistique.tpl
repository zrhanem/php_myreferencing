


 {include file='header_ref.tpl'} 
        <div class="container">
        


    
        
          <div class="row">
  <!-- Modal -->
  <div class="modal" id="myModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="closemodal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
          <label>{_("Chap�tre :")}</label>													              										          					
			<select id="SELCHPT">
			{foreach $Lstchp as $val}
				<option value="{$val['Q4N7C1']}">{$val['LIB']}</option>
			{/foreach}	                                   							          							              							                                    
   			</select>   
        </div>
        <div class="modal-body" >


      <div id="divLoading" style="display: none;">
        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
      </div>
      
	  <table class="table table-hover">
	    <thead>
	      <tr>
	      	<th>{_("Code")}</th>
	        <th>{_("Libell�")}</th>        
	      </tr>
	    </thead>
	    <tbody  id="tblnmt">
	    {foreach $Lstnmclt as $val}
	      <tr class='clickable-row'  id="{$val['Q5HWT2']}">
	        <td>{$val['Q5HWT2']}</td>        
	        <td>{$val['Q5HXT2']}+{$val['Q5HYT2']}+{$val['Q5HZT2']}+{$val['Q5H0T2']}</td>
	      </tr>
	    {/foreach}
	    </tbody>
	  </table>
   			
   			          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="btnclosemodal">Close</button>
        </div>
      </div>
    </div>
  </div>   
  <!-- Fin Modal -->
              
			<ol class="breadcrumb">                            
                {*<li>{_("Dossier : ")}</li>*}
                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
          	</ol> 
			<div class="col-md-26">			
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <div class="pull-left">{_("Logistique")}</div>
                            {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div>{/if}
                            <div class="clearfix"></div>
                        </h3>
                    </div>
                    
                    
                    <div class="panel-body">
						
   	                	  <div class="form-inline">
							<div class="form-error alert alert-danger hidden" id="MissingErr">																															
							</div>						
						  </div>	

	                	<div class="well">
			          		<div class="form-inline">							                        									
								<h4><span class="label label-primary">{_("Pays et Douane")}</span></h4>													              										          					
		                	</div>
		                	<br>
			          		<div class="form-inline">
		                        <div class="form-group">							                        	
									<label>{_("Nomenclature douani�re")}</label>	
									<input type="text" size = "8" maxlength="8" value="{$A00C90}" id="A00C90" onkeyup="validNumber(this)" onblur="validNumber(this)">		
									 <button type="button" class="bbtn btn-primary" id="nomDisplay" >...</button>																			              										          															                                               				                                               				
		          				</div>
		                        <div class="form-group">							                        	
									<label>{_("Pays de fabrication")}</label>													              										          					
	      							<select id="A00C92">
	      							{foreach $Lstpays as $val}
	          							<option value="{$val['P00001']}" {if $val['P00001'] eq $A00C92} selected{/if}>{$val['P00002']}</option>
	          						{/foreach}	                                        							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
		                        <div class="form-group">							                        	
									<label>{_("Si code non trouv�")}</label>													              										          					
                                    <input type="text" size = "10" maxlength="20"  value="{$NOMNTR}" id="NOMNTR" onkeyup="validNumber(this)" onblur="validNumber(this)">           				                                               					
		          				</div>
		                	</div>
	                	</div>
	                	<div class="well">
			          		<div class="form-inline">							                        									
								<h4><span class="label label-primary">{_("Conditionnement")}</span></h4>																					              										          				
		                	</div>
		                	<br>		                		
			          		<div class="form-inline">	
			          			<div class="form-group">						                        	
									<label>{_("Primaire (au contact du produit) Nombre d'unit�")}</label>		
									<input type="text" size = "10" value="{$A00C25}" id="A00C25" onkeyup="validNumber(this)" onblur="validNumber(this)">	
								</div>   	
			          			<div class="form-group">						                        	
									<label>{_("Code EAN du produit")}</label>		
									<input type="text" size = "14" maxlength="20" value="{$A00C95}" id="A00C95" onkeyup="validNumber(this)" onblur="validNumber(this)">
								</div>
			          			<div class="form-group">						                        										
									<span class="label label-danger">{_("Les codes EAN sont constitu�s de 13 chiffres")}</span>								
								</div>   	   										              										          					
		                	</div>
		                	<br>
			          		<div class="form-inline">	
			          			<div class="form-group">						                        	
									<label>{_("Sur Emballage (si existant) Nombre d'unit�")}</label>		
									<input type="text" size = "10" value="{$A00C50}" id="A00C50" onkeyup="validNumber(this)" onblur="validNumber(this)">
								</div>   	
			          			<div class="form-group">						                        	
									<label>{_("Code EAN du conditionnement")}</label>		
									<input type="text" size = "14" maxlength="20"  value="{$A00D10}" id="A00D10" onkeyup="validNumber(this)" onblur="validNumber(this)">
								</div>
			          			<div class="form-group">						                        										
									<span class="label label-danger">{_("Les codes EAN sont constitu�s de 13 chiffres")}</span>								
								</div>     										              										          					
		                	</div>
		                	<br>
			          		{* <div class="form-inline">	
			          			<div class="form-group">						                        	
									<label>{_("Volume total de l'article en litre")}</label>		
									<input type="text" size = "10" value="{$VOLLIT}" id="VOLLIT" onkeyup="validFloat(this)" onblur="validFloat(this)">
								</div>   											              										          					
		                	</div> *}
	                	</div>
	                	{* <div class="well">
			          		<div class="form-inline">		
			          			<h4><span class="label label-primary">{_("Contraintes Logistiques")}</span></h4					                        																	              										          				
		                	</div>
		                	<br>
		                	<div class="form-inline">
	                        	<div class="form-group">							                        	
									<label>{_("Palettisation")}</label>													              										          					
	      							<select id="A00C75">
	          							<option value="1" {if $A00C75 eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $A00C75 eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
		                        <div class="form-group">							                        	
									<label>{_("Respect de la palettisation ECF (max 500Kg et H 180cm)")}</label>													              										          					
	      							<select id="RESPAL">
	          							<option value="1" {if $RESPAL eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $RESPAL eq 2} selected{/if}>{_("Non")}</option>                                         							          							              							                                    
	                       			</select>	                                               				                                               					
		          				</div>
		          			</div>
		          			<br>
		          			<div class="form-inline">			          				
		                        <div class="form-group">							                        	
									<label>{_("Livraison sur palettes s�par�es stock et Contre-Marque (Hors stock)")}</label>													              										          					
	      							<select id="PALSEP">
	          							<option value="1" {if $PALSEP eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $PALSEP eq 2} selected{/if}>{_("Non")}</option>                                         							          							              							                                    
	                       			</select>	                                               				                                               					
		          				</div>
		          			</div>	
		          			<br>
		          			<div class="form-inline">	
		                        <div class="form-group">							                        	
									<label>{_("Marquage colis conforme (code ECF, ref fourn, d�signation, qt� dans l'emballage), au moins FR et GB")}</label>													              										          					
	      							<select id="MRQCNF">
	          							<option value="1" {if $MRQCNF eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $MRQCNF eq 2} selected{/if}>{_("Non")}</option>                                         							          							              							                                    
	                       			</select>	                                               				                                               					
		          				</div>
		          			</div>	
	                	</div> *}                                              
                    </div>                      	                          
                </div>
			</div>
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='logistique_js.tpl'}




 {include file='header_ref.tpl'} 
        <div class="container">
          <div class="row">

				<ol class="breadcrumb">                            
	                {*<li>{_("Dossier : ")}</li>*}
	                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
	          	</ol>         		          		          	

				<div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title">
	                        <div class="pull-left">{_("Eco-Participation")}</div>
	                        {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div>{/if}
	                        <div class="clearfix"></div>
	                    </h3>
	                </div>
	                
	                <div class="panel-body">
		          		<div class="form-inline">
	                        <div class="form-group">							                        	
								<label>{_("Fonction du d�cret ")}</label>													              										          					
      							<select id="seldecret">
          							{foreach $LstFct as $val}
          								<option value="{$val['Code']}" {if $val['Code'] eq $LEWXNA} selected{/if}>{$val['Libelle']}</option>								          						
	          						{/foreach}	                                               							          							              							                                   
                       			</select>	                                               				                                               					
	          				</div>	          				
	                	</div>
	                	<br>
		          		<div class="form-inline">
	                        <div class="form-group">							                        	
								<label>{_("Famille ")}</label>													              										          					
      							<select id="selfam">
          							<option value="1" id="F1" {if $LEWYNA eq 1} selected{/if}>{_("Pas de taxe Eco-participation")}</option>
          							<option value="2" id="F2" {if $LEWYNA eq 2} selected{/if}>{_("Assise")}</option>
          							<option value="3" id="F3" {if $LEWYNA eq 3} selected{/if}>{_("Rangement")}</option>
          							<option value="4" id="F4" {if $LEWYNA eq 4} selected{/if}>{_("Plan de poste et de travail")}</option>          							
          							<option value="5" id="F5" {if $LEWYNA eq 5} selected{/if}>{_("Autres")}</option>                                                 							          							              							                                     
                       			</select>	                                               				                                               					
	          				</div>	          				
	                	</div>
	                	<br>
		          		<div class="form-inline">
	                        <div class="form-group">							                        	
								<label>{_("Mat�riau dominant ")}</label>													              										          					
      							<select id="selmat">
          							<option value="1" id="MD1" {if $LEWZNA eq '1'} selected{/if}>{_("Pas de taxe Eco-participation")}</option>
          							<option value="2" id="MD2" {if $LEWZNA eq '2'} selected{/if}>{_("Sup�rieur � 50% m�taux")}</option>
          							<option value="3" id="MD3" {if $LEWZNA eq '3'} selected{/if}>{_("Sup�rieur � 50% bois")}</option>
          							<option value="4" id="MD4" {if $LEWZNA eq '4'} selected{/if}>{_("Sup�rieur � 50% plastiques")}</option>          							
          							<option value="5" id="MD5" {if $LEWZNA eq '5'} selected{/if}>{_("Autres")}</option>                                                 							          							              							                                     
                       			</select>	                                               				                                               					
	          				</div>	          				
	                	</div>
	                	<br>	                	
		          		<div class="form-inline">
		          			<div class="form-group">  										          				  	          	
								  <label>{_("Poids net (Kg)")}</label>														  
								  <input type="text" size = "10" value="{$LEYCN6}" id="poids" onkeyup="validNumber(this)" onblur="validNumber(this)" >	
								  <label>{_("Le poids Net de l'article � l'unit� et sans emballage")}</label>														  														
							</div>
						</div>	
	                	<br>
	                	<div class="well">
			          		<div class="form-inline">
			          			<div class="form-group">  										          				  	          	
								  <p>{_("L'")} <span class="label label-success">{_("�co-participation")}</span>{_(", ou")} <span class="label label-success">{_("�co-contribution")}</span>{_("(les deux termes sont orthographi�s avec ou sans tiret), est un � co�t � ajout� au prix de vente des �l�ments d�ameublement vendus en France.")}</p>														  												  														
								  <p>{_("Il doit compenser le co�t de la collecte et du traitement des d�chets  des �l�ments d'ameublement en fin de vie, et est enti�rement revers�e aux organismes r�alisant ces t�ches")}</p>
								  <p>{_("Afin de r�pondre � la r�glementation, l'organisme VALDELIA assure, pour le compte de ses adh�rents (dont le groupe E.CF), la prise en charge de la collecte et du traitement des d�chets de mobilier professionnel sur le territoire fran�ais :")}</p>
								  	<ul>
									  <li>{_("mobilier de bureau")}</li>
									  <li>{_("mobilier technique")}</li>
									  <li>{_("mobilier professionnel")}</li>
									  <li>{_("mobilier pour Collectivit�s (scolaire, sant�, loisirs, culture, commercial)...")}</li>
									</ul>
								</div>
							</div>							
						</div>								
	                </div>
	              
	            </div>
                        
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='ecopart_js.tpl'}

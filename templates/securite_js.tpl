    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
     <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>
        
        <!-- Ces fichiers sont pour le upload file -->
        <script src="assets/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
        {* <script src="assets/js/fileinput.min.js"></script>
        <script src="assets/js/fileinput.js"></script> *}
        
        {include file='googleanalytics.tpl'}
        
        {literal}
      		<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $(".clickable-row").click(function(){
			      var id = this.id;
			      $('#A00C90').val(id);		      
			      $('#myModal').hide();						              
			        });                                                                  
			  });
			</script>  
	        <script type="text/JavaScript">
				function valid(f) {
				!(/^[A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]*$/i).test(f.value)?f.value = f.value.replace(/[^A-z�����&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]/ig,''):null;
				} 
			</script>     
			<script type="text/JavaScript">
				function validNumber(f) {		
				!(/^[0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9]/ig,''):null;
				} 
			</script>
			<script type="text/JavaScript">
				function validFloat(f) {		
				!(/^[0-9.]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9.]/ig,''):null;
				} 
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $('#btnValid').click(function(){	
					$('form#my_form').submit();		       					               
			        });                                                                  
			  });
			</script>		
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $("#nomDisplay").click(function(){		      
			      $('#myModal').show();						              
			        });                                                                  
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $("#closemodal").click(function(){		      
			      $('#myModal').hide();						              
			        });                                                                  
			  });
			</script>
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $("#btnclosemodal").click(function(){		      
			      $('#myModal').hide();						              
			        });                                                                  
			  });
			</script>
		{/literal}		
        {literal}      
    		<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $("#SELCHPT").change(function(){				      	 
			      	$('#tblnmt').empty();		
			       	$('#divLoading').show();   		      		      	     
						$.ajax({
						 cache: false,
	                     type: 'POST',
	                     url: 'ajax/ajax_nomenclature.php',
	                     data: "schpt="+$('#SELCHPT').val(),
	                     dataType: "html",
	                     success: function(datas_result){                             	     
	                     	$('#divLoading').hide();
	                     	$('#tblnmt').append(datas_result);            	                     	                     
						 },					 
						 complete: function(data) {     
						    $(function() {
									$(".clickable-row").click(function(){
											      var id = this.id;
											      $('#A00C90').val(id);		      
											      $('#myModal').hide();						              
											        });          
						    });
						  }                  
	                  });   
			      	
			      	
			      		      						              
			        });                                                                  
			  });
			</script>
			<script type="text/javascript">			       		  
		      $('#selCE').change(function(){							       
				if($('#selCE option:selected').val() == 2){
					$('#lblCE').addClass('hidden');
					$('#fileCE').addClass('hidden');
					$('#divficheactCE').addClass('hidden');						
				}else{
					$('#lblCE').removeClass('hidden');
					$('#fileCE').removeClass('hidden');
					$('#divficheactCE').removeClass('hidden');
				}          
		        });                                                                  			  
			</script>		
			<script type="text/javascript">			       		  
		      $('#selinf').change(function(){							       
				if($('#selinf option:selected').val() == 2){
					$('#lblinf').addClass('hidden');
					$('#A00D55').val('0');				
				}else{
					$('#lblinf').removeClass('hidden');
				}          
		        });                                                                  			  
			</script>	

			<script type="text/javascript">			       		  
		      $('#selper').change(function(){							       
					if($('#selper option:selected').val() == 2){
						$('#A00D30').val('0');
						$('#lblper').addClass('hidden');				
					}else{
						$('#lblper').removeClass('hidden');
					}          
		        });                                                                  			  
			</script>	
		{/literal}		
		{literal}                
	        <script type="text/javascript">
				$(function () {
				    
				    // A chaque s�lection de fichier CE
				    $('#my_form').find('input[name="FicheCE"]').on('change', function (e) {
				        var files = $(this)[0].files;
				 
				        if (files.length > 0) {
	   				        if(files[0].size > 10485760){
								alert("Le fichier s�lectionn� d�passe le maximum de taille autoris� qui est de 10Mo");
								$('#my_form').find('input[name="FicheCE"]').val('');				        
					        }else{
					        	// On part du principe qu'il n'y qu'un seul fichier
					            // �tant donn� que l'on a pas renseign� l'attribut "multiple"
					            var file = files[0],
					                $image_preview = $('#image_preview');
					                				            				                
				                $('#btnannulerCE').removeClass('hidden');
					        	}
				        }
				    });

			       
				    // Bouton "Annuler" CE pour vider le champ d'upload
				    $('#btnannulerCE').click(function(){	
				    	e.preventDefault();
				    	
						$('#my_form').find('input[name="FicheCE"]').val('');
						$('#btnannulerCE').addClass('hidden');	       					               
			        });  				  
				 	
				});
			</script>
				
			<script type="text/javascript">	
				$(function () {
				    $('form#my_form').on('submit', function (e) {
				    
				        // On emp�che le navigateur de soumettre le formulaire
				        e.preventDefault();
				    
	   					var msg = "";
						var err = 0;


						if($('#EANUNITE').val() != '' && $('#EANUNITE').val() > 0 && $('#EANUNITE').val().length != 13 ){									
							msg += "Le code EAN de l'unit� doit contenir 13 chiffres"+"<br>";
							err = 1;
						}
						
						if($('#A00C25').val() <= 0 || $('#A00C25').val() == ''){									
							msg += "Vous devez saisir le conditionnement INNER (au contact du produit)"+"<br>";
							err = 1;
						}
						
						if($('#A00C25').val() != '' && $('#A00C25').val() > 0 && $('#A00C95').val().length != 13 ){									
							msg += "Le code EAN du produit doit contenir 13 chiffres"+"<br>";
							err = 1;
						}
						
						if($('#A00C50').val() != '' && $('#A00C50').val() > 0 && $('#A00D10').val().length != 13 ){									
							msg += "Le code EAN du conditionnement doit contenir 13 chiffres"+"<br>";
							err = 1;
						}
						
						if($('#A00C90').val().length != 8 && $('#NOMNTR').val().length != 8 ){									
							msg += "Vous devez saisir la nomenclature douani�re valide, Celle-ci est compos�e de 8 chiffres"+"<br>";
							err = 1;
						}
						
						if($('#A00C92').val() < 1 ){									
							msg += "Vous devez indiquer le pays du produit."+"<br>";
							err = 1;
						}						
						
						if($('#selper').val() == 1 && $('#A00D30').val() <= 0 ){									
							msg += "Vous devez indiquer la dur�e de vie du produit p�rissable."+"<br>";
							err = 1;
						} 
						if($('#selCE').val() == 1 && ($('#ficheactCE').val() == '' && $('#FicheCE').val() == '') ){									
							msg += "Vous devez fournir le certificat de conformit� contact alimentaire"+"<br>";
							err = 1;
						}   				
				    
				    	if($.trim($('#FicheCE').val()) != ''){
							var FicheCE = $('#FicheCE').val().split(".");
							if(FicheCE[1].toLowerCase() != "pdf"){
								msg += "Le fichier attach� n'est pas un fichier PDF valide"+"<br>";
								err = 1;	
							}
						}
				    
     					if(err == 1){
     						$('#STS').val("N");
							$('#MissingErr').removeClass('hidden');	
							$('#MissingErr').html("");
							$('#MissingErr').html("<strong>"+msg+"</strong>");
						
		                	var r = confirm("Vous avez des champs obligatoires non remplis, enregistrer?");
        					if (r == true) {
						        
						        $('#divLoading').show();						        
						        var formData = new FormData($('form#my_form')[0]);	
						 
						        $.ajax({
						            url: 'ajax/ajax_fileupload_securite.php',
						            type: 'POST',
						            contentType: false, // obligatoire pour de l'upload
						            processData: false, // obligatoire pour de l'upload
						            data: formData,
								    async: false,
								    cache: false,
						            success: function (response) {				            							                
						                // La r�ponse du serveur
						                $('#divLoading').hide();
						                if(response == 0){
						                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'success',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });
											$('#my_form').find('input[name="FicheCE"]').val('');
											
											location.reload();			 															            		                					               
						                }else{
						                	if(response == 999){
						                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
						                	}else{
						                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						                	}	
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'error',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });
									        location.reload();	
						                }
						                
						            }
						        });
        					}else{
			                	var CodeRetour = "Enregistrement annul�";
						        var n = noty({
						            text        : CodeRetour,
						            type        : 'warning',
						            dismissQueue: true,
						            timeout     : 5000,
						            closeWith   : ['click'],
						            layout      : 'top',
						            theme       : 'defaultTheme',
						            maxVisible  : 5
						        });	
        					}
        				}else{
        						$('#STS').val("O");
						        $('#divLoading').show();
        						$('#MissingErr').addClass('hidden');
								$('#MissingErr').empty();
						        
						        var formData = new FormData($('form#my_form')[0]);	
						 
						        $.ajax({
						            url: 'ajax/ajax_fileupload_securite.php',
						            type: 'POST',
						            contentType: false, // obligatoire pour de l'upload
						            processData: false, // obligatoire pour de l'upload
						            data: formData,
								    async: false,
								    cache: false,
						            success: function (response) {				            	
						                // La r�ponse du serveur						                				               
						                $('#divLoading').hide();
						                if(response == 0){
						                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'success',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });
											$('#my_form').find('input[name="FicheCE"]').val('');
											$('#my_form').find('input[name="FicheINERIS"]').val('');
											$('#my_form').find('input[name="Fichesec"]').val('');
											$('#my_form').find('input[name="Fichetech"]').val(''); 	 	  	 	
											location.reload();	 	 			            		                					                
						                }else{
						                	if(response == 999){
						                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
						                	}else{
						                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						                	}	
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'error',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });
									        location.reload();	
						                }
						                
						            }
						        });        				
        				}	
				 
				        

				    });
				});
			</script>	
			
		{/literal}
        
  </body>
</html>






<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-3">
                <ul class="list-inline">
                    {* <li><a href="footer/faq_{$smarty.session.LANG}.php">{_("FAQ")}</a></li> *}
                    {* <li><a href="pdf/CGV_CHOMETTEVF_Mars_2015_2.pdf">{_("CGV")}</a></li> *}
                    {* <li><a href="footer/credit_{$smarty.session.LANG}.php">{_("Cr&eacute;dits")}</a></li> *}
                    <li><a href="footer/legal_{$smarty.session.LANG}.php">{_("Mentions L&eacute;gales")}</a></li>
                    {* <li><a href="footer/perso_{$smarty.session.LANG}.php">{_("Donn&eacute;es personnelles")}</a></li> *}
                    <li><a href="http://www.ecf.fr/files/pdf/dev_durable.pdf" target="blank">{_("Rapport d&eacute;veloppement durable")}</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

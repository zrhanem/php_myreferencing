



 {include file='header_ref.tpl'} 
        <div class="container">
          <div class="row">

				<ol class="breadcrumb">                            
	                {* <li>{_("Dossier : ")}</li> *}
	                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
	          	</ol>         		          		          	
				          		          	
				<div class="panel panel-default">				
	                <div class="panel-heading">
	                    <h3 class="panel-title">
	                        <div class="pull-left">{_("Descriptif")}</div>
	                        {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div>{/if} 
	                        <div class="clearfix"></div>
	                    </h3>
	                </div>	                
	                <form id="my_form"  method="post" enctype="multipart/form-data">
	                <div class="panel-body">		
                      <div id="divLoading" style="display: none;">
                      	<p align="center">{_("Saving Datas...Please wait...")}</p>
				        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
				      </div>				
                	  <div class="form-inline">
						<div class="form-error alert alert-danger hidden" id="MissingErr">																															
						</div>	
						<input type="hidden" name="STS" id="STS" value="N">												
					  </div>	
						<br>
	                    <div id="accordion2" class="panel-group">
	                        <div class="panel panel-primary">
	                            <div class="panel-heading">
	                                <h4 class="panel-title">
	                                    <a href="#collapse2One" data-toggle="collapse" class="accordion-toggle">{_("R�f�rence et D�signation")}</a>
	                                </h4>
	                            </div>
	                            <div class="panel-collapse in" id="collapse2One">
	                            	<br>
									<div class="input-group"> 
										<span class="input-group-addon">{_("R�f�rence (Facture/Tarif)(1)")}</span><input type="text" id="DesREF" name="DesREF" class="form-control" maxlength="20" value="{$DESREF}" onkeyup="valid(this)" onblur="valid(this)"  aria-describedby="sizing-addon2" >  
									</div>							
									<br>
									<div class="input-group"> 
										<span class="input-group-addon">{_("D�signation FR(1)")}</span><input type="text" id="DesFR1" name="DesFR1" class="form-control" maxlength="35" value="{$DESFR1}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2" >  
									</div>									
									<br>
									<div class="input-group"> 
										<span class="input-group-addon" >{_("D�signation FR(2)")}</span> <input type="text" id="DesFR2" name="DesFR2" class="form-control" maxlength="35" value="{$DESFR2}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2"> 
									</div>
									<br>
									<div class="input-group"> 
										<span class="input-group-addon" >{_("D�signation GB(1)")}</span> <input type="text" id="DesGB1" name="DesGB1" class="form-control" maxlength="35" value="{$DESGB1}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2" > 
									</div>
									<br>
									<div class="input-group"> 
										<span class="input-group-addon">{_("D�signation GB(2)")}</span> <input type="text" id="DesGB2" name="DesGB2" class="form-control" maxlength="35" value="{$DESGB2}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2" > 
									</div>
									<br>
									<div class="input-group"> 
										<span class="input-group-addon" >{_("D�signation NL(1)")}</span> <input type="text" name="DesNL1"  class="form-control" maxlength="35" value="{$DESNL1}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2" > 
									</div>
									<br>
									<div class="input-group"> 
										<span class="input-group-addon">{_("D�signation NL(2)")}</span> <input type="text" id="DesNL2" name="DesNL2" class="form-control" maxlength="35" value="{$DESNL2}"  onkeyup="valid(this)" onblur="valid(this)" aria-describedby="sizing-addon2"  >
									</div>
									<br>									
	                            </div>
	                        </div>
	                        <div class="panel panel-primary">
	                            <div class="panel-heading">
	                                <h4 class="panel-title">
	                                    <a href="#collapse3" data-toggle="collapse" class="accordion-toggle collapsed" id="accord3">{_("D�lais")}</a>
	                                </h4>
	                            </div>
	                            <div class="panel-collapse collapse" id="collapse3">
		                            <div class="panel-body">  			                            		                            		                            	                            
	                            	<div class="form-inline" >		
										<div class="input-group"> 
											<span class="input-group-addon" id="sizing-addon2">{_("D�lai de livraison en jours")}</span> <input type="text" name="delai" class="form-control" data-min="0"  value="{$DELAI}" onkeyup="validNumber(this)" onblur="validNumber(this)" aria-describedby="sizing-addon2"> 
										</div>											
									</div>
									{* <br>
                                    <div class="form-inline" >	
										<div class="input-group {if $FP eq ""}hidden{/if}" id="divficheact" > 
											<span class="input-group-addon" id="sizing-addon2">{_("Fiche produit actuelle : ")}</span><input type="text" name="ficheact" id="ficheact" class="form-control" value="{$FP}" aria-describedby="sizing-addon2" disabled>														
										</div>
									</div>	*}
									    																			
										<br>
		                            </div>       
		                        </div>
		                    </div>		                    		                   
	                </div>
	            </div> 
	            
	         </form>   
	                                   
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='descriptif_js.tpl'}




 {include file='header_ref.tpl'} 
        <div class="container">
          <div class="row">

				<ol class="breadcrumb">                            
	                {*<li>{_("Dossier : ")}</li>*}
	                <li>{_("R�f�rence: ")}{$DESFR1}</li>            
	          	</ol>         		          		          	

				<div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title">
	                        <div class="pull-left">{_("Marquage et Garantie")}</div>
	                        {if $smarty.session.MODIF eq 'O'}<div class="pull-right"><button type="button" class="btn btn-primary" id="btnValid">{_("Enregistrer")}</button></div>{/if}
	                        <div class="clearfix"></div>
	                    </h3>
	                </div>
	                <form id="my_form"  method="post" action="ajax/ajax_fileupload_marquage.php" enctype="multipart/form-data">
	                <div class="panel-body">
	                      <div id="divLoading" style="display: none;">
	                      	<p align="center">{_("Saving Datas...Please wait...")}</p>
					        <p align="center"><img src="photos/icons/ajax-loader_bis.gif" alt="arobas" style="width:56px; height:56px;"/></p>  
					      </div>
   	                	  <div class="form-inline">
							<div class="form-error alert alert-danger hidden" id="MissingErr">																															
							</div>						
							<input type="hidden" name="STS" id="STS" value="N">
						  </div>	
	                	<div class="well">
			          		<div class="form-inline">							                        									
								<h4><span class="label label-primary">{_("Marquage")}</span></h4>													              										          					
		                	</div>
		                	<br> 
		                	
		                	
		                	
	              	
		                	
		                {*	<table>
						    <tbody>						    
						      <tr>
		                	    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("CE				")}</button><input type="checkbox"  class="hidden" name="X00001" id="X00001" {if !empty($Lstmarq['X00001']) && $Lstmarq['X00001'] eq 1} checked{/if}/></span></td>
		                	    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Induction      ")}</button><input type="checkbox" class="hidden" name="X00010" id="X00010" {if !empty($Lstmarq['X00010']) && $Lstmarq['X00010'] eq 1} checked{/if}></span></td>
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Micro ondes    ")}</button><input type="checkbox"  class="hidden" name="X00005" id="X00005" {if !empty($Lstmarq['X00005']) && $Lstmarq['X00005'] eq 1} checked{/if}/></span></td>    							    
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Garantie Lave vaisselle")}</button><input type="checkbox"  class="hidden" name="X00013" id="X00013" {if !empty($Lstmarq['X00013']) && $Lstmarq['X00013'] eq 1} checked{/if}/></span></td>
						      </tr>
						      <tr>
	    		        	    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("NF             ")}</button><input type="checkbox" class="hidden" name="X00002" id="X00002" {if !empty($Lstmarq['X00002']) && $Lstmarq['X00002'] eq 1} checked{/if}/></span></td>
								<td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Eco label      ")}</button><input type="checkbox" class="hidden" name="X00011" id="X00011" {if !empty($Lstmarq['X00011']) && $Lstmarq['X00011'] eq 1} checked{/if}/></span></td>
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Livr� mont�    ")}</button><input type="checkbox" class="hidden" name="X00015" id="X00015" {if !empty($Lstmarq['X00015']) && $Lstmarq['X00015'] eq 1} checked{/if}/></span></td>
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Contact alimentaire")}</button><input type="checkbox" class="hidden" name="X00004" id="X00004" {if !empty($Lstmarq['X00004']) && $Lstmarq['X00004'] eq 1} checked{/if}/></span></td>																							        
						      </tr>
						      <tr>
							      <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Gaz            ")}</button><input type="checkbox" class="hidden" name="X00006" id="X00006" {if !empty($Lstmarq['X00006']) && $Lstmarq['X00006'] eq 1} checked{/if}/></span></td>		                	   
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Electrique     ")}</button><input type="checkbox" class="hidden" name="X00007" id="X00007" {if !empty($Lstmarq['X00007']) && $Lstmarq['X00007'] eq 1} checked{/if}/></span></td>    							    
			                	<td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Anti-Ebr�chure ")}</button><input type="checkbox" class="hidden" name="X00014" id="X00014" {if !empty($Lstmarq['X00014']) && $Lstmarq['X00014'] eq 1} checked{/if}/></span></td>			                
			                	<td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("A monter soi-m�me  ")}</button><input type="checkbox" class="hidden" name="X00016" id="X00016" {if !empty($Lstmarq['X00016']) && $Lstmarq['X00016'] eq 1} checked{/if}/></span></td>						    							    
						      </tr>
						      <tr>
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("HACCP          ")}</button><input type="checkbox" class="hidden" name="X00003" id="X00003" {if !empty($Lstmarq['X00003']) && $Lstmarq['X00003'] eq 1} checked{/if}/></span></td>		                	    
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Halog�ne           ")}</button><input type="checkbox" class="hidden" name="X00008" id="X00008" {if !empty($Lstmarq['X00008']) && $Lstmarq['X00008'] eq 1} checked{/if}/></span></td>    
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Micro-ondable      ")}</button><input type="checkbox" class="hidden" name="X00012" id="X00012" {if !empty($Lstmarq['X00012']) && $Lstmarq['X00012'] eq 1} checked{/if}/></span></td>
							    <td><span class="button-checkbox"><button type="button" class="btn btn-md" data-color="info">{_("Vitro c�ramique")}</button><input type="checkbox"  class="hidden" name="X00009" id="X00009" {if !empty($Lstmarq['X00009']) && $Lstmarq['X00009'] eq 1} checked{/if}/></span></td>    
						      </tr>						      
						    </tbody>
						  </table> *}
		                	
		                	
		                
		                  <table class = "table table-condensed">
						    <tbody>						    
						      <tr>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00001" id="X00001" {if !empty($Lstmarq['X00001']) && $Lstmarq['X00001'] eq 1} checked{/if}><label for="X00001">{_("CE")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00005" id="X00005" {if !empty($Lstmarq['X00005']) && $Lstmarq['X00005'] eq 1} checked{/if}><label for="X00005">{_("Micro ondes ")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00009" id="X00009" {if !empty($Lstmarq['X00009']) && $Lstmarq['X00009'] eq 1} checked{/if}><label for="X00009">{_("Vitro c�ramique")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00013" id="X00013" {if !empty($Lstmarq['X00013']) && $Lstmarq['X00013'] eq 1} checked{/if}><label for="X00013">{_("Garantie Lave vaisselle")}</label></div></div></td>
						      </tr>
						      <tr>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00002" id="X00002" {if !empty($Lstmarq['X00002']) && $Lstmarq['X00002'] eq 1} checked{/if}><label for="X00002">{_("NF")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00006" id="X00006" {if !empty($Lstmarq['X00006']) && $Lstmarq['X00006'] eq 1} checked{/if}><label for="X00006">{_("Gaz")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00010" id="X00010" {if !empty($Lstmarq['X00010']) && $Lstmarq['X00010'] eq 1} checked{/if}><label for="X00010">{_("Induction")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00014" id="X00014" {if !empty($Lstmarq['X00014']) && $Lstmarq['X00014'] eq 1} checked{/if}><label for="X00014">{_("Anti-Ebr�chure")}</label></div></div></td>
						      </tr>
						      <tr>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00003" id="X00003" {if !empty($Lstmarq['X00003']) && $Lstmarq['X00003'] eq 1} checked{/if}><label for="X00003">{_("HACCP")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00007" id="X00007" {if !empty($Lstmarq['X00007']) && $Lstmarq['X00007'] eq 1} checked{/if}><label for="X00007">{_("Electrique")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00011" id="X00011" {if !empty($Lstmarq['X00011']) && $Lstmarq['X00011'] eq 1} checked{/if}><label for="X00011">{_("Eco label")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00015" id="X00015" {if !empty($Lstmarq['X00015']) && $Lstmarq['X00015'] eq 1} checked{/if}><label for="X00015">{_("Livr� mont�")}</label></div></div></td>
						      </tr>
						      <tr>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00004" id="X00004" {if !empty($Lstmarq['X00004']) && $Lstmarq['X00004'] eq 1} checked{/if}><label for="X00004">{_("Contact alimentaire")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00008" id="X00008" {if !empty($Lstmarq['X00008']) && $Lstmarq['X00008'] eq 1} checked{/if}><label for="X00008">{_("Halog�ne")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00012" id="X00012" {if !empty($Lstmarq['X00012']) && $Lstmarq['X00012'] eq 1} checked{/if}><label for="X00012">{_("Micro-ondable")}</label></div></div></td>
						        <td><div class="funkyradio"><div class="funkyradio-primary"><input type="checkbox" name="X00016" id="X00016" {if !empty($Lstmarq['X00016']) && $Lstmarq['X00016'] eq 1} checked{/if}><label for="X00016">{_("A monter soi-m�me")}</label></div></div></td>						        
						      </tr>						      
						    </tbody>
						  </table> 
	                	</div>
		          		
		          		<div class="well">		          		
			          		<div class="form-inline">							                        									
								<h4><span class="label label-primary">{_("Garantie")}</span></h4>													              										          					
		                	</div>
		                	<br>		                	
		                	<table class="table">
							    <tbody>					   
						      		<tr>
						      			<td>											
										  <label class="checkbox-inline"><input type="checkbox" id='PDG' {if $Lstgrt['A00D95'] eq 2 || $Lstgrt['A00D95'] eq 4} checked{/if}>{_("Pi�ces")}</label>			
										  {* <span class="button-checkbox"><button type="button" class="btn btn-sm" data-color="info">{_("Pi�ces")}</button><input type="checkbox" class="hidden" id="PDG" {if $Lstgrt['A00D95'] eq 2 || $Lstgrt['A00D95'] eq 4} checked{/if}/></span> *}								
						      			</td>
						      			<td>						      						
						      				<div id="divPDG" {if $Lstgrt['A00D95'] neq 2 && $Lstgrt['A00D95'] neq 4}  class="hidden" {/if}>			                        							                        	
												<label class="checkbox-inline">{_("Dur�e de garantie (mois)")}</label>	
												<input type="text" size = "6"  name="A00E25" value="{$Lstgrt['A00E25']}" id='iPDG' onkeyup="validNumber(this)" onblur="validNumber(this)" >
											</div>																																									              										          															                                               				                                               								          			
						      			</td>
									</tr>
									<tr>	
								    	<td>    											
										  <label class="checkbox-inline"><input type="checkbox" id='MODG' {if $Lstgrt['A00D95'] eq 3 || $Lstgrt['A00D95'] eq 4} checked{/if}>{_("Main d'oeuvre")}</label>
										  {* <span class="button-checkbox"><button type="button" class="btn btn-sm" data-color="info">{_("Main d'oeuvre")}</button><input type="checkbox" class="hidden" id="MODG" {if $Lstgrt['A00D95'] eq 3 || $Lstgrt['A00D95'] eq 4} checked{/if}/></span> *}											
										</td>	
										<td>														
											<div id="divMODG" {if $Lstgrt['A00D95'] neq 3 && $Lstgrt['A00D95'] neq 4} class="hidden"{/if}>			                        							                     
												<label class="checkbox-inline">{_("Dur�e de garantie (mois)")}</label>	
												<input type="text" size = "6" name="DUGAMO" value="{$Lstgrt['DUGAMO']}" id='iMODG' onkeyup="validNumber(this)" onblur="validNumber(this)" >
											</div>																																									              										          															                                               				                                               								          				
										</td>
									</tr>
									<tr>
										<input type="hidden" name="A00D95" id="iA00D95" value="{$Lstgrt['A00D95']}">	
									</tr>
								</tbody>
							</table>		
							<br>
							<div class="form-inline">
    	                        <div class="form-group">							                        	
									<label>{_("D�placement?")}</label>													              										          					
	      							<select name="DEPLAC">
	          							<option value="1" {if $Lstgrt['DEPLAC'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstgrt['DEPLAC'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
    	                        <div class="form-group">							                        	
									<label>{_("Port factur�?")}</label>													              										          					
	      							<select name="A00E10">
	      								<option value="1" {if $Lstgrt['A00E10'] eq 1} selected{/if}>{_("Non")}</option>
	          							<option value="2" {if $Lstgrt['A00E10'] eq 2} selected{/if}>{_("Oui")}</option>	          							                                          							          							              							                                    
	                       			</select>	                                               				                                               					
		          				</div>
    	                        <div class="form-group">							                        	
									<label>{_("Echange standard?")}</label>													              										          					
	      							<select name="A00D90">
	          							<option value="1" {if $Lstgrt['A00D90'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstgrt['A00D90'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
							</div>	
							<br>
							<div class="form-inline">
    	                        <div class="form-group">							                        	
									<label>{_("R�seau de r�parateur agr��s?")}</label>													              										          					
	      							<select name="A00E30">
	          							<option value="1" {if $Lstgrt['A00E30'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstgrt['A00E30'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>
								<div class="input-group"> 
									<span class="input-group-addon" id="sizing-addon2">{_("Liste des r�parateurs agr�es")}</span> <textarea class="form-control" cols ="50" name="MA00E35" onkeyup="valid(this)" onblur="valid(this)">{$Lstgrt['MA00E35']}</textarea>
								</div>
							</div>	
							<br>
							<div class="form-inline">
    	                        <div class="form-group">							                        	
									<label>{_("Pouvez vous g�rer les SAV directement avec notre client ?")}</label>													              										          					
	      							<select name="A00E55">
	          							<option value="1" {if $Lstgrt['A00E55'] eq 1} selected{/if}>{_("Oui")}</option>
	          							<option value="2" {if $Lstgrt['A00E55'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
	                       			</select>	                                               				                                               					
		          				</div>		          				
    	                        <div class="form-group">							                        	
									<label>{_("Si oui, indiquer le num�ro de t�l�phone � joindre")}</label>													              										          					
									<input type="text" size = "15" maxlength="20" name="A00E60" value="{$Lstgrt['A00E60']}" onkeyup="validNumber(this)" onblur="validNumber(this)" >		                                               				                                               					
		          				</div>
							</div>
							<br>
							<div class="well">
								<div class="form-inline">
	    	                        <div class="form-group">							                        	
										<label>{_("Eclat�s disponible?")}</label>													              										          					
		      							<select name="A00E40" id="A00E40">
		          							<option value="1" {if $Lstgrt['A00E40'] eq 1} selected{/if}>{_("Oui")}</option>
		          							<option value="2" {if $Lstgrt['A00E40'] eq 2} selected{/if}>{_("Non")}</option>                                          							          							              							                                     
		                       			</select>	                                               				                                               					
			          				</div>
								</div>	
								<br>
							
								<div class="form-inline">
			                        <div class="form-group">							                        	
										<label>{_("Si oui, ins�rer la pi�ce jointe de l'�clat� pi�ces d�tach�s : ")}</label><input type="file" class="form-control" name="Ficheeclpec" id="Ficheeclpec" accept=".pdf"><button type="button" id="btnannulerFEP" class="btn btn-default btn-primary hidden">{_("Annuler")}</button>
			                             	<div class="form-group" style="margin-bottom: 0;">
				                                <div id="image_preview" class="col-lg-10 col-lg-offset-2">
				                                    <div class="thumbnail hidden">
				                                        <img src="http://placehold.it/5" alt="">
				                                        <div class="caption">
				                                            <h4></h4>
				                                            <p></p>
				                                            <p><button type="button" class="btn btn-default btn-danger">Annuler</button></p>
				                                        </div>
				                                    </div>
				                                </div>
				                               </div> 												              										          															                                               				                                               				
			          				</div>		          																							                                  									
								</div>	
								<br>
								<div class="form-inline">
									<div class="input-group {if $FEP eq ""}hidden{/if}" id="divficheactFEP" > 
										<span class="input-group-addon" id="sizing-addon2">{_("Fiche �clat� pi�ces d�tach�s actuelle : ")}</span><input type="text" id="ficheact" class="form-control" name= "FEP" value="{$FEP}" aria-describedby="sizing-addon2" disabled>														
									</div>								
								</div>	
								<br>								
								 <div class="form-inline">
			                        <div class="form-group">							                        	
										<label>{_("Ins�rer, en pi�ce jointe, le mode d'emploi multilingue : ")}</label><input type="file" class="form-control" name="FicheME" id="FicheME" accept=".pdf"><button type="button" id="btnannulerME" class="btn btn-default btn-primary hidden">{_("Annuler")}</button>
											 <div class="form-group" style="margin-bottom: 0;">
				                                <div id="image_previewbis" class="col-lg-10 col-lg-offset-2">
				                                    <div class="thumbnail hidden">
				                                        <img src="http://placehold.it/5" alt="">
				                                        <div class="caption">
				                                            <h4></h4>
				                                            <p></p>
				                                            <p><button type="button" class="btn btn-default btn-danger">Annuler</button></p>
				                                        </div>
				                                    </div>
				                                </div>
				                              </div>  													              										          															                                               				                                               				
			          				</div>		          				
								</div>
								<br>
								<div class="form-inline">
									<div class="input-group {if $ME eq ""}hidden{/if}" id="divficheactME" > 
										<span class="input-group-addon" id="sizing-addon2">{_("Mode d'emploi actuel : ")}</span><input type="text" id="ficheact" class="form-control" name= "ME" value="{$ME}" aria-describedby="sizing-addon2" disabled>														
									</div>								
								</div> 
							</div>
							
	                	</div>
		          		
	                </div>
	            </div>
                </form>         
          </div>
        </div>
        {include file='page_footer.tpl'}
      </div>
    </div>
    {include file='marquage_js.tpl'}

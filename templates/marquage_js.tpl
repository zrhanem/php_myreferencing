    <script type="text/javascript" src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/bootstrap/transition.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/collapse.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/dropdown.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tab.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/tooltip.js"></script>
    <script type="text/javascript" src="assets/js/vendors/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/smoothscroll.min.js"></script>
    <script type="text/javascript" src="assets/js/vendors/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputnumber.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/modal.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="assets/js/offcanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/main.min.js"></script>
    <script type="text/javascript" src="assets/noty/js/noty/packaged/jquery.noty.packaged.js"></script>
        <!-- jQuery (n�cessaire pour les plugins javascript de Bootstrap) -->        
        <!-- Ce fichier javascript contient tous les plugins fournis par Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Ces fichiers sont pour le upload file -->
        <script src="assets/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
        {* <script src="assets/js/fileinput.min.js"></script>
        <script src="assets/js/fileinput.js"></script> *}
        
        {include file='googleanalytics.tpl'}
        
        {literal}    
	        <script type="text/JavaScript">
				function valid(f) {
				!(/^[A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]*$/i).test(f.value)?f.value = f.value.replace(/[^A-z��������&#209;&#241;0-9-\s\-,.'%�()+*:?!��~]/ig,''):null;
				} 
			</script>       
			<script type="text/JavaScript">
				function validNumber(f) {		
				!(/^[0-9]*$/i).test(f.value)?f.value = f.value.replace(/[^0-9]/ig,''):null;
				} 
			</script>    
			<script type="text/javascript">
			  $(document).ready(function(){					   		     	     			     		  
			      $('#btnValid').click(function(){	
					$('form#my_form').submit();		       					               
			        });                                                                  
			  });
			</script>	
			{* Script pour l'apparence checkbox *}	
			<script type="text/javascript">
				$(function () {
				    $('.button-checkbox').each(function () {
				
				        // Settings
				        var $widget = $(this),
				            $button = $widget.find('button'),
				            $checkbox = $widget.find('input:checkbox'),
				            color = $button.data('color'),
				            settings = {
				                on: {
				                    icon: 'glyphicon glyphicon-check'
				                },
				                off: {
				                    icon: 'glyphicon glyphicon-unchecked'
				                }
				            };
				
				        // Event Handlers
				        $button.on('click', function () {
				            $checkbox.prop('checked', !$checkbox.is(':checked'));
				            $checkbox.triggerHandler('change');
				            updateDisplay();
				        });
				        $checkbox.on('change', function () {
				            updateDisplay();
				        });
				
				        // Actions
				        function updateDisplay() {
				            var isChecked = $checkbox.is(':checked');
				
				            // Set the button's state
				            $button.data('state', (isChecked) ? "on" : "off");
				
				            // Set the button's icon
				            $button.find('.state-icon')
				                .removeClass()
				                .addClass('state-icon ' + settings[$button.data('state')].icon);
				
				            // Update the button's color
				            if (isChecked) {
				                $button
				                    .removeClass('btn-default')
				                    .addClass('btn-' + color + ' active');
				            }
				            else {
				                $button
				                    .removeClass('btn-' + color + ' active')
				                    .addClass('btn-default');
				            }
				        }
				
				        // Initialization
				        function init() {
				
				            updateDisplay();
				
				            // Inject the icon if applicable
				            if ($button.find('.state-icon').length == 0) {
				                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>�');
				            }
				        }
				        init();
				    });
				});
			</script>
		{/literal}
		
        {literal}                
	        <script type="text/javascript">
				$(function () {
				    // A chaque s�lection de fichier FEP
				    $('#my_form').find('input[name="Ficheeclpec"]').on('change', function (e) {
				        var files = $(this)[0].files;
				 
				        if (files.length > 0) {
	   				        if(files[0].size > 10485760){
								alert("Le fichier s�lectionn� d�passe le maximum de taille autoris� qui est de 10Mo");
								$('#my_form').find('input[name="Ficheeclpec"]').val('');				        
					        }else{				            				 
					        
					            // On part du principe qu'il n'y qu'un seul fichier
					            // �tant donn� que l'on a pas renseign� l'attribut "multiple"
					            var file = files[0],
					                $image_preview = $('#image_preview');
					                        
				                $('#btnannulerFEP').removeClass('hidden');
					        	}
				        }
				    });
				    
				    // A chaque s�lection de fichier ME
				    $('#my_form').find('input[name="FicheME"]').on('change', function (e) {
				        var files = $(this)[0].files;
				 
				        if (files.length > 0) {
	   				        if(files[0].size > 10485760){
								alert("Le fichier s�lectionn� d�passe le maximum de taille autoris� qui est de 10Mo");
								$('#my_form').find('input[name="FicheME"]').val('');				        
					        }else{
					        	// On part du principe qu'il n'y qu'un seul fichier
					            // �tant donn� que l'on a pas renseign� l'attribut "multiple"
					            var file = files[0],
					                $image_previewbis = $('#image_previewbis');
					                				            				                
				                $('#btnannulerME').removeClass('hidden');
					        	}
				        }
				    });
				    
				    // Bouton "Annuler" FEP pour vider le champ d'upload
				    $('#btnannulerFEP').click(function(){
				    	e.preventDefault();
				    		
						$('#my_form').find('input[name="Ficheeclpec"]').val('');
						$('#btnannulerFEP').addClass('hidden');	       					               
			        });
			        
			        
				    // Bouton "Annuler" ME pour vider le champ d'upload
				    $('#btnannulerME').click(function(){	
				    	e.preventDefault();
				    	
						$('#my_form').find('input[name="FicheME"]').val('');
						$('#btnannulerME').addClass('hidden');	       					               
			        });  
				 
				});
			</script>
			
			
			<script type="text/javascript">
				$('#PDG').on({'click': function(){					
							if($('#PDG').is(':checked')){
								$('#divPDG').removeClass('hidden');
								if($('#MODG').is(':checked')){
									$('#iA00D95').val('4');
								}else{								
									$('#iA00D95').val('2');
								}
							}else{
								$('#divPDG').addClass('hidden');
								$('#iPDG').val('0');
								if($('#MODG').is(':checked')){
									$('#iA00D95').val('3');
								}else{
									$('#iA00D95').val('1');
								}
							}
						}
					});
				$('#MODG').on({'click': function(){
						
							if($('#MODG').is(':checked')){
								$('#divMODG').removeClass('hidden');
								if($('#PDG').is(':checked')){
									$('#iA00D95').val('4');
								}else{
									$('#iA00D95').val('3');
								}
							}else{
								$('#divMODG').addClass('hidden');
								$('#iMODG').val('0');
								if($('#PDG').is(':checked')){
									$('#iA00D95').val('2');
								}else{
									$('#iA00D95').val('1');
								}
							}
						}
					});
			</script>
				
			<script type="text/javascript">	
				$(function () {
				    $('form#my_form').on('submit', function (e) {
				    				    					    
				        // On emp�che le navigateur de soumettre le formulaire
				        e.preventDefault();
				        
    					var msg = "";
						var err = 0;
						
						if($('#PDG').is(':checked') && $.trim($('#iPDG').val()) < 1 ){									
							msg += "Vous devez indiquer la dur�e de garantie des pi�ces"+"<br>";
							err = 1;
						}
						
						if($('#MODG').is(':checked') && $.trim($('#iMODG').val()) < 1){
							msg += "Vous devez indiquer la dur�e de garantie de la main d'oeuvre"+"<br>";
							err = 1;
						}
						
						if( $('#A00E40').val() == 1 && $('#ficheact').val() =='' && $('#Ficheeclpec').val() ==''  ){
							msg += "Vous devez ins�rer la pi�ce jointe de l'�clat� pi�ces d�tach�s"+"<br>";
							err = 1;
						}
						
						if($.trim($('#Ficheeclpec').val()) != ''){
							var Ficheeclpec = $('#Ficheeclpec').val().split(".");
							if(Ficheeclpec[1].toLowerCase() != "pdf"){
								msg += "Le fichier attach� n'est pas un fichier PDF valide"+"<br>";
								err = 1;	
							}
						}

						if($.trim($('#FicheME').val()) != ''){
							var FicheME = $('#FicheME').val().split(".");
							if(FicheME[1].toLowerCase() != "pdf"){
								msg += "Le fichier attach� n'est pas un fichier PDF valide"+"<br>";
								err = 1;	
							}
						}
							
	 					if(err == 1){
	 						$('#STS').val("N");
							$('#MissingErr').removeClass('hidden');	
							$('#MissingErr').html("");
							$('#MissingErr').html("<strong>"+msg+"</strong>");
							
			                var r = confirm("Vous avez des champs obligatoires non remplis, enregistrer?");
	        				if (r == true) {
	        				
	        					$('#divLoading').show();
						        var formData = new FormData($('form#my_form')[0]);	
				 
						        $.ajax({
						            url: 'ajax/ajax_fileupload_marquage.php',
						            type: 'POST',
						            contentType: false, // obligatoire pour de l'upload
						            processData: false, // obligatoire pour de l'upload
						            data: formData,
								    async: false,
								    cache: false,
						            success: function (response) {					            				           
						            			            	
						                // La r�ponse du serveur				                
						                if(response == 0){
						                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'success',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });
						            		$('#my_form').find('input[name="Ficheeclpec"]').val('');	
											$('#my_form').find('input[name="FicheME"]').val(''); 		
											$('#divLoading').hide(); 	
											location.reload();		      											                					               
						                }else{
						                	if(response == 999){
						                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
						                	}else{
						                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
						                	}	
									        var n = noty({
									            text        : CodeRetour,
									            type        : 'error',
									            dismissQueue: true,
									            timeout     : 5000,
									            closeWith   : ['click'],
									            layout      : 'top',
									            theme       : 'defaultTheme',
									            maxVisible  : 5
									        });	
						                	$('#divLoading').hide(); 
						                	location.reload();
						                }
						                
						            }
						        });
						        
	        				}else{
	        				
				                	var CodeRetour = "Enregistrement annul�";
							        var n = noty({
							            text        : CodeRetour,
							            type        : 'warning',
							            dismissQueue: true,
							            timeout     : 5000,
							            closeWith   : ['click'],
							            layout      : 'top',
							            theme       : 'defaultTheme',
							            maxVisible  : 5
							        });	
	        				
	        				}							
						}else{
							$('#STS').val("O");
							$('#divLoading').show();
						
							$('#MissingErr').addClass('hidden');
							$('#MissingErr').empty();
						
					        var formData = new FormData($('form#my_form')[0]);	
				 
					        $.ajax({
					            url: 'ajax/ajax_fileupload_marquage.php',
					            type: 'POST',
					            contentType: false, // obligatoire pour de l'upload
					            processData: false, // obligatoire pour de l'upload
					            data: formData,
							    async: false,
							    cache: false,
					            success: function (response) {					            				           
					            			            	
					                // La r�ponse du serveur				                
					                if(response == 0){
					                	var CodeRetour = "Toutes les donn�es ont �t� bien enregistr�es";			
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'success',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });
					            		$('#my_form').find('input[name="Ficheeclpec"]').val('');	
										$('#my_form').find('input[name="FicheME"]').val(''); 		
										$('#divLoading').hide(); 	
										location.reload();	            		                					                
					                }else{
					                	if(response == 999){
					                		var CodeRetour = "Une erreur s'est produite lors de la copie des fichiers";
					                	}else{
					                		var CodeRetour = "Une erreur s'est produite lors de l'enregistrement des donn�es";
					                	}	
								        var n = noty({
								            text        : CodeRetour,
								            type        : 'error',
								            dismissQueue: true,
								            timeout     : 5000,
								            closeWith   : ['click'],
								            layout      : 'top',
								            theme       : 'defaultTheme',
								            maxVisible  : 5
								        });	
					                	$('#divLoading').hide();
					                	location.reload(); 
					                }
					                
					            }
					        });
						
						}		

				    });
				});
			</script>	
			
		{/literal}
        
  </body>
</html>





